﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TEMS_BOL;

namespace TEMS_DAL
{
    public class ReportDAL
    {
        TEMSEntities db = new TEMSEntities();

        public List<SelectListItem> GetAgencyNameList()
        {
            List<SelectListItem> AgencyList = new List<SelectListItem>();
            var AgencyData = db.MSTR_AGENCY.Where(x => x.ISACTIVE == true).Select(a => new { AgencyID = a.AGENCY_ID, Names = a.AGENCY_NAME }).OrderBy(a => a.Names).ToList();
            AgencyData.ForEach(item => AgencyList.Add(new SelectListItem { Text = item.Names, Value = item.AgencyID }));

            return AgencyList;
        }

        public List<SelectListItem> GetEmployeesNameList()
        {
            List<SelectListItem> EmployeesList = new List<SelectListItem>();
            var EmployeesData = db.MSTR_USER.Where(x => x.ISACTIVE == true).Select(a => new { UserID = a.USER_ID, Names = a.FULL_NAME }).ToList();
            EmployeesData.ForEach(item => EmployeesList.Add(new SelectListItem { Text = item.Names, Value = item.UserID }));

            return EmployeesList;
        }

        public List<SelectListItem> GetEmployeeAgencyNameList()
        {
            List<SelectListItem> EmployeeAgencyList = new List<SelectListItem>();
            var EmployeesData = db.MSTR_USER.Where(x => x.ISACTIVE == true).Select(a => new { UserID = a.USER_ID, Names = a.FULL_NAME }).ToList();
            var AgencyData = db.MSTR_AGENCY.Where(x => x.ISACTIVE == true).Select(a => new { AgencyID = a.AGENCY_ID, Names = a.AGENCY_NAME }).OrderBy(a => a.Names).ToList();
            EmployeesData.ForEach(item => EmployeeAgencyList.Add(new SelectListItem { Text = item.Names, Value = item.UserID }));
            AgencyData.ForEach(item => EmployeeAgencyList.Add(new SelectListItem { Text = item.Names, Value = item.AgencyID }));

            return EmployeeAgencyList;
        }

        public List<TransactionReportListBOL> GetTransactionReportList(TransactionReportBOL TransactionBOL, DateTime FromDate, DateTime ToDate)
        {
            List<TransactionReportListBOL> ReportListData = new List<TransactionReportListBOL>();

            if (TransactionBOL.ReportFor == "Employee")
            {
                string[] UserIds1 = db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDETURE_DETAILS, a => a.EXPENDETURE_NO, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b }).Where(a => a.a.ISACTIVE == true && a.b.REF_USER_ID != null && a.b.ISACTIVE == true).Where(a => (TransactionBOL.EmployeeName != null && TransactionBOL.EmployeeName != "") ? a.b.REF_USER_ID == TransactionBOL.EmployeeName : true).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.a.JOURNEY_DATE >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.a.JOURNEY_DATE <= ToDate : true).Select(a => a.b.REF_USER_ID).Distinct().ToArray();
                string[] UserIds2 = db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDITURE_LOG, a => a.EXPENDETURE_NO, b => b.EXPENDITURE_NO, (a, b) => new { a, b }).Where(a => a.a.ISACTIVE == true && a.b.REF_USER_ID != null && a.b.IsActive == true).Where(a => (TransactionBOL.EmployeeName != null && TransactionBOL.EmployeeName != "") ? a.b.REF_USER_ID == TransactionBOL.EmployeeName : true).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.a.JOURNEY_DATE >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.a.JOURNEY_DATE <= ToDate : true).Select(a => a.b.REF_USER_ID).Distinct().ToArray();
                string[] UserIds3 = db.TRN_Advance.Where(a => a.RefUserId != null && a.IsActive == true).Where(a => (TransactionBOL.EmployeeName != null && TransactionBOL.EmployeeName != "") ? a.RefUserId == TransactionBOL.EmployeeName : true).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true).Select(a => a.RefUserId).Distinct().ToArray();
                string[] UserIds4 = db.TRN_Payment.Where(a => a.RefUserId != null && a.IsActive == true).Where(a => (TransactionBOL.EmployeeName != null && TransactionBOL.EmployeeName != "") ? a.RefUserId == TransactionBOL.EmployeeName : true).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true).Select(a => a.RefUserId).Distinct().ToArray();

                var UserIds = UserIds1.Union(UserIds2).Union(UserIds3).Union(UserIds4).ToArray();
                foreach (var UserId in UserIds)
                {
                    TransactionReportListBOL ReportData = new TransactionReportListBOL();
                    ReportData.EmployeeID = db.MSTR_USER.Where(a => a.ISACTIVE == true && a.USER_ID == UserId).Select(a => a.USER_ID).FirstOrDefault();
                    ReportData.EmployeeName = db.MSTR_USER.Where(a => a.ISACTIVE == true && a.USER_ID == UserId).Select(a => a.FULL_NAME).FirstOrDefault();
                    ReportData.ClaimedAmount = Convert.ToDecimal(db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDETURE_DETAILS, a => a.EXPENDETURE_NO, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b }).Where(a => a.b.REF_USER_ID == UserId && a.a.ISACTIVE == true && a.b.ISACTIVE == true).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.a.JOURNEY_DATE >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.a.JOURNEY_DATE <= ToDate : true).Sum(a => a.b.ClaimedAmount));
                    ReportData.ApprovedAmount = Convert.ToDecimal(db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDITURE_LOG, a => a.EXPENDETURE_NO, b => b.EXPENDITURE_NO, (a, b) => new { a, b }).Where(a => a.b.REF_USER_ID == UserId && a.a.ISACTIVE == true && a.b.IsActive == true).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.a.JOURNEY_DATE >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.a.JOURNEY_DATE <= ToDate : true).Sum(a => a.b.EXPENDITURE_AMOUNT));
                    ReportData.AdvanceAmount = Convert.ToDecimal(db.TRN_Advance.Where(a => a.IsActive == true && a.RefUserId == UserId).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true).Sum(a => a.AdvanceAmount));
                    ReportData.AdvanceAdjustmentAmount = Convert.ToDecimal(db.TRN_Payment.Where(a => a.IsActive == true && a.RefUserId == UserId).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true).Sum(a => a.AdvAdjAmt));
                    decimal PaidAmount = Convert.ToDecimal(db.TRN_Payment.Where(a => a.IsActive == true && a.RefUserId == UserId).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true).Sum(a => a.PaymentAmount));
                    ReportData.PaymentAmount = PaidAmount + ReportData.AdvanceAdjustmentAmount;
                    ReportData.PaidAmount = ReportData.AdvanceAmount + ReportData.PaymentAmount - ReportData.AdvanceAdjustmentAmount;
                    ReportListData.Add(ReportData);
                }
            }
            else
            {
                string[] AgencyIds1 = db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDETURE_DETAILS, a => a.EXPENDETURE_NO, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b }).Where(a => a.a.ISACTIVE == true && a.b.REF_AGENCY_ID != null && a.b.ISACTIVE == true).Where(a => (TransactionBOL.AgencyName != null && TransactionBOL.AgencyName != "") ? a.b.REF_AGENCY_ID == TransactionBOL.AgencyName : true).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.a.JOURNEY_DATE >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.a.JOURNEY_DATE <= ToDate : true).Select(a => a.b.REF_AGENCY_ID).Distinct().ToArray();
                string[] AgencyIds2 = db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDITURE_LOG, a => a.EXPENDETURE_NO, b => b.EXPENDITURE_NO, (a, b) => new { a, b }).Where(a => a.a.ISACTIVE == true && a.b.REF_AGENCY_ID != null && a.b.IsActive == true).Where(a => (TransactionBOL.AgencyName != null && TransactionBOL.AgencyName != "") ? a.b.REF_AGENCY_ID == TransactionBOL.AgencyName : true).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.a.JOURNEY_DATE >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.a.JOURNEY_DATE <= ToDate : true).Select(a => a.b.REF_AGENCY_ID).Distinct().ToArray();
                string[] AgencyIds3 = db.TRN_Advance.Where(a => a.RefAgencyId != null && a.IsActive == true).Where(a => (TransactionBOL.AgencyName != null && TransactionBOL.AgencyName != "") ? a.RefAgencyId == TransactionBOL.AgencyName : true).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true).Select(a => a.RefAgencyId).Distinct().ToArray();
                string[] AgencyIds4 = db.TRN_Payment.Where(a => a.RefAgencyId != null && a.IsActive == true).Where(a => (TransactionBOL.AgencyName != null && TransactionBOL.AgencyName != "") ? a.RefAgencyId == TransactionBOL.AgencyName : true).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true).Select(a => a.RefAgencyId).Distinct().ToArray();

                var AgencyIds = AgencyIds1.Union(AgencyIds2).Union(AgencyIds3).Union(AgencyIds4).ToArray();

                foreach (var AgencyId in AgencyIds)
                {
                    TransactionReportListBOL ReportData = new TransactionReportListBOL();
                    ReportData.AgencyID = db.MSTR_AGENCY.Where(a => a.ISACTIVE == true && a.AGENCY_ID == AgencyId).Select(a => a.AGENCY_ID).FirstOrDefault();
                    ReportData.AgencyName = db.MSTR_AGENCY.Where(a => a.ISACTIVE == true && a.AGENCY_ID == AgencyId).Select(a => a.AGENCY_NAME).FirstOrDefault();
                    ReportData.ClaimedAmount = Convert.ToDecimal(db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDETURE_DETAILS, a => a.EXPENDETURE_NO, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b }).Where(a => a.b.REF_AGENCY_ID == AgencyId && a.a.ISACTIVE == true && a.b.ISACTIVE == true).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.a.JOURNEY_DATE >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.a.JOURNEY_DATE <= ToDate : true).Sum(a => a.b.ClaimedAmount));
                    ReportData.ApprovedAmount = Convert.ToDecimal(db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDITURE_LOG, a => a.EXPENDETURE_NO, b => b.EXPENDITURE_NO, (a, b) => new { a, b }).Where(a => a.b.REF_AGENCY_ID == AgencyId && a.a.ISACTIVE == true && a.b.IsActive == true).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.a.JOURNEY_DATE >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.a.JOURNEY_DATE <= ToDate : true).Sum(a => a.b.EXPENDITURE_AMOUNT));
                    ReportData.AdvanceAmount = Convert.ToDecimal(db.TRN_Advance.Where(a => a.IsActive == true && a.RefAgencyId == AgencyId).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true).Sum(a => a.AdvanceAmount));
                    ReportData.AdvanceAdjustmentAmount = Convert.ToDecimal(db.TRN_Payment.Where(a => a.IsActive == true && a.RefAgencyId == AgencyId).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true).Sum(a => a.AdvAdjAmt));
                    decimal PaidAmount = Convert.ToDecimal(db.TRN_Payment.Where(a => a.IsActive == true && a.RefAgencyId == AgencyId).Where(a => (TransactionBOL.FromDate != null && TransactionBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true).Where(a => (TransactionBOL.ToDate != null && TransactionBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true).Sum(a => a.PaymentAmount));
                    ReportData.PaymentAmount = PaidAmount + ReportData.AdvanceAdjustmentAmount;
                    ReportData.PaidAmount = ReportData.AdvanceAmount + ReportData.PaymentAmount - ReportData.AdvanceAdjustmentAmount;
                    ReportListData.Add(ReportData);
                }
            }


            return ReportListData;
        }

        public List<TransactionHistoryListBOL> GetTransactionReportHistoryData(string Ids, string FromDate, string ToDate, string Type, DateTime FromDateNow, DateTime ToDateNow)
        {
            List<TransactionHistoryListBOL> ListReportHistory = new List<TransactionHistoryListBOL>();
            if (Type == "Employee")
            {
                var ExpendetureEntry = db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDETURE_DETAILS, a => a.EXPENDETURE_NO, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b }).Where(a => a.b.REF_USER_ID == Ids && a.a.ISACTIVE == true && a.b.ISACTIVE == true).Where(a => (FromDate != null && FromDate != "") ? a.a.JOURNEY_DATE >= FromDateNow : true).Where(a => (ToDate != null && ToDate != "") ? a.a.JOURNEY_DATE <= ToDateNow : true).ToList();
                foreach (var ExpendetureData in ExpendetureEntry)
                {
                    TransactionHistoryListBOL ExpendetureHistory = new TransactionHistoryListBOL();
                    ExpendetureHistory.Particulars = "Claimed";
                    ExpendetureHistory.VoucherNo = ExpendetureData.b.REF_EXPENDETURE_NO;
                    ExpendetureHistory.TransactionDate = ExpendetureData.a.JOURNEY_DATE;
                    ExpendetureHistory.EntryDate = ExpendetureData.b.CREATED_ON;
                    ExpendetureHistory.Amount = Convert.ToDecimal(ExpendetureData.b.ClaimedAmount);
                    ListReportHistory.Add(ExpendetureHistory);
                }

                var ExpendetureApproved = db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDITURE_LOG, a => a.EXPENDETURE_NO, b => b.EXPENDITURE_NO, (a, b) => new { a, b }).Where(a => a.b.REF_USER_ID == Ids && a.a.ISACTIVE == true && a.b.IsActive == true).Where(a => (FromDate != null && FromDate != "") ? a.a.JOURNEY_DATE >= FromDateNow : true).Where(a => (ToDate != null && ToDate != "") ? a.a.JOURNEY_DATE <= ToDateNow : true).ToList();
                foreach (var ExpendetureData in ExpendetureApproved)
                {
                    TransactionHistoryListBOL ExpendetureHistory = new TransactionHistoryListBOL();
                    ExpendetureHistory.Particulars = "Approved";
                    ExpendetureHistory.VoucherNo = ExpendetureData.b.EXPENDITURE_NO;
                    ExpendetureHistory.TransactionDate = ExpendetureData.a.JOURNEY_DATE;
                    ExpendetureHistory.EntryDate = ExpendetureData.b.CreatedOn;
                    ExpendetureHistory.Amount = Convert.ToDecimal(ExpendetureData.b.EXPENDITURE_AMOUNT);
                    ListReportHistory.Add(ExpendetureHistory);
                }

                var ExpendeturePayment = db.TRN_Payment.Where(a => a.IsActive && a.RefUserId == Ids).Where(a => (FromDate != null && FromDate != "") ? a.EffectiveDate >= FromDateNow : true).Where(a => (ToDate != null && ToDate != "") ? a.EffectiveDate <= ToDateNow : true).ToList();
                foreach (var ExpendetureData in ExpendeturePayment)
                {
                    TransactionHistoryListBOL ExpendetureHistory = new TransactionHistoryListBOL();
                    ExpendetureHistory.Particulars = "Payment";
                    ExpendetureHistory.VoucherNo = ExpendetureData.PaymentNumber;
                    ExpendetureHistory.TransactionDate = ExpendetureData.EffectiveDate;
                    ExpendetureHistory.EntryDate = ExpendetureData.CreatedOn;
                    ExpendetureHistory.Amount = Convert.ToDecimal(ExpendetureData.PaymentAmount) + Convert.ToDecimal(ExpendetureData.AdvAdjAmt);
                    ListReportHistory.Add(ExpendetureHistory);
                }

                var ExpendetureAdvance = db.TRN_Advance.Where(a => a.IsActive && a.RefUserId == Ids).Where(a => (FromDate != null && FromDate != "") ? a.EffectiveDate >= FromDateNow : true).Where(a => (ToDate != null && ToDate != "") ? a.EffectiveDate <= ToDateNow : true).ToList();
                foreach (var ExpendetureData in ExpendetureAdvance)
                {
                    TransactionHistoryListBOL ExpendetureHistory = new TransactionHistoryListBOL();
                    ExpendetureHistory.Particulars = "Advance";
                    ExpendetureHistory.VoucherNo = ExpendetureData.AdvanceNumber;
                    ExpendetureHistory.TransactionDate = ExpendetureData.EffectiveDate;
                    ExpendetureHistory.EntryDate = ExpendetureData.CreatedOn;
                    ExpendetureHistory.Amount = Convert.ToDecimal(ExpendetureData.AdvanceAmount);
                    ListReportHistory.Add(ExpendetureHistory);
                }
            }
            else if (Type == "Agency")
            {
                var ExpendetureEntry = db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDETURE_DETAILS, a => a.EXPENDETURE_NO, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b }).Where(a => a.b.REF_AGENCY_ID == Ids && a.a.ISACTIVE == true && a.b.ISACTIVE == true).Where(a => (FromDate != null && FromDate != "") ? a.a.JOURNEY_DATE >= FromDateNow : true).Where(a => (ToDate != null && ToDate != "") ? a.a.JOURNEY_DATE <= ToDateNow : true).ToList();
                foreach (var ExpendetureData in ExpendetureEntry)
                {
                    TransactionHistoryListBOL ExpendetureHistory = new TransactionHistoryListBOL();
                    ExpendetureHistory.Particulars = "Claimed";
                    ExpendetureHistory.VoucherNo = ExpendetureData.b.REF_EXPENDETURE_NO;
                    ExpendetureHistory.TransactionDate = ExpendetureData.a.JOURNEY_DATE;
                    ExpendetureHistory.EntryDate = ExpendetureData.b.CREATED_ON;
                    ExpendetureHistory.Amount = Convert.ToDecimal(ExpendetureData.b.ClaimedAmount);
                    ListReportHistory.Add(ExpendetureHistory);
                }

                var ExpendetureApproved = db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDITURE_LOG, a => a.EXPENDETURE_NO, b => b.EXPENDITURE_NO, (a, b) => new { a, b }).Where(a => a.b.REF_AGENCY_ID == Ids && a.a.ISACTIVE == true && a.b.IsActive == true).Where(a => (FromDate != null && FromDate != "") ? a.a.JOURNEY_DATE >= FromDateNow : true).Where(a => (ToDate != null && ToDate != "") ? a.a.JOURNEY_DATE <= ToDateNow : true).ToList();
                foreach (var ExpendetureData in ExpendetureApproved)
                {
                    TransactionHistoryListBOL ExpendetureHistory = new TransactionHistoryListBOL();
                    ExpendetureHistory.Particulars = "Approved";
                    ExpendetureHistory.VoucherNo = ExpendetureData.b.EXPENDITURE_NO;
                    ExpendetureHistory.TransactionDate = ExpendetureData.a.JOURNEY_DATE;
                    ExpendetureHistory.EntryDate = ExpendetureData.b.CreatedOn;
                    ExpendetureHistory.Amount = Convert.ToDecimal(ExpendetureData.b.EXPENDITURE_AMOUNT);
                    ListReportHistory.Add(ExpendetureHistory);
                }

                var ExpendeturePayment = db.TRN_Payment.Where(a => a.IsActive && a.RefAgencyId == Ids).Where(a => (FromDate != null && FromDate != "") ? a.EffectiveDate >= FromDateNow : true).Where(a => (ToDate != null && ToDate != "") ? a.EffectiveDate <= ToDateNow : true).ToList();
                foreach (var ExpendetureData in ExpendeturePayment)
                {
                    TransactionHistoryListBOL ExpendetureHistory = new TransactionHistoryListBOL();
                    ExpendetureHistory.Particulars = "Payment";
                    ExpendetureHistory.VoucherNo = ExpendetureData.PaymentNumber;
                    ExpendetureHistory.TransactionDate = ExpendetureData.EffectiveDate;
                    ExpendetureHistory.EntryDate = ExpendetureData.CreatedOn;
                    ExpendetureHistory.Amount = Convert.ToDecimal(ExpendetureData.PaymentAmount) + Convert.ToDecimal(ExpendetureData.AdvAdjAmt);
                    ListReportHistory.Add(ExpendetureHistory);
                }

                var ExpendetureAdvance = db.TRN_Advance.Where(a => a.IsActive && a.RefAgencyId == Ids).Where(a => (FromDate != null && FromDate != "") ? a.EffectiveDate >= FromDateNow : true).Where(a => (ToDate != null && ToDate != "") ? a.EffectiveDate <= ToDateNow : true).ToList();
                foreach (var ExpendetureData in ExpendetureAdvance)
                {
                    TransactionHistoryListBOL ExpendetureHistory = new TransactionHistoryListBOL();
                    ExpendetureHistory.Particulars = "Advance";
                    ExpendetureHistory.VoucherNo = ExpendetureData.AdvanceNumber;
                    ExpendetureHistory.TransactionDate = ExpendetureData.EffectiveDate;
                    ExpendetureHistory.EntryDate = ExpendetureData.CreatedOn;
                    ExpendetureHistory.Amount = Convert.ToDecimal(ExpendetureData.AdvanceAmount);
                    ListReportHistory.Add(ExpendetureHistory);
                }
            }

            return ListReportHistory;
        }

        public List<RegisterReportListBOL> GetRegisterReportList(RegisterReportBOL RegisterBOL, DateTime Years, DateTime Month, DateTime FromMonth, DateTime ToMonth, DateTime FromDate, DateTime ToDate)
        {
            List<RegisterReportListBOL> RegisterReportListData = new List<RegisterReportListBOL>();
            DateTime YearsFrom = (RegisterBOL.Years != "" && RegisterBOL.Years != null) ? Convert.ToDateTime("01-Jan" + RegisterBOL.Years) : DateTime.Now;
            DateTime YearsTo = (RegisterBOL.Years != "" && RegisterBOL.Years != null) ? Convert.ToDateTime(YearsFrom).AddYears(1).AddMinutes(-1) : DateTime.Now;
            DateTime MonthFrom = (RegisterBOL.Month != "" && RegisterBOL.Month != null) ? Convert.ToDateTime("01-" + RegisterBOL.Month) : DateTime.Now;
            DateTime MonthTo = (RegisterBOL.Month != "" && RegisterBOL.Month != null) ? Convert.ToDateTime(MonthFrom).AddMonths(1).AddMinutes(-1) : DateTime.Now;

            string[] RegisterIds1 = db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDETURE_DETAILS, a => a.EXPENDETURE_NO, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b })
                .Where(a => a.a.ISACTIVE == true && a.b.REF_USER_ID != null && a.b.ISACTIVE == true)
                .Where(a => (RegisterBOL.EmployeeName != null && RegisterBOL.EmployeeName != "") ? a.b.REF_USER_ID == RegisterBOL.EmployeeName : true)
                .Where(a => (RegisterBOL.EmployeeOrAgencyName != null && RegisterBOL.EmployeeOrAgencyName != "") ? a.b.REF_USER_ID == RegisterBOL.EmployeeOrAgencyName : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.a.JOURNEY_DATE >= Years : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.a.JOURNEY_DATE <= YearsTo : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.a.JOURNEY_DATE >= Month : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.a.JOURNEY_DATE <= MonthTo : true)
                .Where(a => (RegisterBOL.FromMonth != null && RegisterBOL.FromMonth != "") ? a.a.JOURNEY_DATE >= FromMonth : true)
                .Where(a => (RegisterBOL.ToMonth != null && RegisterBOL.ToMonth != "") ? a.a.JOURNEY_DATE <= ToMonth : true)
                .Where(a => (RegisterBOL.FromDate != null && RegisterBOL.FromDate != "") ? a.a.JOURNEY_DATE >= FromDate : true)
                .Where(a => (RegisterBOL.ToDate != null && RegisterBOL.ToDate != "") ? a.a.JOURNEY_DATE <= ToDate : true)
                .Select(a => a.b.REF_USER_ID).Distinct().ToArray();

            string[] RegisterIds2 = db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDITURE_LOG, a => a.EXPENDETURE_NO, b => b.EXPENDITURE_NO, (a, b) => new { a, b })
                .Where(a => a.a.ISACTIVE == true && a.b.REF_USER_ID != null && a.b.IsActive == true)
                .Where(a => (RegisterBOL.EmployeeName != null && RegisterBOL.EmployeeName != "") ? a.b.REF_USER_ID == RegisterBOL.EmployeeName : true)
                .Where(a => (RegisterBOL.EmployeeOrAgencyName != null && RegisterBOL.EmployeeOrAgencyName != "") ? a.b.REF_USER_ID == RegisterBOL.EmployeeOrAgencyName : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.a.JOURNEY_DATE >= Years : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.a.JOURNEY_DATE <= YearsTo : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.a.JOURNEY_DATE >= Month : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.a.JOURNEY_DATE <= MonthTo : true)
                .Where(a => (RegisterBOL.FromMonth != null && RegisterBOL.FromMonth != "") ? a.a.JOURNEY_DATE >= FromMonth : true)
                .Where(a => (RegisterBOL.ToMonth != null && RegisterBOL.ToMonth != "") ? a.a.JOURNEY_DATE <= ToMonth : true)
                .Where(a => (RegisterBOL.FromDate != null && RegisterBOL.FromDate != "") ? a.a.JOURNEY_DATE >= FromDate : true)
                .Where(a => (RegisterBOL.ToDate != null && RegisterBOL.ToDate != "") ? a.a.JOURNEY_DATE <= ToDate : true)
                .Select(a => a.b.REF_USER_ID).Distinct().ToArray();

            string[] RegisterIds3 = db.TRN_Advance.Where(a => a.RefUserId != null && a.IsActive == true)
                .Where(a => (RegisterBOL.EmployeeName != null && RegisterBOL.EmployeeName != "") ? a.RefUserId == RegisterBOL.EmployeeName : true)
                .Where(a => (RegisterBOL.EmployeeOrAgencyName != null && RegisterBOL.EmployeeOrAgencyName != "") ? a.RefUserId == RegisterBOL.EmployeeOrAgencyName : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.EffectiveDate >= Years : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.EffectiveDate <= YearsTo : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.EffectiveDate >= Month : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.EffectiveDate <= MonthTo : true)
                .Where(a => (RegisterBOL.FromMonth != null && RegisterBOL.FromMonth != "") ? a.EffectiveDate >= FromMonth : true)
                .Where(a => (RegisterBOL.ToMonth != null && RegisterBOL.ToMonth != "") ? a.EffectiveDate <= ToMonth : true)
                .Where(a => (RegisterBOL.FromDate != null && RegisterBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true)
                .Where(a => (RegisterBOL.ToDate != null && RegisterBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true)
                .Select(a => a.RefUserId).Distinct().ToArray();

            string[] RegisterIds4 = db.TRN_Payment.Where(a => a.RefUserId != null && a.IsActive == true)
                .Where(a => (RegisterBOL.EmployeeName != null && RegisterBOL.EmployeeName != "") ? a.RefUserId == RegisterBOL.EmployeeName : true)
                .Where(a => (RegisterBOL.EmployeeOrAgencyName != null && RegisterBOL.EmployeeOrAgencyName != "") ? a.RefUserId == RegisterBOL.EmployeeOrAgencyName : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.EffectiveDate >= Years : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.EffectiveDate <= YearsTo : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.EffectiveDate >= Month : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.EffectiveDate <= MonthTo : true)
                .Where(a => (RegisterBOL.FromMonth != null && RegisterBOL.FromMonth != "") ? a.EffectiveDate >= FromMonth : true)
                .Where(a => (RegisterBOL.ToMonth != null && RegisterBOL.ToMonth != "") ? a.EffectiveDate <= ToMonth : true)
                .Where(a => (RegisterBOL.FromDate != null && RegisterBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true)
                .Where(a => (RegisterBOL.ToDate != null && RegisterBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true)
                .Select(a => a.RefUserId).Distinct().ToArray();

            string[] RegisterIds5 = db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDETURE_DETAILS, a => a.EXPENDETURE_NO, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b })
                .Where(a => a.a.ISACTIVE == true && a.b.REF_AGENCY_ID != null && a.b.ISACTIVE == true)
                .Where(a => (RegisterBOL.AgencyName != null && RegisterBOL.AgencyName != "") ? a.b.REF_AGENCY_ID == RegisterBOL.AgencyName : true)
                .Where(a => (RegisterBOL.EmployeeOrAgencyName != null && RegisterBOL.EmployeeOrAgencyName != "") ? a.b.REF_AGENCY_ID == RegisterBOL.EmployeeOrAgencyName : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.a.JOURNEY_DATE >= Years : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.a.JOURNEY_DATE <= YearsTo : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.a.JOURNEY_DATE >= Month : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.a.JOURNEY_DATE <= MonthTo : true)
                .Where(a => (RegisterBOL.FromMonth != null && RegisterBOL.FromMonth != "") ? a.a.JOURNEY_DATE >= FromMonth : true)
                .Where(a => (RegisterBOL.ToMonth != null && RegisterBOL.ToMonth != "") ? a.a.JOURNEY_DATE <= ToMonth : true)
                .Where(a => (RegisterBOL.FromDate != null && RegisterBOL.FromDate != "") ? a.a.JOURNEY_DATE >= FromDate : true)
                .Where(a => (RegisterBOL.ToDate != null && RegisterBOL.ToDate != "") ? a.a.JOURNEY_DATE <= ToDate : true)
                .Select(a => a.b.REF_AGENCY_ID).Distinct().ToArray();

            string[] RegisterIds6 = db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDITURE_LOG, a => a.EXPENDETURE_NO, b => b.EXPENDITURE_NO, (a, b) => new { a, b })
                .Where(a => a.a.ISACTIVE == true && a.b.REF_AGENCY_ID != null && a.b.IsActive == true)
                .Where(a => (RegisterBOL.AgencyName != null && RegisterBOL.AgencyName != "") ? a.b.REF_AGENCY_ID == RegisterBOL.AgencyName : true)
                .Where(a => (RegisterBOL.EmployeeOrAgencyName != null && RegisterBOL.EmployeeOrAgencyName != "") ? a.b.REF_AGENCY_ID == RegisterBOL.EmployeeOrAgencyName : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.a.JOURNEY_DATE >= Years : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.a.JOURNEY_DATE <= YearsTo : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.a.JOURNEY_DATE >= Month : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.a.JOURNEY_DATE <= MonthTo : true)
                .Where(a => (RegisterBOL.FromMonth != null && RegisterBOL.FromMonth != "") ? a.a.JOURNEY_DATE >= FromMonth : true)
                .Where(a => (RegisterBOL.ToMonth != null && RegisterBOL.ToMonth != "") ? a.a.JOURNEY_DATE <= ToMonth : true)
                .Where(a => (RegisterBOL.FromDate != null && RegisterBOL.FromDate != "") ? a.a.JOURNEY_DATE >= FromDate : true)
                .Where(a => (RegisterBOL.ToDate != null && RegisterBOL.ToDate != "") ? a.a.JOURNEY_DATE <= ToDate : true)
                .Select(a => a.b.REF_AGENCY_ID).Distinct().ToArray();

            string[] RegisterIds7 = db.TRN_Advance.Where(a => a.RefAgencyId != null && a.IsActive == true)
                .Where(a => (RegisterBOL.AgencyName != null && RegisterBOL.AgencyName != "") ? a.RefAgencyId == RegisterBOL.AgencyName : true)
                .Where(a => (RegisterBOL.EmployeeOrAgencyName != null && RegisterBOL.EmployeeOrAgencyName != "") ? a.RefAgencyId == RegisterBOL.EmployeeOrAgencyName : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.EffectiveDate >= Years : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.EffectiveDate <= YearsTo : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.EffectiveDate >= Month : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.EffectiveDate <= MonthTo : true)
                .Where(a => (RegisterBOL.FromMonth != null && RegisterBOL.FromMonth != "") ? a.EffectiveDate >= FromMonth : true)
                .Where(a => (RegisterBOL.ToMonth != null && RegisterBOL.ToMonth != "") ? a.EffectiveDate <= ToMonth : true)
                .Where(a => (RegisterBOL.FromDate != null && RegisterBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true)
                .Where(a => (RegisterBOL.ToDate != null && RegisterBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true)
                .Select(a => a.RefAgencyId).Distinct().ToArray();

            string[] RegisterIds8 = db.TRN_Payment.Where(a => a.RefAgencyId != null && a.IsActive == true)
                .Where(a => (RegisterBOL.AgencyName != null && RegisterBOL.AgencyName != "") ? a.RefAgencyId == RegisterBOL.AgencyName : true)
                .Where(a => (RegisterBOL.EmployeeOrAgencyName != null && RegisterBOL.EmployeeOrAgencyName != "") ? a.RefAgencyId == RegisterBOL.EmployeeOrAgencyName : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.EffectiveDate >= Years : true)
                .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.EffectiveDate <= YearsTo : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.EffectiveDate >= Month : true)
                .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.EffectiveDate <= MonthTo : true)
                .Where(a => (RegisterBOL.FromMonth != null && RegisterBOL.FromMonth != "") ? a.EffectiveDate >= FromMonth : true)
                .Where(a => (RegisterBOL.ToMonth != null && RegisterBOL.ToMonth != "") ? a.EffectiveDate <= ToMonth : true)
                .Where(a => (RegisterBOL.FromDate != null && RegisterBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true)
                .Where(a => (RegisterBOL.ToDate != null && RegisterBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true)
                .Select(a => a.RefAgencyId).Distinct().ToArray();

            string[] RegisterIds;
            if (RegisterBOL.ReportFor == "Employee")
            {
                RegisterIds = RegisterIds1.Union(RegisterIds2).Union(RegisterIds3).Union(RegisterIds4).ToArray();

            }
            else if (RegisterBOL.ReportFor == "Agency")
            {
                RegisterIds = RegisterIds5.Union(RegisterIds6).Union(RegisterIds7).Union(RegisterIds8).ToArray();

            }
            else
            {
                RegisterIds = RegisterIds1.Union(RegisterIds2).Union(RegisterIds3).Union(RegisterIds4).Union(RegisterIds5).Union(RegisterIds6).Union(RegisterIds7).Union(RegisterIds8).ToArray();

            }

            foreach (var Ids in RegisterIds)
            {
                RegisterReportListBOL ReportData = new RegisterReportListBOL();
                if (Ids.StartsWith("A"))
                {
                    ReportData.Names = db.MSTR_AGENCY.Where(a => a.ISACTIVE && a.AGENCY_ID == Ids).Select(a => a.AGENCY_NAME).FirstOrDefault();
                }
                else
                {
                    ReportData.Names = db.MSTR_USER.Where(a => a.ISACTIVE && a.USER_ID == Ids).Select(a => a.FULL_NAME).FirstOrDefault();
                }
                ReportData.ClaimedAmount = Convert.ToDecimal(db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDETURE_DETAILS, a => a.EXPENDETURE_NO, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b })
                    .Where(a => (a.b.REF_USER_ID == Ids || a.b.REF_AGENCY_ID == Ids) && a.a.ISACTIVE == true && a.b.ISACTIVE == true )
                    .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.a.JOURNEY_DATE >= Years : true)
                    .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.a.JOURNEY_DATE <= YearsTo : true)
                    .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.a.JOURNEY_DATE >= Month : true)
                    .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.a.JOURNEY_DATE <= MonthTo : true)
                    .Where(a => (RegisterBOL.FromMonth != null && RegisterBOL.FromMonth != "") ? a.a.JOURNEY_DATE >= FromMonth : true)
                    .Where(a => (RegisterBOL.ToMonth != null && RegisterBOL.ToMonth != "") ? a.a.JOURNEY_DATE <= ToMonth : true)
                    .Where(a => (RegisterBOL.FromDate != null && RegisterBOL.FromDate != "") ? a.a.JOURNEY_DATE >= FromDate : true)
                    .Where(a => (RegisterBOL.ToDate != null && RegisterBOL.ToDate != "") ? a.a.JOURNEY_DATE <= ToDate : true)
                    .Sum(a => a.b.ClaimedAmount));

                ReportData.ApprovedAmount = Convert.ToDecimal(db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDITURE_LOG, a => a.EXPENDETURE_NO, b => b.EXPENDITURE_NO, (a, b) => new { a, b })
                    .Where(a => (a.b.REF_USER_ID == Ids || a.b.REF_AGENCY_ID == Ids) && a.a.ISACTIVE == true && a.b.IsActive == true)
                    .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.a.JOURNEY_DATE >= Years : true)
                    .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.a.JOURNEY_DATE <= YearsTo : true)
                    .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.a.JOURNEY_DATE >= Month : true)
                    .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.a.JOURNEY_DATE <= MonthTo : true)
                    .Where(a => (RegisterBOL.FromMonth != null && RegisterBOL.FromMonth != "") ? a.a.JOURNEY_DATE >= FromMonth : true)
                    .Where(a => (RegisterBOL.ToMonth != null && RegisterBOL.ToMonth != "") ? a.a.JOURNEY_DATE <= ToMonth : true)
                    .Where(a => (RegisterBOL.FromDate != null && RegisterBOL.FromDate != "") ? a.a.JOURNEY_DATE >= FromDate : true)
                    .Where(a => (RegisterBOL.ToDate != null && RegisterBOL.ToDate != "") ? a.a.JOURNEY_DATE <= ToDate : true)
                    .Sum(a => a.b.EXPENDITURE_AMOUNT));

                ReportData.AdvanceAmount = Convert.ToDecimal(db.TRN_Advance.Where(a => a.IsActive && (a.RefUserId == Ids || a.RefAgencyId == Ids))
                    .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.EffectiveDate >= Years : true)
                    .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.EffectiveDate <= YearsTo : true)
                    .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.EffectiveDate >= Month : true)
                    .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.EffectiveDate <= MonthTo : true)
                    .Where(a => (RegisterBOL.FromMonth != null && RegisterBOL.FromMonth != "") ? a.EffectiveDate >= FromMonth : true)
                    .Where(a => (RegisterBOL.ToMonth != null && RegisterBOL.ToMonth != "") ? a.EffectiveDate <= ToMonth : true)
                    .Where(a => (RegisterBOL.FromDate != null && RegisterBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true)
                    .Where(a => (RegisterBOL.ToDate != null && RegisterBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true)
                    .Sum(a => a.AdvanceAmount));

                ReportData.AdvanceAdjustmentAmount = Convert.ToDecimal(db.TRN_Payment.Where(a => a.IsActive && (a.RefUserId == Ids || a.RefAgencyId == Ids))
                    .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.EffectiveDate >= Years : true)
                    .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.EffectiveDate <= YearsTo : true)
                    .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.EffectiveDate >= Month : true)
                    .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.EffectiveDate <= MonthTo : true)
                    .Where(a => (RegisterBOL.FromMonth != null && RegisterBOL.FromMonth != "") ? a.EffectiveDate >= FromMonth : true)
                    .Where(a => (RegisterBOL.ToMonth != null && RegisterBOL.ToMonth != "") ? a.EffectiveDate <= ToMonth : true)
                    .Where(a => (RegisterBOL.FromDate != null && RegisterBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true)
                    .Where(a => (RegisterBOL.ToDate != null && RegisterBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true)
                    .Sum(a => a.AdvAdjAmt));

                decimal PaidAmount = Convert.ToDecimal(db.TRN_Payment.Where(a => a.IsActive && (a.RefUserId == Ids || a.RefAgencyId == Ids))
                    .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.EffectiveDate >= Years : true)
                    .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.EffectiveDate <= YearsTo : true)
                    .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.EffectiveDate >= Month : true)
                    .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.EffectiveDate <= MonthTo : true)
                    .Where(a => (RegisterBOL.FromMonth != null && RegisterBOL.FromMonth != "") ? a.EffectiveDate >= FromMonth : true)
                    .Where(a => (RegisterBOL.ToMonth != null && RegisterBOL.ToMonth != "") ? a.EffectiveDate <= ToMonth : true)
                    .Where(a => (RegisterBOL.FromDate != null && RegisterBOL.FromDate != "") ? a.EffectiveDate >= FromDate : true)
                    .Where(a => (RegisterBOL.ToDate != null && RegisterBOL.ToDate != "") ? a.EffectiveDate <= ToDate : true)
                    .Sum(a => a.PaymentAmount));

                ReportData.PaymentAmount = PaidAmount + ReportData.AdvanceAdjustmentAmount;
                ReportData.TotalAmount = ReportData.AdvanceAmount + ReportData.PaymentAmount - ReportData.AdvanceAdjustmentAmount;
                ReportData.BalanceAmount = ReportData.ApprovedAmount - ReportData.TotalAmount;

                ReportData.UnsettledClaimsAmount = Convert.ToDecimal(db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDETURE_DETAILS, a => a.EXPENDETURE_NO, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b })
                    .Join(db.EXPENDETURE_STATUS_LOG, ab => ab.a.EXPENDETURE_NO, c => c.REF_EXPENDETURE_NO, (ab, c) => new { ab, c })
                    .Where(a => (a.ab.b.REF_USER_ID == Ids || a.ab.b.REF_AGENCY_ID == Ids) && a.ab.a.ISACTIVE == true && a.ab.b.ISACTIVE == true && a.c.ISACTIVE == true && a.c.REF_APPROVE_STATUS_ID == "AS0006")
                    .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.ab.a.JOURNEY_DATE >= Years : true)
                    .Where(a => (RegisterBOL.Years != null && RegisterBOL.Years != "") ? a.ab.a.JOURNEY_DATE <= YearsTo : true)
                    .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.ab.a.JOURNEY_DATE >= Month : true)
                    .Where(a => (RegisterBOL.Month != null && RegisterBOL.Month != "") ? a.ab.a.JOURNEY_DATE <= MonthTo : true)
                    .Where(a => (RegisterBOL.FromMonth != null && RegisterBOL.FromMonth != "") ? a.ab.a.JOURNEY_DATE >= FromMonth : true)
                    .Where(a => (RegisterBOL.ToMonth != null && RegisterBOL.ToMonth != "") ? a.ab.a.JOURNEY_DATE <= ToMonth : true)
                    .Where(a => (RegisterBOL.FromDate != null && RegisterBOL.FromDate != "") ? a.ab.a.JOURNEY_DATE >= FromDate : true)
                    .Where(a => (RegisterBOL.ToDate != null && RegisterBOL.ToDate != "") ? a.ab.a.JOURNEY_DATE <= ToDate : true)
                    .Sum(a => a.ab.b.ENTRY_AMOUNT));

                RegisterReportListData.Add(ReportData);
            }
            return RegisterReportListData;
        }



        public List<ExpendetureSummaryReportListBOL> GetExpendetureSummaryReportList(ExpendetureReportBOL ExpendetureBOL, DateTime FromDate, DateTime ToDate)
        {
            List<ExpendetureSummaryReportListBOL> ExpendetureSummaryReportList = new List<ExpendetureSummaryReportListBOL>();

            var ExpenditureSummary = db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDETURE_DETAILS,a => a.EXPENDETURE_NO,b => b.REF_EXPENDETURE_NO,(a, b) => new { a, b }).
                Join(db.EXPENDETURE_STATUS_LOG,ab => ab.a.EXPENDETURE_NO, c => c.REF_EXPENDETURE_NO,(ab, c) => new { ab, c })
                .Where(x => x.ab.a.ISACTIVE == true && x.ab.b.ISACTIVE == true && x.c.ISACTIVE == true )
                .Where(a => (ExpendetureBOL.ExpendetureNo != null && ExpendetureBOL.ExpendetureNo != "") ? a.ab.a.EXPENDETURE_NO == ExpendetureBOL.ExpendetureNo : true)
                .Where(a => (ExpendetureBOL.EmployeeOrAgencyName != null && ExpendetureBOL.EmployeeOrAgencyName != "") ? (a.ab.b.REF_USER_ID == ExpendetureBOL.EmployeeOrAgencyName) || (a.ab.b.REF_AGENCY_ID == ExpendetureBOL.EmployeeOrAgencyName) : true)
                .Where(a => (ExpendetureBOL.EmployeeName != null && ExpendetureBOL.EmployeeName != "") ? a.ab.b.REF_USER_ID == ExpendetureBOL.EmployeeName : true)
                .Where(a => (ExpendetureBOL.AgencyName != null && ExpendetureBOL.AgencyName != "") ? a.ab.b.REF_AGENCY_ID == ExpendetureBOL.AgencyName : true)
                .Where(a => (ExpendetureBOL.FromDate != null && ExpendetureBOL.FromDate != "") ? a.ab.a.JOURNEY_DATE >= FromDate : true)
                .Where(a => (ExpendetureBOL.ToDate != null && ExpendetureBOL.ToDate != "") ? a.ab.a.JOURNEY_DATE <= ToDate : true).
                Select(x => new
                {
                    ExpendetureNo = x.ab.a.EXPENDETURE_NO,
                    ExpByUser = x.ab.b.REF_USER_ID,
                    ExpByAgency = x.ab.b.REF_AGENCY_ID,
                    Journeydate = x.ab.a.JOURNEY_DATE,
                    EntryAmount = x.ab.b.ENTRY_AMOUNT,
                    ClaimedAmount = x.ab.b.ClaimedAmount,
                    ApprovedAmount = x.ab.b.APPROVED_AMOUNT,
                    Status = x.c.MSTR_APPROVE_STATUS.APPROVE_STATUS_NAME
                }).OrderBy(x => x.Journeydate).OrderByDescending(x => x.ExpendetureNo).ToList();

            string[] DistinctExpNo = ExpenditureSummary.Select(x => x.ExpendetureNo).Distinct().ToArray();
            foreach (var ExpNo in DistinctExpNo)
            {
                var ParticularExpNo = ExpenditureSummary.Where(x => x.ExpendetureNo == ExpNo).ToList();
                if (ExpendetureBOL.ReportFor == "Employee")
                {
                    string[] UserIds = ParticularExpNo.Where(x => x.ExpByUser != null).Select(x => x.ExpByUser).Distinct().ToArray();
                    foreach (var UserId in UserIds)
                    {
                        var ParticularExpByUser = ParticularExpNo.Where(x => x.ExpByUser == UserId).ToList();
                        ExpendetureSummaryReportListBOL SummaryData = new ExpendetureSummaryReportListBOL();
                        SummaryData.ExpendetureNo = ParticularExpByUser[0].ExpendetureNo;
                        SummaryData.ReportFor = "Employee";
                        SummaryData.Name = db.MSTR_USER.Where(a => a.ISACTIVE == true && a.USER_ID == UserId).Select(a => a.FULL_NAME).FirstOrDefault();
                        SummaryData.VoucherDate = ParticularExpByUser[0].Journeydate;
                        SummaryData.EntryAmount = Convert.ToDecimal(ParticularExpByUser.Sum(x => x.EntryAmount));
                        SummaryData.ClaimedAmount = Convert.ToDecimal(ParticularExpByUser.Sum(x => x.ClaimedAmount));
                        SummaryData.ApprovedAmount = Convert.ToDecimal(ParticularExpByUser.Sum(x => x.ApprovedAmount));
                        SummaryData.Status = ParticularExpByUser[0].Status;
                        ExpendetureSummaryReportList.Add(SummaryData);
                    }
                }
                else if (ExpendetureBOL.ReportFor == "Agency")
                {
                    string[] AgencyIds = ParticularExpNo.Where(x => x.ExpByAgency != null).Select(x => x.ExpByAgency).Distinct().ToArray();
                    foreach (var AgencyId in AgencyIds)
                    {
                        var ParticularExpByAgency = ParticularExpNo.Where(x => x.ExpByAgency == AgencyId).ToList();
                        ExpendetureSummaryReportListBOL SummaryData = new ExpendetureSummaryReportListBOL();
                        SummaryData.ExpendetureNo = ParticularExpByAgency[0].ExpendetureNo;
                        SummaryData.ReportFor = "Agency";
                        SummaryData.Name = db.MSTR_AGENCY.Where(a => a.ISACTIVE == true && a.AGENCY_ID == AgencyId).Select(a => a.AGENCY_NAME).FirstOrDefault();
                        SummaryData.VoucherDate = ParticularExpByAgency[0].Journeydate;
                        SummaryData.EntryAmount = Convert.ToDecimal(ParticularExpByAgency.Sum(x => x.EntryAmount));
                        SummaryData.ClaimedAmount = Convert.ToDecimal(ParticularExpByAgency.Sum(x => x.ClaimedAmount));
                        SummaryData.ApprovedAmount = Convert.ToDecimal(ParticularExpByAgency.Sum(x => x.ApprovedAmount));
                        SummaryData.Status = ParticularExpByAgency[0].Status;
                        ExpendetureSummaryReportList.Add(SummaryData);
                    }
                }
                else
                {
                    string[] UserIds = ParticularExpNo.Where(x => x.ExpByUser != null).Select(x => x.ExpByUser).Distinct().ToArray();
                    foreach (var UserId in UserIds)
                    {
                        var ParticularExpByUser = ParticularExpNo.Where(x => x.ExpByUser == UserId).ToList();
                        ExpendetureSummaryReportListBOL SummaryData = new ExpendetureSummaryReportListBOL();
                        SummaryData.ExpendetureNo = ParticularExpByUser[0].ExpendetureNo;
                        SummaryData.ReportFor = "Employee";
                        SummaryData.Name = db.MSTR_USER.Where(a => a.ISACTIVE == true && a.USER_ID == UserId).Select(a => a.FULL_NAME).FirstOrDefault();
                        SummaryData.VoucherDate = ParticularExpByUser[0].Journeydate;
                        SummaryData.EntryAmount = Convert.ToDecimal(ParticularExpByUser.Sum(x => x.EntryAmount));
                        SummaryData.ClaimedAmount = Convert.ToDecimal(ParticularExpByUser.Sum(x => x.ClaimedAmount));
                        SummaryData.ApprovedAmount = Convert.ToDecimal(ParticularExpByUser.Sum(x => x.ApprovedAmount));
                        SummaryData.Status = ParticularExpByUser[0].Status;
                        ExpendetureSummaryReportList.Add(SummaryData);
                    }

                    string[] AgencyIds = ParticularExpNo.Where(x => x.ExpByAgency != null).Select(x => x.ExpByAgency).Distinct().ToArray();
                    foreach (var AgencyId in AgencyIds)
                    {
                        var ParticularExpByAgency = ParticularExpNo.Where(x => x.ExpByAgency == AgencyId).ToList();
                        ExpendetureSummaryReportListBOL SummaryData = new ExpendetureSummaryReportListBOL();
                        SummaryData.ExpendetureNo = ParticularExpByAgency[0].ExpendetureNo;
                        SummaryData.ReportFor = "Agency";
                        SummaryData.Name = db.MSTR_AGENCY.Where(a => a.ISACTIVE == true && a.AGENCY_ID == AgencyId).Select(a => a.AGENCY_NAME).FirstOrDefault();
                        SummaryData.VoucherDate = ParticularExpByAgency[0].Journeydate;
                        SummaryData.EntryAmount = Convert.ToDecimal(ParticularExpByAgency.Sum(x => x.EntryAmount));
                        SummaryData.ClaimedAmount = Convert.ToDecimal(ParticularExpByAgency.Sum(x => x.ClaimedAmount));
                        SummaryData.ApprovedAmount = Convert.ToDecimal(ParticularExpByAgency.Sum(x => x.ApprovedAmount));
                        SummaryData.Status = ParticularExpByAgency[0].Status;
                        ExpendetureSummaryReportList.Add(SummaryData);
                    }
                }
                
            }
            return ExpendetureSummaryReportList;
        }

        public List<ExpendetureDetailsReportListBOL> GetExpendetureDetailsReportList(ExpendetureReportBOL ExpendetureBOL, DateTime FromDate, DateTime ToDate)
        {
            List<ExpendetureDetailsReportListBOL> ExpendetureDetailsReportList = new List<ExpendetureDetailsReportListBOL>();

            var ExpenditureDetails = db.TRN_EXPENDETURE_SUMMARY.Join(db.TRN_EXPENDETURE_DETAILS, a => a.EXPENDETURE_NO, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b }).
                Join(db.EXPENDETURE_STATUS_LOG, ab => ab.a.EXPENDETURE_NO, c => c.REF_EXPENDETURE_NO, (ab, c) => new { ab, c })
                .Where(x => x.ab.a.ISACTIVE == true && x.ab.b.ISACTIVE == true && x.c.ISACTIVE == true)
                .Where(a => (ExpendetureBOL.ExpendetureNo != null && ExpendetureBOL.ExpendetureNo != "") ? a.ab.a.EXPENDETURE_NO == ExpendetureBOL.ExpendetureNo : true)
                .Where(a => (ExpendetureBOL.EmployeeOrAgencyName != null && ExpendetureBOL.EmployeeOrAgencyName != "") ? (a.ab.b.REF_USER_ID == ExpendetureBOL.EmployeeOrAgencyName) || (a.ab.b.REF_AGENCY_ID == ExpendetureBOL.EmployeeOrAgencyName) : true)
                .Where(a => (ExpendetureBOL.EmployeeName != null && ExpendetureBOL.EmployeeName != "") ? a.ab.b.REF_USER_ID == ExpendetureBOL.EmployeeName : true)
                .Where(a => (ExpendetureBOL.AgencyName != null && ExpendetureBOL.AgencyName != "") ? a.ab.b.REF_AGENCY_ID == ExpendetureBOL.AgencyName : true)
                .Where(a => (ExpendetureBOL.FromDate != null && ExpendetureBOL.FromDate != "") ? a.ab.a.JOURNEY_DATE >= FromDate : true)
                .Where(a => (ExpendetureBOL.ToDate != null && ExpendetureBOL.ToDate != "") ? a.ab.a.JOURNEY_DATE <= ToDate : true).
                Select(x => new
                {
                    ExpendetureNo = x.ab.a.EXPENDETURE_NO,
                    CostCenter = x.ab.b.MSTR_COST_CENTER.COST_CENTER_NAME,
                    ExpendetureHead = x.ab.b.MSTR_EXPENDETURE_FOR.EXPENDETURE_FOR_NAME,
                    ExpByUser = x.ab.b.REF_USER_ID,
                    ExpByAgency = x.ab.b.REF_AGENCY_ID,
                    Journeydate = x.ab.a.JOURNEY_DATE,
                    EntryAmount = x.ab.b.ENTRY_AMOUNT,
                    ClaimedAmount = x.ab.b.ClaimedAmount,
                    ApprovedAmount = x.ab.b.APPROVED_AMOUNT,
                    Status = x.c.MSTR_APPROVE_STATUS.APPROVE_STATUS_NAME
                }).OrderBy(x => x.Journeydate).OrderByDescending(x => x.ExpendetureNo).ToList();

            string[] ExpenditureNo = ExpenditureDetails.Select(x => x.ExpendetureNo).Distinct().ToArray();
            foreach (var ExpNo in ExpenditureNo)
            {

                var ParticularExpNo = ExpenditureDetails.Where(x => x.ExpendetureNo == ExpNo).ToList();
                if (ExpendetureBOL.ReportFor == "Employee")
                {
                    string[] UserIds = ParticularExpNo.Where(x => x.ExpByUser != null).Select(x => x.ExpByUser).Distinct().ToArray();
                    foreach (var UserId in UserIds)
                    {
                        var ParticularExpHead = ParticularExpNo.Where(x => x.ExpByUser == UserId).ToList();
                        foreach (var ExpHeadDetails in ParticularExpHead)
                        {
                            ExpendetureDetailsReportListBOL DetailsData = new ExpendetureDetailsReportListBOL();
                            DetailsData.ExpendetureNo = ExpHeadDetails.ExpendetureNo;
                            DetailsData.ReportFor = "Employee";
                            DetailsData.Name = db.MSTR_USER.Where(a => a.ISACTIVE == true && a.USER_ID == UserId).Select(a => a.FULL_NAME).FirstOrDefault();
                            DetailsData.VoucherDate = ExpHeadDetails.Journeydate;
                            DetailsData.ExpendetureHead = ExpHeadDetails.CostCenter + "(" + ExpHeadDetails.ExpendetureHead + ")";
                            DetailsData.EntryAmount = Convert.ToDecimal(ExpHeadDetails.EntryAmount);
                            DetailsData.ClaimedAmount = Convert.ToDecimal(ExpHeadDetails.ClaimedAmount);
                            DetailsData.ApprovedAmount = Convert.ToDecimal(ExpHeadDetails.ApprovedAmount);
                            DetailsData.Status = ExpHeadDetails.Status;
                            ExpendetureDetailsReportList.Add(DetailsData);
                        }
                    }
                }
                else if (ExpendetureBOL.ReportFor == "Agency")
                {
                    string[] AgencyIds = ParticularExpNo.Where(x => x.ExpByAgency != null).Select(x => x.ExpByAgency).Distinct().ToArray();
                    foreach (var AgencyId in AgencyIds)
                    {
                        var ParticularExpHead = ParticularExpNo.Where(x => x.ExpByAgency == AgencyId).ToList();
                        foreach (var ExpHeadDetails in ParticularExpHead)
                        {
                            ExpendetureDetailsReportListBOL DetailsData = new ExpendetureDetailsReportListBOL();
                            DetailsData.ExpendetureNo = ExpHeadDetails.ExpendetureNo;
                            DetailsData.ReportFor = "Agency";
                            DetailsData.Name = db.MSTR_AGENCY.Where(a => a.ISACTIVE == true && a.AGENCY_ID == AgencyId).Select(a => a.AGENCY_NAME).FirstOrDefault();
                            DetailsData.VoucherDate = ExpHeadDetails.Journeydate;
                            DetailsData.ExpendetureHead = ExpHeadDetails.CostCenter + "(" + ExpHeadDetails.ExpendetureHead + ")";
                            DetailsData.EntryAmount = Convert.ToDecimal(ExpHeadDetails.EntryAmount);
                            DetailsData.ClaimedAmount = Convert.ToDecimal(ExpHeadDetails.ClaimedAmount);
                            DetailsData.ApprovedAmount = Convert.ToDecimal(ExpHeadDetails.ApprovedAmount);
                            DetailsData.Status = ExpHeadDetails.Status;
                            ExpendetureDetailsReportList.Add(DetailsData);
                        }
                    }
                }
                else
                {
                    string[] UserIds = ParticularExpNo.Where(x => x.ExpByUser != null).Select(x => x.ExpByUser).Distinct().ToArray();
                    foreach (var UserId in UserIds)
                    {
                        var ParticularExpHead = ParticularExpNo.Where(x => x.ExpByUser == UserId).ToList();
                        foreach (var ExpHeadDetails in ParticularExpHead)
                        {
                            ExpendetureDetailsReportListBOL DetailsData = new ExpendetureDetailsReportListBOL();
                            DetailsData.ExpendetureNo = ExpHeadDetails.ExpendetureNo;
                            DetailsData.ReportFor = "Employee";
                            DetailsData.Name = db.MSTR_USER.Where(a => a.ISACTIVE == true && a.USER_ID == UserId).Select(a => a.FULL_NAME).FirstOrDefault();
                            DetailsData.VoucherDate = ExpHeadDetails.Journeydate;
                            DetailsData.ExpendetureHead = ExpHeadDetails.CostCenter + "(" + ExpHeadDetails.ExpendetureHead + ")";
                            DetailsData.EntryAmount = Convert.ToDecimal(ExpHeadDetails.EntryAmount);
                            DetailsData.ClaimedAmount = Convert.ToDecimal(ExpHeadDetails.ClaimedAmount);
                            DetailsData.ApprovedAmount = Convert.ToDecimal(ExpHeadDetails.ApprovedAmount);
                            DetailsData.Status = ExpHeadDetails.Status;
                            ExpendetureDetailsReportList.Add(DetailsData);
                        }
                    }

                    string[] AgencyIds = ParticularExpNo.Where(x => x.ExpByAgency != null).Select(x => x.ExpByAgency).Distinct().ToArray();
                    foreach (var AgencyId in AgencyIds)
                    {
                        var ParticularExpHead = ParticularExpNo.Where(x => x.ExpByAgency == AgencyId).ToList();
                        foreach (var ExpHeadDetails in ParticularExpHead)
                        {
                            ExpendetureDetailsReportListBOL DetailsData = new ExpendetureDetailsReportListBOL();
                            DetailsData.ExpendetureNo = ExpHeadDetails.ExpendetureNo;
                            DetailsData.ReportFor = "Agency";
                            DetailsData.Name = db.MSTR_AGENCY.Where(a => a.ISACTIVE == true && a.AGENCY_ID == AgencyId).Select(a => a.AGENCY_NAME).FirstOrDefault();
                            DetailsData.VoucherDate = ExpHeadDetails.Journeydate;
                            DetailsData.ExpendetureHead = ExpHeadDetails.CostCenter + "(" + ExpHeadDetails.ExpendetureHead + ")";
                            DetailsData.EntryAmount = Convert.ToDecimal(ExpHeadDetails.EntryAmount);
                            DetailsData.ClaimedAmount = Convert.ToDecimal(ExpHeadDetails.ClaimedAmount);
                            DetailsData.ApprovedAmount = Convert.ToDecimal(ExpHeadDetails.ApprovedAmount);
                            DetailsData.Status = ExpHeadDetails.Status;
                            ExpendetureDetailsReportList.Add(DetailsData);
                        }
                    }
                }
            }

            return ExpendetureDetailsReportList;
        }
    }
}