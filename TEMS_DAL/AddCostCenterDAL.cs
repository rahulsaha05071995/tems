﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TEMS_BOL;
using static TEMS_BOL.AddCostCenterBOL;

namespace TEMS_DAL
{
    public class AddCostCenterDAL
    {
        TEMSEntities db = new TEMSEntities();
        public List<SelectListItem> ListCity()
        {
            List<SelectListItem> CityList = new List<SelectListItem>();
            List<MSTR_CITY> CityData = db.MSTR_CITY.Where(x => x.ISACTIVE == true).ToList();
            CityData.ForEach(x => CityList.Add(new SelectListItem { Text = x.CITY_NAME, Value = x.CITY_ID }));
            return CityList;
        }

        public bool AddCostCenterData(AddCostCenterBOL costcenter, string USERID)
        {
            bool CheckAddCostCenter = true;

            try
            {
                var CostCenterID = db.MSTR_COST_CENTER.OrderByDescending(x => x.COST_CENTER_ID).FirstOrDefault();
                MSTR_COST_CENTER cost = new MSTR_COST_CENTER();
                if (CostCenterID == null)
                {
                    cost.COST_CENTER_ID = "0001";
                }
                else
                {
                    cost.COST_CENTER_ID = "0" + (Convert.ToInt32(CostCenterID.COST_CENTER_ID.Substring(1, CostCenterID.COST_CENTER_ID.Length - 1)) + 1).ToString("D3");
                }
                cost.REF_CITY_ID = costcenter.RefCityID;
                cost.COST_CENTER_NAME = costcenter.CostCenterName;
                cost.CREATED_BY = USERID;
                cost.CREATED_ON = DateTime.Now;
                cost.ISACTIVE = Convert.ToBoolean(costcenter.Status);

                db.MSTR_COST_CENTER.Add(cost);                
                db.SaveChanges();
            }

            catch (Exception ex)
            {
                CheckAddCostCenter = false;
            }
            return CheckAddCostCenter;
        }

        public List<SelectListItem> ListCostCenter()
        {
            List<SelectListItem> CostCenterList = new List<SelectListItem>();
            List<MSTR_COST_CENTER> CostCenterData = db.MSTR_COST_CENTER.Where(x => x.ISACTIVE == true).ToList();
            CostCenterData.ForEach(x => CostCenterList.Add(new SelectListItem { Text = x.COST_CENTER_NAME, Value = x.COST_CENTER_ID }));
            return CostCenterList;
        }

        public List<CostCenterData> ListCostCenterData(AddCostCenterBOL costcenter)
        {
            List<CostCenterData> CostCenterDataList = new List<CostCenterData>();

            var Data = db.MSTR_COST_CENTER.
                Where(x => (costcenter.RefCityID != null && costcenter.RefCityID != "") ? x.REF_CITY_ID == costcenter.RefCityID : true).
                Where(x => (costcenter.CostCenterName != null && costcenter.CostCenterName != "") ? x.COST_CENTER_ID == costcenter.CostCenterName : true).                
                Where(x => (costcenter.Status != null && costcenter.Status != "") ? x.ISACTIVE.ToString() == costcenter.Status : true).
                Select(x => new
                {                    
                    CityName = x.MSTR_CITY.CITY_NAME,
                    Id = x.COST_CENTER_ID,
                    CostCenterName = x.COST_CENTER_NAME,
                    Status = x.ISACTIVE
                }).ToList();

            foreach (var item in Data)
            {
                CostCenterData CC = new CostCenterData();
                CC.RefCityID = item.CityName;
                CC.CostCenterName = item.CostCenterName;
                CC.CostCenterID = item.Id;
                if (item.Status == true)
                {
                    CC.Status = "Active";
                }
                else
                {
                    CC.Status = "Inactive";
                }
                CostCenterDataList.Add(CC);
            }
            return CostCenterDataList;
        }

        public AddCostCenterBOL EditCostCenter(string CostCenterId)
        {
            AddCostCenterBOL costcenter = new AddCostCenterBOL();

            var Data = db.MSTR_COST_CENTER.Where(x => x.COST_CENTER_ID == CostCenterId).ToList();

            if (Data.Count > 0)
            {
                costcenter.RefCityID = Data[0].REF_CITY_ID;
                costcenter.CostCenterName = Data[0].COST_CENTER_NAME;
                costcenter.Status = Convert.ToString(Data[0].ISACTIVE);
            }
            return costcenter;
        }

        public bool EditCostCenterdata(AddCostCenterBOL costcenter, string USERID)
        {
            bool EditCostCenterdata = true;
            try
            {
                MSTR_COST_CENTER Data = db.MSTR_COST_CENTER.FirstOrDefault(x => x.COST_CENTER_ID == costcenter.CostCenterID);
                if (Data != null)
                {
                    Data.REF_CITY_ID = costcenter.RefCityID;
                    Data.COST_CENTER_NAME = costcenter.CostCenterName;
                    Data.UPDATED_BY = USERID;
                    Data.UPDATED_ON = DateTime.Now;
                    Data.ISACTIVE = Convert.ToBoolean(costcenter.Status);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                EditCostCenterdata = false;
            }
            return EditCostCenterdata;
        }
    }
}
