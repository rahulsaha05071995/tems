﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TEMS_BOL;
using static TEMS_BOL.PayAmountBOL;

namespace TEMS_DAL
{
    public class PayAmountDAL
    {
        TEMSEntities db = new TEMSEntities();
        public List<SelectListItem> ListExpID()
        {
            List<SelectListItem> ExpIDList = new List<SelectListItem>();
            var ExpIDData = db.TRN_EXPENDITURE_LOG.Where(x => x.IsActive == true && x.PAYMENT_STATUS == "Unpaid").Select(x => new { ExpNo = x.EXPENDITURE_NO }).Distinct().ToList();
            ExpIDData.ForEach(item => ExpIDList.Add(new SelectListItem { Text = item.ExpNo, Value = item.ExpNo }));
            return ExpIDList;
        }

        public List<SearchPayment> SearchPaymentDAL(PayAmountBOL payment)
        {
            List<SearchPayment> TableDataList = new List<SearchPayment>();

            if(payment.VoucherNo != null)
            {
                var TableData = db.TRN_LEDGER.
                Where(x => x.IsActive == true).
                Where(x => (payment.VoucherNo != null && payment.VoucherNo != "") ? x.VoucherNo == payment.VoucherNo : true).
                ToList();
                foreach (var item in TableData)
                {
                    SearchPayment pay = new SearchPayment();
                    pay.VoucherNo = item.VoucherNo;
                    pay.VoucherDate = String.Format("{0:dd/MM/yyyy}", item.EffectiveDate);
                    if (item.VoucherNo.StartsWith("ADV"))
                    {
                        pay.PaymentType = "Advance";
                    }
                    else
                    {
                        pay.PaymentType = "Payment";
                    }
                    pay.PaymentMode = "Online";
                    pay.PaymentTo = item.MSTR_USER.FULL_NAME;
                    pay.Amount = Convert.ToDecimal(item.DrAmount);
                    pay.CreatedDate = String.Format("{0:dd/MM/yyyy}", item.CreatedOn);
                    TableDataList.Add(pay);
                }
            }
            

            else if(payment.VoucherNo == null && payment.PaymentType == "Payment")
            {
                DateTime FromDate = (payment.FromDate != null && payment.FromDate != "") ? DateTime.ParseExact(payment.FromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now;
                DateTime ToDate = (payment.FromDate != null && payment.ToDate != "") ? DateTime.ParseExact(payment.ToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now;


                var data = db.TRN_Payment
                    .Join(db.MSTR_USER,
                        c => c.RefUserId,
                        o => o.USER_ID,
                        (c, o) => new { c, o }).Where(x => x.c.IsActive == true && x.o.ISACTIVE == true).
                        Where(x => (payment.PaymentMode != null && payment.PaymentMode != "") ? x.c.ModeOfPayment == payment.PaymentMode : true).
                        Where(x => (payment.PaymentTo != null && payment.PaymentTo != "") ? x.c.RefUserId == payment.PaymentTo : true).
                        Where(x => (payment.AgencyName != null && payment.AgencyName != "") ? x.c.RefAgencyId == payment.AgencyName : true).
                        Where(x => (payment.FromDate != null && payment.FromDate != "") ? x.c.CreatedOn >= FromDate : true).
                        Where(x => (payment.ToDate != null && payment.ToDate != "") ? x.c.CreatedOn <= ToDate : true).
                        ToList();

                foreach(var item in data)
                {
                    SearchPayment pay = new SearchPayment();
                    pay.VoucherNo = item.c.PaymentNumber;
                    pay.VoucherDate = String.Format("{0:dd/MM/yyyy}", item.c.EffectiveDate);
                    pay.PaymentType = "Payment";                    
                    pay.PaymentMode = item.c.ModeOfPayment;
                    if (payment.PaymentFor == "Employee")
                    {
                        pay.PaymentTo = item.o.FULL_NAME;
                    }
                    else
                    {
                        pay.PaymentTo = item.c.MSTR_AGENCY.AGENCY_NAME;
                    }
                    pay.Amount = Convert.ToDecimal(item.c.PaymentAmount + item.c.AdvAdjAmt);
                    pay.CreatedDate = String.Format("{0:dd/MM/yyyy}", item.c.CreatedOn);
                    TableDataList.Add(pay);
                }
            }

            else if(payment.VoucherNo == null && payment.PaymentType == null && payment.PaymentMode == null)
            {
                DateTime FromDate = (payment.FromDate != null && payment.FromDate != "") ? DateTime.ParseExact(payment.FromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now;
                DateTime ToDate = (payment.FromDate != null && payment.ToDate != "") ? DateTime.ParseExact(payment.ToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now;


                var data = db.TRN_Payment
                    .Join(db.MSTR_USER,
                        c => c.RefUserId,
                        o => o.USER_ID,
                        (c, o) => new { c, o }).Where(x => x.c.IsActive == true && x.o.ISACTIVE == true).
                        Where(x => (payment.PaymentMode != null && payment.PaymentMode != "") ? x.c.ModeOfPayment == payment.PaymentMode : true).
                        Where(x => (payment.PaymentTo != null && payment.PaymentTo != "") ? x.c.RefUserId == payment.PaymentTo : true).
                        Where(x => (payment.AgencyName != null && payment.AgencyName != "") ? x.c.RefAgencyId == payment.AgencyName : true).
                        Where(x => (payment.FromDate != null && payment.FromDate != "") ? x.c.CreatedOn >= FromDate : true).
                        Where(x => (payment.ToDate != null && payment.ToDate != "") ? x.c.CreatedOn <= ToDate : true).
                        ToList();

                foreach (var item in data)
                {
                    SearchPayment pay = new SearchPayment();
                    pay.VoucherNo = item.c.PaymentNumber;
                    pay.VoucherDate = String.Format("{0:dd/MM/yyyy}", item.c.EffectiveDate);
                    pay.PaymentType = "Payment";
                    pay.PaymentMode = item.c.ModeOfPayment;
                    if (payment.PaymentFor == "Employee")
                    {
                        pay.PaymentTo = item.o.FULL_NAME;
                    }
                    else
                    {
                        pay.PaymentTo = item.c.MSTR_AGENCY.AGENCY_NAME;
                    }
                    pay.Amount = Convert.ToDecimal(item.c.PaymentAmount + item.c.AdvAdjAmt);
                    pay.CreatedDate = String.Format("{0:dd/MM/yyyy}", item.c.CreatedOn);
                    TableDataList.Add(pay);
                }


                var data1 = db.TRN_Advance
                    .Join(db.MSTR_USER,
                        c => c.RefUserId,
                        o => o.USER_ID,
                        (c, o) => new { c, o }).Where(x => x.c.IsActive == true && x.o.ISACTIVE == true).
                        Where(x => (payment.PaymentMode != null && payment.PaymentMode != "") ? x.c.ModeOfPayment == payment.PaymentMode : true).
                        Where(x => (payment.PaymentTo != null && payment.PaymentTo != "") ? x.c.RefUserId == payment.PaymentTo : true).
                        Where(x => (payment.AgencyName != null && payment.AgencyName != "") ? x.c.RefAgencyId == payment.AgencyName : true).
                        Where(x => (payment.FromDate != null && payment.FromDate != "") ? x.c.CreatedOn >= FromDate : true).
                        Where(x => (payment.ToDate != null && payment.ToDate != "") ? x.c.CreatedOn <= ToDate : true).
                        ToList();


                foreach (var item in data1)
                {
                    SearchPayment pay = new SearchPayment();
                    pay.VoucherNo = item.c.AdvanceNumber;
                    pay.VoucherDate = String.Format("{0:dd/MM/yyyy}", item.c.EffectiveDate);
                    pay.PaymentType = "Advance";
                    pay.PaymentMode = item.c.ModeOfPayment;
                    if (payment.PaymentFor == "Employee")
                    {
                        pay.PaymentTo = item.o.FULL_NAME;
                    }
                    else
                    {
                        pay.PaymentTo = item.c.MSTR_AGENCY.AGENCY_NAME;
                    }
                    pay.Amount = Convert.ToDecimal(item.c.AdvanceAmount);
                    pay.CreatedDate = String.Format("{0:dd/MM/yyyy}", item.c.CreatedOn);
                    TableDataList.Add(pay);
                }
                return TableDataList;
            }

            else
            {
                DateTime FromDate = (payment.FromDate != null && payment.FromDate != "") ? DateTime.ParseExact(payment.FromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now;
                DateTime ToDate = (payment.FromDate != null && payment.ToDate != "") ? DateTime.ParseExact(payment.ToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now;


                var data = db.TRN_Advance
                    .Join(db.MSTR_USER,
                        c => c.RefUserId,
                        o => o.USER_ID,
                        (c, o) => new { c, o }).Where(x => x.c.IsActive == true && x.o.ISACTIVE == true).
                        Where(x => (payment.PaymentMode != null && payment.PaymentMode != "") ? x.c.ModeOfPayment == payment.PaymentMode : true).
                        Where(x => (payment.PaymentTo != null && payment.PaymentTo != "") ? x.c.RefUserId == payment.PaymentTo : true).
                        Where(x => (payment.AgencyName != null && payment.AgencyName != "") ? x.c.RefAgencyId == payment.AgencyName : true).
                        Where(x => (payment.FromDate != null && payment.FromDate != "") ? x.c.CreatedOn >= FromDate : true).
                        Where(x => (payment.ToDate != null && payment.ToDate != "") ? x.c.CreatedOn <= ToDate : true).
                        ToList();


                foreach (var item in data)
                {
                    SearchPayment pay = new SearchPayment();
                    pay.VoucherNo = item.c.AdvanceNumber;
                    pay.VoucherDate = String.Format("{0:dd/MM/yyyy}", item.c.EffectiveDate);
                    pay.PaymentType = "Advance";
                    pay.PaymentMode = item.c.ModeOfPayment;
                    if (payment.PaymentFor == "Employee")
                    {
                        pay.PaymentTo = item.o.FULL_NAME;
                    }
                    else
                    {
                        pay.PaymentTo = item.c.MSTR_AGENCY.AGENCY_NAME;
                    }
                    pay.Amount = Convert.ToDecimal(item.c.AdvanceAmount);
                    pay.CreatedDate = String.Format("{0:dd/MM/yyyy}", item.c.CreatedOn);
                    TableDataList.Add(pay);
                }
            }

            return TableDataList;
        }


        public bool FetchPaymentData(PayAmountBOL payment, string USERNAME)
        {
            bool PaymentData = true;
            //try
            //{
            if (payment.PaymentType == "Advance")
                {
                    TRN_Advance advance = new TRN_Advance();
                    var AdvanceID = db.TRN_Advance.OrderByDescending(x => x.AdvanceId).FirstOrDefault();
                    if (AdvanceID == null)
                    {
                        advance.AdvanceId = "1001";
                    }
                    else
                    {
                        advance.AdvanceId = (Convert.ToInt32(AdvanceID.AdvanceId) + 1).ToString(); /*(Convert.ToInt32(LedgerID.LEDGERID.Substring(4, LedgerID.LEDGERID.Length - 4)) + 1).ToString("D5");*/
                    }
                    var AdvanceNo = db.TRN_Advance.OrderByDescending(x => x.AdvanceNumber).FirstOrDefault();
                    var DT = DateTime.Today;

                    if (AdvanceID == null)
                    {
                        advance.AdvanceNumber = "ADV-" + DT.ToString("yy") + DT.AddYears(1).ToString("yy") + "-" + "00001";
                    }
                    else
                    {
                        advance.AdvanceNumber = "ADV-" + DT.ToString("yy") + DT.AddYears(1).ToString("yy") + "-" + (Convert.ToInt32(AdvanceNo.AdvanceNumber.Substring(9, AdvanceNo.AdvanceNumber.Length - 9)) + 1).ToString("D5");

                    }
                    if(payment.PaymentFor == "Employee")
                    {
                        advance.RefUserId = payment.PaymentTo;
                    }
                    else
                    {
                        advance.RefAgencyId = payment.AgencyName;
                    }
                    
                    advance.ModeOfPayment = payment.PaymentMode;
                    advance.AdvanceAmount = Convert.ToDecimal(payment.Amount);
                    advance.RemainingAmount = Convert.ToString(payment.Amount);
                    advance.EffectiveDate = DateTime.ParseExact(payment.PaymentDate, "dd/MM/yyyy", CultureInfo.InvariantCulture); ;
                    advance.CreatedBy = USERNAME;
                    advance.CreatedOn = DateTime.Now;
                    advance.IsActive = true;
                    db.TRN_Advance.Add(advance);
                    var Refadvanceid = advance.AdvanceId;
                    var voucherno = advance.AdvanceNumber;

                    TRN_LEDGER ledger = new TRN_LEDGER();
                    var LedgerID = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).FirstOrDefault();
                    if (LedgerID == null)
                    {
                        ledger.LEDGERID = 1;
                    }
                    else
                    {
                        ledger.LEDGERID = (Convert.ToInt32(LedgerID.LEDGERID) + 1);
                    }
                ledger.DrAmount = Convert.ToDecimal(payment.Amount);
                ledger.CrAmount = 0;
                if (payment.PaymentFor == "Employee")
                    {
                        ledger.REF_USER_ID = payment.PaymentTo;
                        var lastbalanceuser = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).Where(x => x.IsActive == true && x.REF_USER_ID == payment.PaymentTo).FirstOrDefault();
                        if (lastbalanceuser != null)
                        {
                            if (lastbalanceuser.BalanceAmount != null)
                            {
                                ledger.BalanceAmount = (Convert.ToDecimal(lastbalanceuser.BalanceAmount) - Convert.ToDecimal(ledger.DrAmount));
                            }
                            else
                            {
                                ledger.BalanceAmount = ledger.DrAmount;
                            }
                        }
                        else
                        {
                            ledger.BalanceAmount = -ledger.DrAmount;
                        }
                    }                
                    else
                    {
                        ledger.REF_AGENCY_ID = payment.AgencyName;
                        var lastbalanceagency = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).Where(x => x.IsActive == true && x.REF_AGENCY_ID == payment.AgencyName).FirstOrDefault();
                        if(lastbalanceagency != null)
                        {
                            if (lastbalanceagency.BalanceAmount != null)
                            {
                                ledger.BalanceAmount = (Convert.ToDecimal(lastbalanceagency.BalanceAmount) - Convert.ToDecimal(ledger.DrAmount));
                            }
                            else
                            {
                                ledger.BalanceAmount = ledger.DrAmount;
                            }
                        }
                        else
                        {
                            ledger.BalanceAmount = -ledger.DrAmount;
                        }
                    }
                    
                    ledger.RefAdvanceId = Refadvanceid;
                    ledger.VoucherNo = voucherno;
                    
                    ledger.EffectiveDate = DateTime.ParseExact(payment.PaymentDate,"dd/MM/yyyy", CultureInfo.InvariantCulture);
                    ledger.CreatedBy = USERNAME;
                    ledger.CreatedOn = DateTime.Now;
                    ledger.IsActive = true;
                    db.TRN_LEDGER.Add(ledger);
                }
            else
            {                
                if (Convert.ToDecimal(payment.ExpAmount) == Convert.ToDecimal(payment.Amount))
                {
                    TRN_Payment pay = new TRN_Payment();
                    var RefExpLogId = db.TRN_EXPENDITURE_LOG.Where(x => x.IsActive == true && x.EXPENDITURE_NO == payment.ExpID).Select(x => x.EXPENDITURE_LOG_ID).FirstOrDefault();
                    var PaymentId = db.TRN_Payment.OrderByDescending(x => x.PaymentId).FirstOrDefault();
                    var DT = DateTime.Today;
                    if (PaymentId == null)
                    {
                        pay.PaymentId = 1001;
                        pay.PaymentNumber = "PAY-" + DT.ToString("yy") + DT.AddYears(1).ToString("yy") + "-" + "00001";
                    }
                    else
                    {
                        pay.PaymentId = (Convert.ToInt32(PaymentId.PaymentId) + 1);
                        pay.PaymentNumber = "PAY-" + DT.ToString("yy") + DT.AddYears(1).ToString("yy") + "-" +
                            (Convert.ToInt64(PaymentId.PaymentNumber.Substring(9, PaymentId.PaymentNumber.Length - 9)) + 1).ToString("D5");
                    }
                    if (payment.PaymentFor == "Employee")
                    {
                        pay.RefUserId = payment.PaymentTo;
                    }
                    else
                    {
                        pay.RefAgencyId = payment.AgencyName;
                    }
                    pay.PaymentAmount = payment.Amount;
                    pay.EffectiveDate = DateTime.ParseExact(payment.PaymentDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    pay.CreatedBy = USERNAME;
                    pay.CreatedOn = DateTime.Now;
                    pay.ModeOfPayment = payment.PaymentMode;
                    pay.IsActive = true;
                    pay.AdvAdjAmt = 0;
                    pay.RefExpenditureLogId = RefExpLogId;
                    db.TRN_Payment.Add(pay);

                    var RefPaymentId = pay.PaymentId;

                    TRN_LEDGER ledger = new TRN_LEDGER();
                    var LedgerID = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).FirstOrDefault();
                    if (LedgerID == null)
                    {
                        ledger.LEDGERID = 1;
                    }
                    else
                    {
                        ledger.LEDGERID = (Convert.ToInt32(LedgerID.LEDGERID) + 1);
                    }
                    if (payment.PaymentFor == "Employee")
                    {
                        ledger.REF_USER_ID = payment.PaymentTo;
                    }
                    else
                    {
                        ledger.REF_AGENCY_ID = payment.AgencyName;
                    }

                    ledger.RefPaymentId = RefPaymentId.ToString();
                    ledger.VoucherNo = pay.PaymentNumber;
                    ledger.DrAmount = Convert.ToDecimal(payment.Amount);
                    ledger.CrAmount = 0;

                    var lastbalanceuser = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).Where(x => x.IsActive == true && x.REF_USER_ID == payment.PaymentTo).FirstOrDefault();
                    if (lastbalanceuser != null)
                    {
                        if (lastbalanceuser.BalanceAmount != null)
                        {
                            ledger.BalanceAmount = (Convert.ToDecimal(lastbalanceuser.BalanceAmount) - Convert.ToDecimal(ledger.DrAmount));
                        }
                        else
                        {
                            ledger.BalanceAmount = ledger.DrAmount;
                        }
                    }
                    else
                    {
                        var lastbalanceagency = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).Where(x => x.IsActive == true && x.REF_AGENCY_ID == payment.AgencyName).FirstOrDefault();
                        if (lastbalanceagency != null)
                        {
                            if (lastbalanceagency.BalanceAmount != null)
                            {
                                ledger.BalanceAmount = (Convert.ToDecimal(lastbalanceagency.BalanceAmount) - Convert.ToDecimal(ledger.DrAmount));
                            }
                            else
                            {
                                ledger.BalanceAmount = ledger.DrAmount;
                            }
                        }
                        else
                        {
                            ledger.BalanceAmount = -ledger.DrAmount;
                        }

                    }
                    ledger.EffectiveDate = DateTime.ParseExact(payment.PaymentDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    ledger.CreatedBy = USERNAME;
                    ledger.CreatedOn = DateTime.Now;
                    ledger.IsActive = true;
                    db.TRN_LEDGER.Add(ledger);


                    List<TRN_EXPENDETURE_DETAILS> RefExpForId = db.TRN_EXPENDETURE_DETAILS.Where(x => x.ISACTIVE == true && x.REF_EXPENDETURE_NO == payment.ExpID && x.REF_USER_ID == payment.PaymentTo).ToList();
                    List<TRN_ExpenduterPaymentMap> Listmap = new List<TRN_ExpenduterPaymentMap>();
                    
                    foreach (var item in RefExpForId)
                    {
                        TRN_ExpenduterPaymentMap map = new TRN_ExpenduterPaymentMap();

                        map.RefExpenduterLogId = RefExpLogId;
                        map.RefExpenduterForId = item.REF_EXPENDETURE_FOR_ID;
                        if (payment.PaymentFor == "Employee")
                        {
                            map.RefUserId = payment.PaymentTo;
                        }
                        else
                        {
                            map.RefAgencyId = payment.AgencyName;
                        }
                        map.CrAmount = item.APPROVED_AMOUNT.ToString();
                        map.DrAmount = item.APPROVED_AMOUNT.ToString();
                        map.RefPaymentId = RefPaymentId.ToString();
                        map.CreatedBy = USERNAME;
                        map.CreatedOn = DateTime.Now;
                        map.IsActive = true;
                        Listmap.Add(map);
                    }
                    if (Listmap.Count > 0)
                    {
                        db.TRN_ExpenduterPaymentMap.AddRange(Listmap);
                    }


                    if (payment.PaymentFor == "Employee")
                    {
                        TRN_EXPENDITURE_LOG log = db.TRN_EXPENDITURE_LOG.FirstOrDefault(x => x.IsActive == true && x.EXPENDITURE_NO == payment.ExpID && x.REF_USER_ID == payment.PaymentTo);
                        if (log != null)
                        {
                            log.REMAINING_AMOUNT = "0.00";
                            log.PAYMENT_STATUS = "Paid";
                            log.UpdatedBy = USERNAME;
                            log.UpdatedOn = DateTime.Now;
                        }
                    }
                    else
                    {
                        TRN_EXPENDITURE_LOG log = db.TRN_EXPENDITURE_LOG.FirstOrDefault(x => x.IsActive == true && x.EXPENDITURE_NO == payment.ExpID && x.REF_AGENCY_ID == payment.AgencyName);
                        if (log != null)
                        {
                            log.REMAINING_AMOUNT = "0.00";
                            log.PAYMENT_STATUS = "Paid";
                            log.UpdatedBy = USERNAME;
                            log.UpdatedOn = DateTime.Now;
                        }
                    }
                }

                else
                {
                    TRN_Payment pay = new TRN_Payment();
                    var RefExpLogId = db.TRN_EXPENDITURE_LOG.Where(x => x.IsActive == true && x.EXPENDITURE_NO == payment.ExpID).Select(x => x.EXPENDITURE_LOG_ID).FirstOrDefault();
                    var PaymentId = db.TRN_Payment.OrderByDescending(x => x.PaymentId).FirstOrDefault();
                    var DT = DateTime.Today;
                    if (PaymentId == null)
                    {
                        pay.PaymentId = 1001;
                        pay.PaymentNumber = "PAY-" + DT.ToString("yy") + DT.AddYears(1).ToString("yy") + "-" + "00001";
                    }
                    else
                    {
                        pay.PaymentId = (Convert.ToInt32(PaymentId.PaymentId) + 1);
                        pay.PaymentNumber = "PAY-" + DT.ToString("yy") + DT.AddYears(1).ToString("yy") + "-" +
                            (Convert.ToInt64(PaymentId.PaymentNumber.Substring(9, PaymentId.PaymentNumber.Length - 9)) + 1).ToString("D5");
                    }
                    if (payment.PaymentFor == "Employee")
                    {
                        pay.RefUserId = payment.PaymentTo;
                    }
                    else
                    {
                        pay.RefAgencyId = payment.AgencyName;
                    }
                    pay.PaymentAmount = Convert.ToDecimal(payment.Amount);
                    pay.EffectiveDate = DateTime.ParseExact(payment.PaymentDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    pay.CreatedBy = USERNAME;
                    pay.CreatedOn = DateTime.Now;
                    pay.ModeOfPayment = payment.PaymentMode;
                    pay.IsActive = true;
                    pay.RefExpenditureLogId = RefExpLogId;
                    if (Convert.ToDecimal(payment.ExpAmount) > Convert.ToDecimal(payment.AdvancedAmount))
                    {
                        pay.AdvAdjAmt = Convert.ToDecimal(payment.AdvancedAmount);
                    }
                    else
                    {
                        pay.AdvAdjAmt = Convert.ToDecimal(payment.ExpAmount);
                    }
                    db.TRN_Payment.Add(pay);

                    var LedgerPaymentAmount = Convert.ToDecimal(pay.PaymentAmount) + Convert.ToDecimal(pay.AdvAdjAmt);

                    var RefPayId = pay.PaymentId;
                    var PaymentNo = pay.PaymentNumber;

                    if (payment.PaymentFor == "Employee")
                    {
                        
                        var AdvCount = db.TRN_Advance.Where(x => x.IsActive == true && x.RemainingAmount != "0.00" && x.RefUserId == payment.PaymentTo).ToList();
                        var count = AdvCount.Count();
                        var Amount = Convert.ToDecimal(payment.ExpAmount);

                        List<TRN_Advance_Adjustment> AdvAdj = new List<TRN_Advance_Adjustment>();
                        List<TRN_LEDGER> ListLedger = new List<TRN_LEDGER>();                        
                        var cnt = 1;
                        decimal balanceAmount = 0;
                        
                            for (int i = 0; i < count; i++)
                            {
                                if (Amount > 0)
                                {
                                    var RefAdvanceId = AdvCount[i].AdvanceId;
                                    decimal AdvAdjAmt = 0;
                                                                
                                    if(Amount > 0)
                                    {
                                        var AmountIndexwise = Convert.ToDecimal(AdvCount[i].RemainingAmount);
                                        if (Amount >= Convert.ToDecimal(AdvCount[i].RemainingAmount))
                                        {
                                            AdvAdjAmt = Convert.ToDecimal(AdvCount[i].RemainingAmount);
                                            AdvCount[i].RemainingAmount = "0.00";
                                        }
                                        else
                                        {
                                            AdvAdjAmt = Amount;
                                            AdvCount[i].RemainingAmount = (Convert.ToDecimal(AdvCount[i].RemainingAmount) - Amount/*Convert.ToDecimal(payment.ExpAmount)*/).ToString();
                                        }
                                        AdvCount[i].UpdatedBy = USERNAME;
                                        AdvCount[i].UpdatedOn = DateTime.Now;

                                        balanceAmount = Convert.ToDecimal(balanceAmount) + Convert.ToDecimal(AdvAdjAmt);
                                        Amount = Amount - AmountIndexwise;
                                    }
                                
                                    TRN_Advance_Adjustment Adv = new TRN_Advance_Adjustment();
                                    Adv.AdvanceAdjId = payment.AdvanceAdjId;
                                    var AdvAdjNo = db.TRN_Advance_Adjustment.OrderByDescending(x => x.AdvanceAdjNumber).FirstOrDefault();
                                    if(AdvAdjNo == null)
                                    {
                                        Adv.AdvanceAdjNumber = "AdvAdj-" + DT.ToString("yy") + DT.AddYears(1).ToString("yy") + "-" + "00001"; ;
                                    }
                                    else
                                    {
                                        Adv.AdvanceAdjNumber = "AdvAdj-" + DT.ToString("yy") + DT.AddYears(1).ToString("yy") + "-" +
                                            (Convert.ToInt64(AdvAdjNo.AdvanceAdjNumber.Substring(12, AdvAdjNo.AdvanceAdjNumber.Length - 12)) + 1).ToString("D5");
                                    }
                                var Refadvadjno = Adv.AdvanceAdjNumber;
                                
                                    Adv.RefAdvanceId = RefAdvanceId;
                                    Adv.RefPaymentId = RefPayId;
                                    Adv.Amount = AdvAdjAmt.ToString();
                                    Adv.CreatedBy = USERNAME;
                                    Adv.CreatedOn = DateTime.Now;
                                    Adv.IsActive = true;
                                    AdvAdj.Add(Adv);

                                    TRN_LEDGER Ledger = new TRN_LEDGER();
                                    var LedgerID = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).FirstOrDefault();
                                    if (LedgerID == null)
                                    {
                                        Ledger.LEDGERID = 1;
                                    }
                                    else
                                    {
                                        Ledger.LEDGERID = (Convert.ToInt32(LedgerID.LEDGERID) + (i + 1));
                                    }

                                    Ledger.REF_USER_ID = payment.PaymentTo;
                                    Ledger.REF_EXPENDITURE_LOG_ID = RefExpLogId;
                                    Ledger.RefAdvAdjId = payment.AdvanceAdjId.ToString();
                                    Ledger.VoucherNo = Refadvadjno;
                                    Ledger.DrAmount = 0;
                                    Ledger.CrAmount = AdvAdjAmt;
                                    var lastbalanceuser = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).Where(x => x.IsActive == true && x.REF_USER_ID == payment.PaymentTo).FirstOrDefault();
                                    if (lastbalanceuser != null)
                                    {
                                        if (lastbalanceuser.BalanceAmount != null)
                                        {
                                            Ledger.BalanceAmount = (Convert.ToDecimal(lastbalanceuser.BalanceAmount) + balanceAmount);
                                        }
                                        else
                                        {
                                            Ledger.BalanceAmount =  Ledger.CrAmount;
                                        }
                                    }
                                    Ledger.EffectiveDate = DateTime.ParseExact(payment.PaymentDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    Ledger.CreatedBy = USERNAME;
                                    Ledger.CreatedOn = DateTime.Now;
                                    Ledger.IsActive = true;

                                    ListLedger.Add(Ledger);

                                    payment.AdvanceAdjId = payment.AdvanceAdjId + 1;
                                    cnt = cnt + i;
                                }
                            }
                        if (AdvAdj.Count > 0)
                        {
                            db.TRN_Advance_Adjustment.AddRange(AdvAdj);
                        }
                        if (ListLedger.Count > 0)
                        {
                            db.TRN_LEDGER.AddRange(ListLedger);
                        }                        

                        TRN_LEDGER Led = new TRN_LEDGER();

                        var LedID = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).FirstOrDefault();
                        if (LedID == null)
                        {
                            Led.LEDGERID = 1;
                        }
                        else
                        {
                            Led.LEDGERID = (Convert.ToInt32(LedID.LEDGERID) + (cnt + 1));
                        }
                        
                        Led.REF_USER_ID = payment.PaymentTo;
                        Led.REF_EXPENDITURE_LOG_ID = RefExpLogId;
                        Led.RefPaymentId = RefPayId.ToString();
                        Led.VoucherNo = PaymentNo;
                        Led.DrAmount = LedgerPaymentAmount;
                        Led.CrAmount = 0;
                        var lastbalanceuseradjust = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).Where(x => x.IsActive == true && x.REF_USER_ID == payment.PaymentTo).FirstOrDefault();
                        if (lastbalanceuseradjust != null)
                        {
                            if (lastbalanceuseradjust.BalanceAmount != null)
                            {
                                Led.BalanceAmount = ((Convert.ToDecimal(ListLedger[ListLedger.Count - 1].BalanceAmount)) - (Convert.ToDecimal(payment.ExpAmount)));
                            }
                            else
                            {
                                Led.BalanceAmount = Convert.ToDecimal(payment.ExpAmount);
                            }
                        }
                        Led.EffectiveDate = DateTime.ParseExact(payment.PaymentDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        Led.CreatedBy = USERNAME;
                        Led.CreatedOn = DateTime.Now;
                        Led.IsActive = true;
                        db.TRN_LEDGER.Add(Led);

                        List<TRN_EXPENDETURE_DETAILS> RefExpForId = db.TRN_EXPENDETURE_DETAILS.Where(x => x.ISACTIVE == true && x.REF_EXPENDETURE_NO == payment.ExpID && x.REF_USER_ID == payment.PaymentTo).ToList();
                        List<TRN_ExpenduterPaymentMap> Listmap = new List<TRN_ExpenduterPaymentMap>();
                        //var RefExpLogId = db.TRN_EXPENDITURE_LOG.Where(x => x.IsActive == true && x.EXPENDITURE_NO == payment.ExpID).Select(x => x.EXPENDITURE_LOG_ID).FirstOrDefault();
                        foreach (var item in RefExpForId)
                        {
                            TRN_ExpenduterPaymentMap map = new TRN_ExpenduterPaymentMap();

                            map.RefExpenduterLogId = RefExpLogId;
                            map.RefExpenduterForId = item.REF_EXPENDETURE_FOR_ID;
                            map.RefUserId = payment.PaymentTo;
                            map.CrAmount = item.APPROVED_AMOUNT.ToString();
                            map.DrAmount = item.APPROVED_AMOUNT.ToString();
                            map.RefPaymentId = RefPayId.ToString();
                            map.CreatedBy = USERNAME;
                            map.CreatedOn = DateTime.Now;
                            map.IsActive = true;
                            Listmap.Add(map);
                        }
                        if (Listmap.Count > 0)
                        {
                            db.TRN_ExpenduterPaymentMap.AddRange(Listmap);
                        }

                        TRN_EXPENDITURE_LOG log = db.TRN_EXPENDITURE_LOG.FirstOrDefault(x => x.IsActive == true && x.EXPENDITURE_NO == payment.ExpID && x.REF_USER_ID == payment.PaymentTo);
                        if (log != null)
                        {
                            log.REMAINING_AMOUNT = "0.00";
                            log.PAYMENT_STATUS = "Paid";
                            log.UpdatedBy = USERNAME;
                            log.UpdatedOn = DateTime.Now;
                        }
                    }
                    else
                    {
                        var AdvCount = db.TRN_Advance.Where(x => x.IsActive == true && x.RemainingAmount != "0" && x.RefAgencyId == payment.AgencyName).ToList();
                        var count = AdvCount.Count();
                        var Amount = Convert.ToDecimal(payment.ExpAmount);

                        List<TRN_Advance_Adjustment> AdvAdj = new List<TRN_Advance_Adjustment>();
                        List<TRN_LEDGER> ListLedger = new List<TRN_LEDGER>();
                        
                        var cnt = 1;
                        decimal balanceAmount = 0;
                        while (Amount > 0)
                        {
                            for (int i = 0; i < count; i++)
                            {
                                var RefAdvanceId = AdvCount[i].AdvanceId;
                                var AdvAdjAmt = "";

                                if (Amount > 0)
                                {
                                    var AmountIndexwise = Convert.ToDecimal(AdvCount[i].RemainingAmount);
                                    if (Amount >= Convert.ToDecimal(AdvCount[i].RemainingAmount))
                                    {
                                        AdvAdjAmt = AdvCount[i].RemainingAmount;
                                        AdvCount[i].RemainingAmount = "0.00";
                                    }
                                    else
                                    {
                                        AdvAdjAmt = Amount.ToString();
                                        AdvCount[i].RemainingAmount = (Convert.ToDecimal(AdvCount[i].RemainingAmount) - Amount/*Convert.ToDecimal(payment.ExpAmount)*/).ToString();
                                    }
                                    AdvCount[i].UpdatedBy = USERNAME;
                                    AdvCount[i].UpdatedOn = DateTime.Now;

                                    balanceAmount = Convert.ToDecimal(balanceAmount) + Convert.ToDecimal(AdvAdjAmt);
                                    Amount = Amount - AmountIndexwise;
                                }

                                TRN_Advance_Adjustment Adv = new TRN_Advance_Adjustment();
                                Adv.AdvanceAdjId = payment.AdvanceAdjId;
                                var AdvAdjNo = db.TRN_Advance_Adjustment.OrderByDescending(x => x.AdvanceAdjNumber).FirstOrDefault();
                                if (AdvAdjNo == null)
                                {
                                    Adv.AdvanceAdjNumber = "AdvAdj-" + DT.ToString("yy") + DT.AddYears(1).ToString("yy") + "-" + "00001"; ;
                                }
                                else
                                {
                                    Adv.AdvanceAdjNumber = "AdvAdj-" + DT.ToString("yy") + DT.AddYears(1).ToString("yy") + "-" +
                                        (Convert.ToInt64(AdvAdjNo.AdvanceAdjNumber.Substring(12, AdvAdjNo.AdvanceAdjNumber.Length - 12)) + 1).ToString("D5");
                                }
                                var Refadvadjno = Adv.AdvanceAdjNumber;
                                Adv.RefAdvanceId = RefAdvanceId;
                                Adv.RefPaymentId = RefPayId;
                                Adv.Amount = AdvAdjAmt;
                                Adv.CreatedBy = USERNAME;
                                Adv.CreatedOn = DateTime.Now;
                                Adv.IsActive = true;
                                AdvAdj.Add(Adv);

                                TRN_LEDGER Ledger = new TRN_LEDGER();
                                var LedgerID = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).FirstOrDefault();
                                if (LedgerID == null)
                                {
                                    Ledger.LEDGERID = 1;
                                }
                                else
                                {
                                    Ledger.LEDGERID = (Convert.ToInt32(LedgerID.LEDGERID) + (i + 1));
                                }

                                Ledger.REF_USER_ID = payment.PaymentTo;
                                Ledger.REF_EXPENDITURE_LOG_ID = RefExpLogId;
                                Ledger.RefAdvAdjId = payment.AdvanceAdjId.ToString();
                                Ledger.VoucherNo = Refadvadjno;
                                Ledger.DrAmount = 0;
                                Ledger.CrAmount = Convert.ToDecimal(AdvAdjAmt);
                                var lastbalanceuser = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).Where(x => x.IsActive == true && x.REF_AGENCY_ID == payment.AgencyName).FirstOrDefault();
                                if (lastbalanceuser != null)
                                {
                                    if (lastbalanceuser.BalanceAmount != null)
                                    {
                                        Ledger.BalanceAmount = (Convert.ToDecimal(lastbalanceuser.BalanceAmount) + balanceAmount);
                                    }
                                    else
                                    {
                                        Ledger.BalanceAmount = Ledger.CrAmount;
                                    }
                                }
                                Ledger.EffectiveDate = DateTime.ParseExact(payment.PaymentDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                Ledger.CreatedBy = USERNAME;
                                Ledger.CreatedOn = DateTime.Now;
                                Ledger.IsActive = true;

                                ListLedger.Add(Ledger);

                                payment.AdvanceAdjId = payment.AdvanceAdjId + 1;
                                cnt = cnt + i;
                            }

                        }
                        if (AdvAdj.Count > 0)
                        {
                            db.TRN_Advance_Adjustment.AddRange(AdvAdj);
                        }
                        if (ListLedger.Count > 0)
                        {
                            db.TRN_LEDGER.AddRange(ListLedger);
                        }

                        TRN_LEDGER Led = new TRN_LEDGER();

                        var LedID = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).FirstOrDefault();
                        if (LedID == null)
                        {
                            Led.LEDGERID = 1;
                        }
                        else
                        {
                            Led.LEDGERID = (Convert.ToInt32(LedID.LEDGERID) + (cnt + 1));
                        }

                        Led.REF_USER_ID = payment.PaymentTo;
                        Led.REF_EXPENDITURE_LOG_ID = RefExpLogId;
                        Led.RefPaymentId = RefPayId.ToString();
                        Led.VoucherNo = PaymentNo;
                        Led.DrAmount = LedgerPaymentAmount;
                        Led.CrAmount = 0;
                        var lastbalanceuseradjust = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).Where(x => x.IsActive == true && x.REF_AGENCY_ID == payment.AgencyName).FirstOrDefault();
                        if (lastbalanceuseradjust != null)
                        {
                            if (lastbalanceuseradjust.BalanceAmount != null)
                            {
                                Led.BalanceAmount = (Convert.ToDecimal(lastbalanceuseradjust.BalanceAmount) - Convert.ToDecimal(payment.ExpAmount));
                            }
                            else
                            {
                                Led.BalanceAmount = Convert.ToDecimal(payment.ExpAmount);
                            }
                        }
                        Led.EffectiveDate = DateTime.ParseExact(payment.PaymentDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        Led.CreatedBy = USERNAME;
                        Led.CreatedOn = DateTime.Now;
                        Led.IsActive = true;
                        db.TRN_LEDGER.Add(Led);

                        List<TRN_EXPENDETURE_DETAILS> RefExpForId = db.TRN_EXPENDETURE_DETAILS.Where(x => x.ISACTIVE == true && x.REF_EXPENDETURE_NO == payment.ExpID && x.REF_AGENCY_ID == payment.AgencyName).ToList();
                        List<TRN_ExpenduterPaymentMap> Listmap = new List<TRN_ExpenduterPaymentMap>();
                        
                        foreach (var item in RefExpForId)
                        {
                            TRN_ExpenduterPaymentMap map = new TRN_ExpenduterPaymentMap();

                            map.RefExpenduterLogId = RefExpLogId;
                            map.RefExpenduterForId = item.REF_EXPENDETURE_FOR_ID;
                            map.RefUserId = payment.PaymentTo;
                            map.CrAmount = item.APPROVED_AMOUNT.ToString();
                            map.DrAmount = item.APPROVED_AMOUNT.ToString();
                            map.RefPaymentId = RefPayId.ToString();
                            map.CreatedBy = USERNAME;
                            map.CreatedOn = DateTime.Now;
                            map.IsActive = true;
                            Listmap.Add(map);
                        }
                        if (Listmap.Count > 0)
                        {
                            db.TRN_ExpenduterPaymentMap.AddRange(Listmap);
                        }



                        TRN_EXPENDITURE_LOG log = db.TRN_EXPENDITURE_LOG.FirstOrDefault(x => x.IsActive == true && x.EXPENDITURE_NO == payment.ExpID && x.REF_AGENCY_ID == payment.AgencyName);
                        if (log != null)
                        {
                            log.REMAINING_AMOUNT = "0.00";
                            log.PAYMENT_STATUS = "Paid";
                            log.UpdatedBy = USERNAME;
                            log.UpdatedOn = DateTime.Now;
                        }

                    }
                }
            }

            db.SaveChanges();
            //}
            //catch (Exception ex)
            //{
            //    PaymentData = false;
            //}
            return PaymentData;
        }

        public int GetAdvanceAdjId()
        {
            int AdvanceAdjId;
            var id = db.TRN_Advance_Adjustment.OrderByDescending(x => x.AdvanceAdjId).FirstOrDefault();
            if(id == null)
            {
                AdvanceAdjId = 1001;
            }
            else
            {
                AdvanceAdjId = id.AdvanceAdjId + 1;
            }
            return AdvanceAdjId;
        }


    }
}
