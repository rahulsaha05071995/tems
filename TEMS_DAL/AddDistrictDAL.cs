﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TEMS_BOL;
using static TEMS_BOL.AddDistrictBOL;

namespace TEMS_DAL
{
    public class AddDistrictDAL
    {
        TEMSEntities db = new TEMSEntities();
        public List<SelectListItem> ListState()
        {
            List<SelectListItem> StateList = new List<SelectListItem>();
            var StateData = db.MSTR_STATE.Where(x => x.ISACTIVE == true).ToList();
            StateData.ForEach(x => StateList.Add(new SelectListItem { Text = x.STATE_NAME, Value = x.STATE_ID }));

            return StateList;
        }

        public List<DistrictData> GetDistrictData(AddDistrictBOL District)
        {
            List<DistrictData> DistrictListReportData = new List<DistrictData>();

            var Data = db.MSTR_DISTRICT.
                Where(x => (District.RefStateID != null && District.RefStateID != "") ? x.REF_STATE_ID == District.RefStateID : true).
                Where(x => (District.DistrictName != null && District.DistrictName != "") ? x.DISTRICT_ID == District.DistrictName : true).
                Where(x => (District.Status != null && District.Status != "") ? x.ISACTIVE.ToString() == District.Status : true).
                Select(x => new
                {
                    State = x.MSTR_STATE.STATE_NAME,
                    Name = x.DISTRICT_NAME,
                    Id = x.DISTRICT_ID,
                    Status = x.ISACTIVE
                }).ToList();

            foreach (var item in Data)
            {
                DistrictData Dis = new DistrictData();
                Dis.RefStateID = item.State;
                Dis.DistrictID = item.Id;
                Dis.DistrictName = item.Name;
                if (item.Status == true)
                {
                    Dis.Status = "Active";
                }
                else
                {
                    Dis.Status = "Inactive";
                }
                DistrictListReportData.Add(Dis);
            }

            return DistrictListReportData;
        }

        public bool AddDistrictData(AddDistrictBOL District, string USERID)
        {
            bool checkdistrictdata = true;
            try
            {
                var DistrictID = db.MSTR_DISTRICT.OrderByDescending(x => x.DISTRICT_ID).FirstOrDefault();
                MSTR_DISTRICT Dis = new MSTR_DISTRICT();
                if (DistrictID == null)
                {
                    Dis.DISTRICT_ID = "DIS0001";
                }
                else
                {
                    Dis.DISTRICT_ID = "DIS" + (Convert.ToInt32(DistrictID.DISTRICT_ID.Substring(3, DistrictID.DISTRICT_ID.Length - 3)) + 1).ToString("D4");
                }
                Dis.REF_STATE_ID = District.RefStateID;
                Dis.DISTRICT_NAME = District.DistrictName;
                Dis.CREATED_BY = USERID;
                Dis.CREATED_ON = DateTime.Now;
                Dis.ISACTIVE = Convert.ToBoolean(District.Status);
                db.MSTR_DISTRICT.Add(Dis);
                db.SaveChanges();
            }
            catch(Exception ex)
            {
                checkdistrictdata = false;
            }
            return checkdistrictdata;
        }

        public AddDistrictBOL EditDistrict(string DId)
        {
            AddDistrictBOL District = new AddDistrictBOL();

            var Data = db.MSTR_DISTRICT.Where(x => x.DISTRICT_ID == DId).ToList();

            if (Data.Count > 0)
            {
                District.RefStateID = Data[0].REF_STATE_ID;
                District.DistrictName = Data[0].DISTRICT_NAME;
                District.DistrictID = Data[0].DISTRICT_ID;
                District.Status = Convert.ToString(Data[0].ISACTIVE);
            }
            return District;
        }

        public bool EditDistrictData(AddDistrictBOL District, string USERID)
        {
            bool EditDistrictdata = true;
            try
            {
                MSTR_DISTRICT Data = db.MSTR_DISTRICT.FirstOrDefault(x => x.DISTRICT_ID == District.DistrictID);
                if (Data != null)
                {
                    Data.DISTRICT_NAME = District.DistrictName;
                    Data.UPDATED_BY = USERID;
                    Data.UPDATED_ON = DateTime.Now;
                    Data.ISACTIVE = Convert.ToBoolean(District.Status);
                    db.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                EditDistrictdata = false;
            }
            return EditDistrictdata;
        }

    }

}
