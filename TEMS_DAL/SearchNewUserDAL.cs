﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TEMS_BOL;
using static TEMS_BOL.SearchNewUserBOL;

namespace TEMS_DAL
{
    public class SearchNewUserDAL
    {
        TEMSEntities db = new TEMSEntities();
        public List<UserListReportBO> GetUserList(SearchNewUserBOL search)
        {
            List<UserListReportBO> UserData = new List<UserListReportBO>();

            var UserDataList = db.MSTR_USER.                
                Where(x => (search.Email != null && search.Email != "") ? x.USER_NAME == search.Email : true).
                Where(x => (search.Name != null && search.Name != "") ? x.FULL_NAME == search.Name : true).
                Where(x => (search.Role != null && search.Role != "") ? x.REF_ROLE_ID == search.Role : true).
                Where(x => (search.Status != null && search.Status != "") ? x.ISACTIVE.ToString() == search.Status : true).
                Select(x => new
                {
                    Name = x.FULL_NAME,
                    Email = x.USER_NAME,
                    Role = x.MSTR_USER_ROLE.ROLE_NAME,
                    //Designation = x.o.DESIGNATION,
                    UserID = x.USER_ID,
                    Status = x.ISACTIVE

                }).ToList();

            foreach(var item in UserDataList)
            {
                UserListReportBO User = new UserListReportBO();
                User.Name = item.Name;
                User.Email = item.Email;
                User.Role = item.Role;
                //User.Designation = item.Designation;
                if(item.Status == true)
                {
                    User.Status = "Active";
                }
                else
                {
                    User.Status = "Inactive";
                }
                
                User.UserID = item.UserID;
                UserData.Add(User);
            }

            return UserData;
        }
    }
}
