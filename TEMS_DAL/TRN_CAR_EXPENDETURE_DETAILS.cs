//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TEMS_DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class TRN_CAR_EXPENDETURE_DETAILS
    {
        public string CAR_EXPENDETURE_DETAILS_ID { get; set; }
        public string REF_EXPENDETURE_NO { get; set; }
        public string REF_COST_CENTER_ID { get; set; }
        public string DISTANCE_TRAVEL { get; set; }
        public decimal FUEL_COST { get; set; }
        public Nullable<decimal> CAR_RENT { get; set; }
        public Nullable<decimal> TOLL_TAX { get; set; }
        public decimal TOTAL { get; set; }
        public System.DateTime CREATED_ON { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_ON { get; set; }
        public string UPDATED_BY { get; set; }
        public bool ISACTIVE { get; set; }
    
        public virtual MSTR_COST_CENTER MSTR_COST_CENTER { get; set; }
        public virtual TRN_EXPENDETURE_SUMMARY TRN_EXPENDETURE_SUMMARY { get; set; }
    }
}
