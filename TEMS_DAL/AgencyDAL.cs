﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TEMS_BOL;
using static TEMS_BOL.AddAgencyBOL;

namespace TEMS_DAL
{
    public class AgencyDAL
    {
        TEMSEntities db = new TEMSEntities();

        public List<AgencyListData> GetagencyList(AddAgencyBOL search)
        {
            List<AgencyListData> AgencyListReportData = new List<AgencyListData>();

            var Data = db.MSTR_AGENCY.
                Where(x => (search.AgencyEmail != null && search.AgencyEmail != "") ? x.EMAIL_ADDRESS == search.AgencyEmail : true).
                Where(x => (search.AgencyName != null && search.AgencyName != "") ? x.AGENCY_ID == search.AgencyName : true).
                Where(x => (search.AgencyMobileNo != null && search.AgencyMobileNo != "") ? x.MOBILE_NO == search.AgencyMobileNo : true).
                Where(x => (search.AgencyWhatsAppNo != null && search.AgencyWhatsAppNo != "") ? x.WHATSAPP_NO == search.AgencyWhatsAppNo : true).
                Where(x => (search.AgencyAddress != null && search.AgencyAddress != "") ? x.AGENCY_ADDRESS == search.AgencyAddress : true).
                Where(x => (search.Status != null && search.Status != "") ? x.ISACTIVE.ToString() == search.Status : true).
                Select(x => new
                {
                    Name = x.AGENCY_NAME,
                    Email = x.EMAIL_ADDRESS,
                    Mobile = x.MOBILE_NO,
                    WhatsApp = x.WHATSAPP_NO,
                    AgencyID = x.AGENCY_ID,
                    Status = x.ISACTIVE

                }).ToList();

            foreach(var item in Data)
            {
                AgencyListData agency = new AgencyListData();
                agency.AgencyName = item.Name;
                agency.AgencyEmail = item.Email;
                agency.AgencyMobileNo = item.Mobile;
                agency.AgencyWhatsAppNo = item.WhatsApp;
                agency.AgencyID = item.AgencyID;
                if (item.Status == true)
                {
                    agency.Status = "Active";
                }
                else
                {
                    agency.Status = "Inactive";
                }
                AgencyListReportData.Add(agency);
            }


            return AgencyListReportData;
        }


        public bool AddAgencyData(AddAgencyBOL agency, string USERID)
        {
            bool checkagencydata = true;
            try
            {
                var AgencyID = db.MSTR_AGENCY.OrderByDescending(x => x.AGENCY_ID).FirstOrDefault();
                MSTR_AGENCY agncy = new MSTR_AGENCY();
                if(AgencyID == null)
                {
                    agncy.AGENCY_ID = "A001";
                }
                else
                {
                    agncy.AGENCY_ID = "A" + (Convert.ToInt32(AgencyID.AGENCY_ID.Substring(1, AgencyID.AGENCY_ID.Length - 1)) + 1).ToString("D3");
                }
                agncy.AGENCY_NAME = agency.AgencyName;
                agncy.AGENCY_ADDRESS = agency.AgencyAddress;
                agncy.MOBILE_NO = agency.AgencyMobileNo;
                agncy.WHATSAPP_NO = agency.AgencyWhatsAppNo;
                agncy.EMAIL_ADDRESS = agency.AgencyEmail;
                agncy.CREATED_BY = USERID;
                agncy.CREATED_ON = DateTime.Now;
                agncy.ISACTIVE = Convert.ToBoolean(agency.Status);
                db.MSTR_AGENCY.Add(agncy);
                db.SaveChanges();
            }
            catch(Exception ex)
            {
                checkagencydata = false;
            }
            return checkagencydata;
        }

        public AddAgencyBOL EditAgency(string AId)
        {
            AddAgencyBOL agency = new AddAgencyBOL();
            var Data = db.MSTR_AGENCY.Where(x => x.AGENCY_ID == AId).ToList();
            if (Data.Count > 0)
            {
                agency.AgencyName = Data[0].AGENCY_NAME;
                agency.AgencyEmail = Data[0].EMAIL_ADDRESS;
                agency.AgencyMobileNo = Data[0].MOBILE_NO;
                agency.AgencyWhatsAppNo = Data[0].WHATSAPP_NO;
                agency.AgencyAddress = Data[0].AGENCY_ADDRESS;
                agency.AgencyID = Data[0].AGENCY_ID;
                agency.Status = Data[0].ISACTIVE.ToString();
            }
            return agency;
        }

        public bool EditAgencyData(AddAgencyBOL agency, string USERID)
        {
            bool Editagencydata = true;
            try
            {
                MSTR_AGENCY Data = db.MSTR_AGENCY.FirstOrDefault(x=> x.AGENCY_ID == agency.AgencyID);
                if (Data != null)
                {
                    Data.AGENCY_NAME = agency.AgencyName;
                    Data.AGENCY_ADDRESS = agency.AgencyAddress;
                    Data.MOBILE_NO = agency.AgencyMobileNo;
                    Data.WHATSAPP_NO = agency.AgencyWhatsAppNo;
                    Data.EMAIL_ADDRESS = agency.AgencyEmail;
                    Data.UPDATED_BY = USERID;
                    Data.UPDATED_ON = DateTime.Now;
                    Data.ISACTIVE = Convert.ToBoolean(agency.Status);
                    db.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                Editagencydata = false;
            }
            return Editagencydata;
        }
    }
}
