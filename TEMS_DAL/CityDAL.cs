﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TEMS_BOL;
using static TEMS_BOL.AddCityBOL;

namespace TEMS_DAL
{
    public class CityDAL
    {
        TEMSEntities db = new TEMSEntities();
        public List<SelectListItem> ListDistrict()
        {
            List<SelectListItem> DistrictList = new List<SelectListItem>();
            var DistrictData = db.MSTR_DISTRICT.Where(x => x.ISACTIVE == true).ToList();
            DistrictData.ForEach(x => DistrictList.Add(new SelectListItem { Text = x.DISTRICT_NAME, Value = x.DISTRICT_ID }));
            return DistrictList;
        }

        public bool AddCityData(AddCityBOL City, string USERID)
        {
            bool CheckAddCity = true;
            try
            {
                var CityID = db.MSTR_CITY.OrderByDescending(x => x.CITY_ID).FirstOrDefault();
                MSTR_CITY cit = new MSTR_CITY();
                if (CityID == null)
                {
                    cit.CITY_ID = "CIT0001";
                }
                else
                {
                    cit.CITY_ID = "CIT" + (Convert.ToInt32(CityID.CITY_ID.Substring(3, CityID.CITY_ID.Length - 3)) + 1).ToString("D4");
                }
                cit.REF_DISTRICT_ID = City.RefDistrictId;
                cit.REF_STATE_ID = City.RefStateId;
                cit.CITY_NAME = City.CityName;
                cit.CREATED_BY = USERID;
                cit.CREATED_ON = DateTime.Now;
                cit.ISACTIVE = Convert.ToBoolean(City.Status);

                db.MSTR_CITY.Add(cit);
                db.SaveChanges();
            }

            catch(Exception ex)
            {
                CheckAddCity = false;
            }
            return CheckAddCity;
        }

        public List<SelectListItem> ListCity()
        {
            List<SelectListItem> CityList = new List<SelectListItem>();
            var CityData = db.MSTR_CITY.Where(x => x.ISACTIVE == true).ToList();
            CityData.ForEach(x => CityList.Add(new SelectListItem { Text = x.CITY_NAME, Value = x.CITY_ID }));
            return CityList;
        }

        public List<CityData> GetCityData(AddCityBOL City)
        {
            List<CityData> CityListData = new List<CityData>();

            var Data = db.MSTR_CITY.
                Where(x => (City.RefStateId != null && City.RefStateId != "") ? x.REF_STATE_ID == City.RefStateId : true).
                Where(x => (City.RefDistrictId != null && City.RefDistrictId != "") ? x.REF_DISTRICT_ID == City.RefDistrictId : true).
                Where(x => (City.CityName != null && City.CityName != "") ? x.CITY_ID == City.CityName : true).
                Where(x => (City.Status != null && City.Status != "") ? x.ISACTIVE.ToString() == City.Status : true).
                Select(x => new
                {
                    State = x.MSTR_STATE.STATE_NAME,
                    District = x.MSTR_DISTRICT.DISTRICT_NAME,
                    CityName = x.CITY_NAME,
                    Id = x.CITY_ID,
                    Status = x.ISACTIVE
                }).ToList();

            foreach (var item in Data)
            {
                CityData Cit = new CityData();
                Cit.RefStateId = item.State;
                Cit.RefDistrictId = item.District;
                Cit.CityId = item.Id;
                Cit.CityName = item.CityName;
                if (item.Status == true)
                {
                    Cit.Status = "Active";
                }
                else
                {
                    Cit.Status = "Inactive";
                }
                CityListData.Add(Cit);
            }

            return CityListData;
        }

        public AddCityBOL EditCity(string CId)
        {
            AddCityBOL City = new AddCityBOL();
            var Data = db.MSTR_CITY.Where(x => x.CITY_ID == CId).ToList();

            if (Data.Count > 0)
            {
                City.RefStateId = Data[0].REF_STATE_ID;
                City.RefDistrictId = Data[0].REF_DISTRICT_ID;
                City.CityId = Data[0].CITY_ID;
                City.CityName = Data[0].CITY_NAME;
                City.Status = Convert.ToString(Data[0].ISACTIVE);
            }
            return City;
        }

        public bool EditCityData(AddCityBOL City, string USERID)
        {
            bool EditCitydata = true;
            try
            {
                MSTR_CITY Data = db.MSTR_CITY.FirstOrDefault(x => x.CITY_ID == City.CityId);
                if (Data != null)
                {
                    Data.REF_DISTRICT_ID = City.RefDistrictId;
                    Data.REF_STATE_ID = City.RefStateId;
                    Data.CITY_NAME = City.CityName;
                    Data.UPDATED_BY = USERID;
                    Data.UPDATED_ON = DateTime.Now;
                    Data.ISACTIVE = Convert.ToBoolean(City.Status);
                    db.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                EditCitydata = false;
            }
            return EditCitydata;
        }
    }
}
