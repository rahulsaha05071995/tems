﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TEMS_BOL;
using static TEMS_BOL.LedgerBOL;

namespace TEMS_DAL
{
    public class LedgerDAL
    {
        TEMSEntities db = new TEMSEntities();

        public List<LedgerSummaryReportListBOL> GetLedgerSummaryReportList(LedgerBOL Ledger, DateTime FromDate, DateTime ToDate)
        {
            List<LedgerSummaryReportListBOL> SummaryReportListData = new List<LedgerSummaryReportListBOL>();
            if (Ledger.FromDate != null && Ledger.FromDate != "")
            {
                DateTime dateFrom = FromDate.AddMinutes(-1);
                var lstBalance = db.TRN_LEDGER.OrderByDescending(a => a.CreatedOn).Where(a => a.IsActive == true && a.CreatedOn <= dateFrom).Where(a => (Ledger.EmployeeName != null && Ledger.EmployeeName != "") ? a.REF_USER_ID == Ledger.EmployeeName : true).Where(a => (Ledger.AgencyName != null && Ledger.AgencyName != "") ? a.REF_AGENCY_ID == Ledger.AgencyName : true).FirstOrDefault();

                if (lstBalance == null)
                {
                    LedgerSummaryReportListBOL LedgerReport = new LedgerSummaryReportListBOL();
                    LedgerReport.CreatedDate = FromDate.ToString("dd/MM/yyyy");
                    LedgerReport.Particulars = "Opening Balance";
                    LedgerReport.BalanceAmount = 0;
                    SummaryReportListData.Add(LedgerReport);
                }
                else
                {
                    LedgerSummaryReportListBOL LedgerReport = new LedgerSummaryReportListBOL();
                    LedgerReport.CreatedDate = FromDate.ToString("dd/MM/yyyy");
                    LedgerReport.Particulars = "Opening Balance";
                    LedgerReport.BalanceAmount = Convert.ToDecimal(lstBalance.BalanceAmount);
                    SummaryReportListData.Add(LedgerReport);
                }
            }
            else
            {
                LedgerSummaryReportListBOL LedgerReport = new LedgerSummaryReportListBOL();
                LedgerReport.CreatedDate = db.TRN_LEDGER.Where(a => a.IsActive == true).Where(a => (Ledger.EmployeeName != null && Ledger.EmployeeName != "") ? a.REF_USER_ID == Ledger.EmployeeName : true).Where(a => (Ledger.AgencyName != null && Ledger.AgencyName != "") ? a.REF_AGENCY_ID == Ledger.AgencyName : true).OrderBy(a => a.CreatedOn).Select(a => a.CreatedOn).FirstOrDefault().ToString("dd/MM/yyyy");
                LedgerReport.Particulars = "Opening Balance";
                LedgerReport.BalanceAmount = 0;
                SummaryReportListData.Add(LedgerReport);
            }


            var LedgerDataList = db.TRN_LEDGER.Where(a => a.IsActive == true)
                .Where(a => (Ledger.EmployeeName != null && Ledger.EmployeeName != "") ? a.REF_USER_ID == Ledger.EmployeeName : true)
                .Where(a => (Ledger.AgencyName != null && Ledger.AgencyName != "") ? a.REF_AGENCY_ID == Ledger.AgencyName : true)
                .Where(a => (Ledger.FromDate != null && Ledger.FromDate != "") ? a.CreatedOn >= FromDate : true)
                .Where(a => (Ledger.ToDate != null && Ledger.ToDate != "") ? a.CreatedOn <= ToDate : true).OrderBy(a => a.CreatedOn).ToList();

            foreach (var LedgerData in LedgerDataList)
            {
                LedgerSummaryReportListBOL LedgerReport = new LedgerSummaryReportListBOL();
                LedgerReport.CreatedDate = LedgerData.CreatedOn.ToString("dd/MM/yyyy");
                LedgerReport.TransactionDate = LedgerData.EffectiveDate.ToString("dd/MM/yyyy");
                if (LedgerData.VoucherNo.StartsWith("PAY"))
                {
                    LedgerReport.Particulars = "Payment (" + LedgerData.VoucherNo + ")";
                }
                else if (LedgerData.VoucherNo.StartsWith("ADV"))
                {
                    LedgerReport.Particulars = "Advance (" + LedgerData.VoucherNo + ")";
                }
                else if (LedgerData.VoucherNo.StartsWith("AdvAdj"))
                {
                    LedgerReport.Particulars = "Advance Adjustment (" + LedgerData.VoucherNo + ")";
                }
                else if (LedgerData.VoucherNo.StartsWith("1"))
                {
                    LedgerReport.Particulars = "Expendeture Approved (" + LedgerData.VoucherNo + ")";
                }
                LedgerReport.DebitAmount = Convert.ToDecimal(LedgerData.DrAmount);
                LedgerReport.CreditAmount = Convert.ToDecimal(LedgerData.CrAmount);
                LedgerReport.BalanceAmount = Convert.ToDecimal(LedgerData.BalanceAmount);
                SummaryReportListData.Add(LedgerReport);
            }


            return SummaryReportListData;
        }

        public List<LedgerDetailsReportListBOL> GetLedgerDetailsReportList(LedgerBOL Ledger, DateTime FromDate, DateTime ToDate)
        {
            List<LedgerDetailsReportListBOL> DetailsReportListData = new List<LedgerDetailsReportListBOL>();
            if (Ledger.FromDate != null && Ledger.FromDate != "")
            {
                DateTime dateFrom = FromDate.AddMinutes(-1);
                var lstBalance = db.TRN_LEDGER.OrderByDescending(a => a.CreatedOn).Where(a => a.IsActive == true && a.CreatedOn <= dateFrom).Where(a => (Ledger.EmployeeName != null && Ledger.EmployeeName != "") ? a.REF_USER_ID == Ledger.EmployeeName : true).Where(a => (Ledger.AgencyName != null && Ledger.AgencyName != "") ? a.REF_AGENCY_ID == Ledger.AgencyName : true).FirstOrDefault();

                if (lstBalance == null)
                {
                    LedgerDetailsReportListBOL LedgerReport = new LedgerDetailsReportListBOL();
                    LedgerReport.CreatedDate = FromDate;
                    LedgerReport.Particulars = "Opening Balance";
                    LedgerReport.BalanceAmount = 0;
                    DetailsReportListData.Add(LedgerReport);
                }
                else
                {
                    LedgerDetailsReportListBOL LedgerReport = new LedgerDetailsReportListBOL();
                    LedgerReport.CreatedDate = FromDate;
                    LedgerReport.Particulars = "Opening Balance";
                    LedgerReport.BalanceAmount = Convert.ToDecimal(lstBalance.BalanceAmount);
                    DetailsReportListData.Add(LedgerReport);
                }
            }
            else
            {
                LedgerDetailsReportListBOL LedgerReport = new LedgerDetailsReportListBOL();
                LedgerReport.CreatedDate = db.TRN_LEDGER.Where(a => a.IsActive == true).Where(a => (Ledger.EmployeeName != null && Ledger.EmployeeName != "") ? a.REF_USER_ID == Ledger.EmployeeName : true).Where(a => (Ledger.AgencyName != null && Ledger.AgencyName != "") ? a.REF_AGENCY_ID == Ledger.AgencyName : true).OrderBy(a => a.CreatedOn).Select(a => a.CreatedOn).FirstOrDefault();
                LedgerReport.Particulars = "Opening Balance";
                LedgerReport.BalanceAmount = 0;
                DetailsReportListData.Add(LedgerReport);
            }


            var LedgerDataList = db.TRN_LEDGER.Where(a => a.IsActive == true)
                .Where(a => (Ledger.EmployeeName != null && Ledger.EmployeeName != "") ? a.REF_USER_ID == Ledger.EmployeeName : true)
                .Where(a => (Ledger.AgencyName != null && Ledger.AgencyName != "") ? a.REF_AGENCY_ID == Ledger.AgencyName : true)
                .Where(a => (Ledger.FromDate != null && Ledger.FromDate != "") ? a.CreatedOn >= FromDate : true)
                .Where(a => (Ledger.ToDate != null && Ledger.ToDate != "") ? a.CreatedOn <= ToDate : true).OrderBy(a => a.CreatedOn).ToList();

            string[] ApprovedVoucherNo = LedgerDataList.Where(x => x.VoucherNo.StartsWith("1")).Select(x => x.VoucherNo).Distinct().ToArray();
            foreach (var LedgerData in LedgerDataList.Where(x => !x.VoucherNo.StartsWith("1")))
            {
                LedgerDetailsReportListBOL LedgerReport = new LedgerDetailsReportListBOL();
                LedgerReport.CreatedDate = LedgerData.CreatedOn;
                LedgerReport.TransactionDate = LedgerData.EffectiveDate.ToString("dd/MM/yyyy");
                if (LedgerData.VoucherNo.StartsWith("PAY"))
                {
                    LedgerReport.Particulars = "Payment (" + LedgerData.VoucherNo + ")";
                }
                else if (LedgerData.VoucherNo.StartsWith("ADV"))
                {
                    LedgerReport.Particulars = "Advance (" + LedgerData.VoucherNo + ")";
                }
                else if (LedgerData.VoucherNo.StartsWith("AdvAdj"))
                {
                    LedgerReport.Particulars = "Advance Adjustment (" + LedgerData.VoucherNo + ")";
                }
                else if (LedgerData.VoucherNo.StartsWith("1"))
                {
                    LedgerReport.Particulars = "Expendeture Approved (" + LedgerData.VoucherNo + ")";
                }
                LedgerReport.DebitAmount = Convert.ToDecimal(LedgerData.DrAmount);
                LedgerReport.CreditAmount = Convert.ToDecimal(LedgerData.CrAmount);
                LedgerReport.BalanceAmount = Convert.ToDecimal(LedgerData.BalanceAmount);
                DetailsReportListData.Add(LedgerReport);
            }


            foreach (var VoucherNo in ApprovedVoucherNo)
            {
                var ParticularLedgerDataList = LedgerDataList.Where(x => x.VoucherNo == VoucherNo).ToList();
                decimal lstBalanceAmount = Convert.ToDecimal(ParticularLedgerDataList.Select(x => x.BalanceAmount).FirstOrDefault());
                foreach (var LedgerApproved in ParticularLedgerDataList)
                {
                    var LedgerDataApproved = ParticularLedgerDataList.Join(db.TRN_EXPENDETURE_DETAILS, a => a.VoucherNo, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b })
                        .Where(x => x.b.ISACTIVE == true && x.a.VoucherNo == VoucherNo && x.b.REF_EXPENDETURE_FOR_ID == "0002").ToList();
                    if (LedgerDataApproved.Count > 0)
                    {
                        LedgerDetailsReportListBOL LedgerReport = new LedgerDetailsReportListBOL();
                        LedgerReport.CreatedDate = LedgerApproved.CreatedOn;
                        LedgerReport.TransactionDate = LedgerApproved.EffectiveDate.ToString("dd/MM/yyyy");
                        LedgerReport.Particulars = "Car Fuel (" + LedgerApproved.VoucherNo + ")";
                        LedgerReport.CreditAmount = Convert.ToDecimal(LedgerDataApproved.Where(x => x.b.REF_EXPENDETURE_NO == LedgerApproved.VoucherNo && x.b.REF_EXPENDETURE_FOR_ID == "0002").Sum(x => x.b.APPROVED_AMOUNT));
                        LedgerReport.BalanceAmount = Convert.ToDecimal(lstBalanceAmount - LedgerApproved.CrAmount + LedgerApproved.DrAmount + LedgerReport.CreditAmount);
                        lstBalanceAmount = LedgerReport.BalanceAmount;
                        DetailsReportListData.Add(LedgerReport);
                    }
                    
                }

                foreach (var LedgerApproved in ParticularLedgerDataList)
                {
                    var LedgerDataApproved = LedgerDataList.Join(db.TRN_EXPENDETURE_DETAILS, a => a.VoucherNo, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b })
                        .Where(x => x.b.ISACTIVE == true && x.a.VoucherNo == VoucherNo && x.b.REF_EXPENDETURE_FOR_ID == "0002" && x.b.REF_USER_ID != null).ToList();
                    if (LedgerDataApproved.Count > 0)
                    {
                        LedgerDetailsReportListBOL LedgerReport = new LedgerDetailsReportListBOL();
                        LedgerReport.CreatedDate = LedgerApproved.CreatedOn;
                        LedgerReport.TransactionDate = LedgerApproved.EffectiveDate.ToString("dd/MM/yyyy");
                        LedgerReport.Particulars = "Fuel Paid By --"+ LedgerDataApproved[0].b.MSTR_USER.FULL_NAME + " (" + LedgerApproved.VoucherNo + ")";
                        LedgerReport.DebitAmount = Convert.ToDecimal(LedgerDataApproved.Where(x => x.b.REF_EXPENDETURE_NO == LedgerApproved.VoucherNo && x.b.REF_EXPENDETURE_FOR_ID == "0002" && x.b.REF_USER_ID != null).Sum(x => x.b.APPROVED_AMOUNT));
                        LedgerReport.BalanceAmount = Convert.ToDecimal(lstBalanceAmount - LedgerReport.DebitAmount);
                        lstBalanceAmount = LedgerReport.BalanceAmount;
                        DetailsReportListData.Add(LedgerReport);
                    }
                    
                }

                foreach (var LedgerApproved in ParticularLedgerDataList)
                {
                    var LedgerDataApproved = LedgerDataList.Join(db.TRN_EXPENDETURE_DETAILS, a => a.VoucherNo, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b })
                        .Where(x => x.b.ISACTIVE == true && x.a.VoucherNo == VoucherNo && x.b.REF_EXPENDETURE_FOR_ID == "0003").ToList();
                    if (LedgerDataApproved.Count > 0)
                    {
                        LedgerDetailsReportListBOL LedgerReport = new LedgerDetailsReportListBOL();
                        LedgerReport.CreatedDate = LedgerApproved.CreatedOn;
                        LedgerReport.TransactionDate = LedgerApproved.EffectiveDate.ToString("dd/MM/yyyy");
                        LedgerReport.Particulars = "Car Rent (" + LedgerApproved.VoucherNo + ")";
                        LedgerReport.CreditAmount = Convert.ToDecimal(LedgerDataApproved.Where(x => x.b.REF_EXPENDETURE_NO == LedgerApproved.VoucherNo && x.b.REF_EXPENDETURE_FOR_ID == "0003").Sum(x => x.b.APPROVED_AMOUNT));
                        LedgerReport.BalanceAmount = Convert.ToDecimal(lstBalanceAmount + LedgerReport.CreditAmount);
                        lstBalanceAmount = LedgerReport.BalanceAmount;
                        DetailsReportListData.Add(LedgerReport);
                    }

                }

                foreach (var LedgerApproved in ParticularLedgerDataList)
                {
                    var LedgerDataApproved = LedgerDataList.Join(db.TRN_EXPENDETURE_DETAILS, a => a.VoucherNo, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b })
                        .Where(x => x.b.ISACTIVE == true && x.a.VoucherNo == VoucherNo && x.b.REF_EXPENDETURE_FOR_ID == "0003" && x.b.REF_USER_ID != null).ToList();
                    if (LedgerDataApproved.Count > 0)
                    {
                        LedgerDetailsReportListBOL LedgerReport = new LedgerDetailsReportListBOL();
                        LedgerReport.CreatedDate = LedgerApproved.CreatedOn;
                        LedgerReport.TransactionDate = LedgerApproved.EffectiveDate.ToString("dd/MM/yyyy");
                        LedgerReport.Particulars = "Car Rent Paid By --" + LedgerDataApproved[0].b.MSTR_USER.FULL_NAME + " (" + LedgerApproved.VoucherNo + ")";
                        LedgerReport.DebitAmount = Convert.ToDecimal(LedgerDataApproved.Where(x => x.b.REF_EXPENDETURE_NO == LedgerApproved.VoucherNo && x.b.REF_EXPENDETURE_FOR_ID == "0003" && x.b.REF_USER_ID != null).Sum(x => x.b.APPROVED_AMOUNT));
                        LedgerReport.BalanceAmount = Convert.ToDecimal(lstBalanceAmount - LedgerReport.DebitAmount);
                        lstBalanceAmount = LedgerReport.BalanceAmount;
                        DetailsReportListData.Add(LedgerReport);
                    }

                }

                foreach (var LedgerApproved in ParticularLedgerDataList)
                {
                    var LedgerDataApproved = LedgerDataList.Join(db.TRN_EXPENDETURE_DETAILS, a => a.VoucherNo, b => b.REF_EXPENDETURE_NO, (a, b) => new { a, b })
                        .Where(x => x.b.ISACTIVE == true && x.a.VoucherNo == VoucherNo && x.b.REF_EXPENDETURE_FOR_ID == "0004" && x.b.REF_AGENCY_ID != null && x.b.APPROVED_AMOUNT >0).ToList();
                    if (LedgerDataApproved.Count > 0)
                    {
                        LedgerDetailsReportListBOL LedgerReport = new LedgerDetailsReportListBOL();
                        LedgerReport.CreatedDate = LedgerApproved.CreatedOn;
                        LedgerReport.TransactionDate = LedgerApproved.EffectiveDate.ToString("dd/MM/yyyy");
                        LedgerReport.Particulars = "Toll Tax (" + LedgerApproved.VoucherNo + ")";
                        LedgerReport.CreditAmount = Convert.ToDecimal(LedgerDataApproved.Where(x => x.b.REF_EXPENDETURE_NO == LedgerApproved.VoucherNo && x.b.REF_EXPENDETURE_FOR_ID == "0004" && x.b.REF_AGENCY_ID != null && x.b.APPROVED_AMOUNT > 0).Sum(x => x.b.APPROVED_AMOUNT));
                        LedgerReport.BalanceAmount = Convert.ToDecimal(lstBalanceAmount + LedgerReport.CreditAmount);
                        lstBalanceAmount = LedgerReport.BalanceAmount;
                        DetailsReportListData.Add(LedgerReport);
                    }

                }
            }
            
            return DetailsReportListData;
        }
    }
}
