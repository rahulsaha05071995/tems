﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TEMS_BOL;
using static TEMS_BOL.ApprovalBOL;

namespace TEMS_DAL
{
    public class ApprovalDAL
    {
        TEMSEntities db = new TEMSEntities();
        public List<SelectListItem> ListCity()
        {
            List<SelectListItem> CityList = new List<SelectListItem>();
            List<MSTR_CITY> CityData = db.MSTR_CITY.Where(x => x.ISACTIVE == true).ToList();
            CityData.ForEach(x => CityList.Add(new SelectListItem { Text = x.CITY_NAME, Value = x.CITY_ID }));
            return CityList;
        }

        public ApprovalBOL FetchApproveList(string ID)
        {
            ApprovalBOL approve = new ApprovalBOL();
            List<expendeture_details> ExpDetailsList = new List<expendeture_details>();
            var summary = db.TRN_EXPENDETURE_SUMMARY
                .Join(db.EXPENDETURE_STATUS_LOG,
                c => c.EXPENDETURE_NO,
                o => o.REF_EXPENDETURE_NO,
                (c, o) => new { c, o }).Where(x => x.c.EXPENDETURE_NO == ID && x.c.ISACTIVE == true && x.o.ISACTIVE == true).ToList();            

            if (summary.Count > 0)
            {
                approve.expendeture_no = summary[0].c.EXPENDETURE_NO;
                approve.State = summary[0].c.REF_STATE_ID;
                approve.Journey_from = summary[0].c.JOURNEY_FROM;
                approve.Journey_to = summary[0].c.JOURNEY_TO;
                approve.Journey_type = summary[0].c.Journey_Type;
                approve.journey_date = summary[0].c.JOURNEY_DATE != null ? summary[0].c.JOURNEY_DATE.ToString("dd/MM/yyyy") : "";
                approve.distance_covered = summary[0].c.DISTANCE_COVERED;
                approve.car_hire = Convert.ToBoolean(summary[0].c.CAR_HIRE);
                approve.Status = summary[0].o.MSTR_APPROVE_STATUS.APPROVE_STATUS_NAME;
                approve.Remarks = summary[0].c.REMARKS;


                //var RefUserID = db.TRN_EXPENDETURE_DETAILS.Where(x => x.REF_EXPENDETURE_NO == ID && x.ISACTIVE == true).Select(x => x.REF_USER_ID).FirstOrDefault();
                List <TRN_EXPENDETURE_DETAILS> ExpDetails = db.TRN_EXPENDETURE_DETAILS.Where(x => x.REF_EXPENDETURE_NO == ID  && x.ISACTIVE == true && x.REF_USER_ID !=null).ToList();
                if (ExpDetails.Count > 0)
                {
                    foreach (var item in ExpDetails)
                    {
                        expendeture_details exp = new expendeture_details();
                        exp.project = item.REF_PROJECT_ID;
                        exp.Cost_Center = item.REF_COST_CENTER_ID;
                        exp.ExpendetureFor = item.REF_EXPENDETURE_FOR_ID;
                        exp.expendeture_amount = String.Format("{0:0.00}", item.ENTRY_AMOUNT);
                        exp.approved_amount = String.Format("{0:0.00}", item.APPROVED_AMOUNT);
                        exp.expendeture_attachment = item.ATTACHMENT;
                        ExpDetailsList.Add(exp);
                    }
                }
                approve.ExpendetureDetailsList = ExpDetailsList;

                if(approve.car_hire == true)
                {
                    List<TRN_CAR_DETAILS> CarDetails = db.TRN_CAR_DETAILS.Where(x => x.REF_EXPENDETURE_NO == ID && x.ISACTIVE == true).ToList();
                    approve.Agency = CarDetails[0].REF_AGENCY_ID;
                    approve.car_no = CarDetails[0].CAR_NO;
                    approve.driver_name = CarDetails[0].DRIVER_NAME;
                    approve.car_rental_charge = String.Format("{0:0.00}", CarDetails[0].CAR_RENTAL_CHARGES);
                    approve.Car_Mileage = CarDetails[0].MILEAGE;
                    approve.fuel_rate = String.Format("{0:0.00}", CarDetails[0].FUEL_RATE);
                    approve.car_rent_recipt = CarDetails[0].CAR_RENT_RECEIPT;
                    approve.fuel_cost_receipt = CarDetails[0].FUEL_COST_RECEIPT;
                    approve.toll_tax_recipt = CarDetails[0].TOLL_TAX_RECEIPT;

                    List<CarExpendetureDetails> CarExpDetailsList = new List<CarExpendetureDetails>();
                    List<TRN_CAR_EXPENDETURE_DETAILS> CarExpDetails = db.TRN_CAR_EXPENDETURE_DETAILS.Where(x => x.REF_EXPENDETURE_NO == ID && x.ISACTIVE == true).ToList();
                    if (CarExpDetails.Count > 0)
                    {
                        foreach (var item in CarExpDetails)
                        {
                            CarExpendetureDetails Exp = new CarExpendetureDetails();
                            Exp.Cost_Center = item.REF_COST_CENTER_ID;
                            Exp.distance_travel = item.DISTANCE_TRAVEL;
                            Exp.fuel_cost_for_cost_center = String.Format("{0:0.00}", item.FUEL_COST);
                            Exp.car_rent_for_cost_center = String.Format("{0:0.00}", item.CAR_RENT);
                            Exp.toll_tax = String.Format("{0:0.00}", item.TOLL_TAX);
                            Exp.total = String.Format("{0:0.00}", item.TOTAL);
                            CarExpDetailsList.Add(Exp);
                        }
                        approve.CarExpendetureList = CarExpDetailsList;
                    }
                }

                List<TRN_EXPENDITURE_LOG> log = db.TRN_EXPENDITURE_LOG.Where(x => x.IsActive == true && x.EXPENDITURE_NO == approve.expendeture_no).ToList();
                if (log.Count > 0)
                {
                    approve.ApproverRemarks = log[0].Comment;
                }
                
            }

            return approve;
        }

        public List<SelectListItem> getdata(ApprovalBOL approve, string USERNAME)
        {
            List<SelectListItem> Savedata = new List<SelectListItem>();

            List<TRN_EXPENDETURE_DETAILS> OldExpDetails = db.TRN_EXPENDETURE_DETAILS.Where(x => x.REF_EXPENDETURE_NO == approve.expendeture_no
            && x.ISACTIVE == true).ToList();

            if (OldExpDetails.Count > 0)
            {
                db.TRN_EXPENDETURE_DETAILS.RemoveRange(OldExpDetails);
            }

            List<TRN_EXPENDETURE_DETAILS> ListExpDetails = new List<TRN_EXPENDETURE_DETAILS>();
            foreach (var item in approve.ExpendetureDetailsList)
            {
                TRN_EXPENDETURE_DETAILS ExpDetails = new TRN_EXPENDETURE_DETAILS();
                ExpDetails.EXPENDETURE_DETAILS_ID = Guid.NewGuid().ToString();
                ExpDetails.REF_EXPENDETURE_NO = approve.expendeture_no;
                ExpDetails.REF_COST_CENTER_ID = item.Cost_Center;
                ExpDetails.REF_PROJECT_ID = item.project;
                ExpDetails.REF_EXPENDETURE_FOR_ID = item.ExpendetureFor;
                ExpDetails.ENTRY_AMOUNT = Convert.ToDecimal(item.expendeture_amount);
                ExpDetails.ClaimedAmount = Convert.ToDecimal(item.expendeture_amount);
                ExpDetails.APPROVED_AMOUNT = Convert.ToDecimal(item.approved_amount);
                ExpDetails.CREATED_ON = OldExpDetails[0].CREATED_ON;
                ExpDetails.CREATED_BY = OldExpDetails[0].CREATED_BY;
                ExpDetails.UPDATED_ON = DateTime.Now;
                ExpDetails.UPDATED_BY = USERNAME;
                ExpDetails.REF_USER_ID = OldExpDetails[0].CREATED_BY;
                ExpDetails.ISACTIVE = true;
                ListExpDetails.Add(ExpDetails);
            }
            if (ListExpDetails.Count > 0)
            {
                db.TRN_EXPENDETURE_DETAILS.AddRange(ListExpDetails);
            }

            if (approve.car_hire == true)
            {
                List<TRN_CAR_EXPENDETURE_DETAILS> OldExpCarDetails = db.TRN_CAR_EXPENDETURE_DETAILS.Where(x => x.REF_EXPENDETURE_NO == approve.expendeture_no
                && x.ISACTIVE == true).ToList();

                if (OldExpCarDetails.Count > 0)
                {
                    db.TRN_CAR_EXPENDETURE_DETAILS.RemoveRange(OldExpCarDetails);
                }

                List<TRN_EXPENDETURE_DETAILS> ListExpDetail = new List<TRN_EXPENDETURE_DETAILS>();
                List<TRN_CAR_EXPENDETURE_DETAILS> ListCarExpDetails = new List<TRN_CAR_EXPENDETURE_DETAILS>();
                foreach (var item in approve.CarExpendetureList)
                {
                    TRN_CAR_EXPENDETURE_DETAILS CarExpDetails = new TRN_CAR_EXPENDETURE_DETAILS();
                    CarExpDetails.CAR_EXPENDETURE_DETAILS_ID = Guid.NewGuid().ToString();
                    CarExpDetails.REF_EXPENDETURE_NO = approve.expendeture_no;
                    CarExpDetails.REF_COST_CENTER_ID = item.Cost_Center;
                    CarExpDetails.DISTANCE_TRAVEL = item.distance_travel;
                    CarExpDetails.FUEL_COST = Convert.ToDecimal(item.fuel_cost_for_cost_center);
                    CarExpDetails.CAR_RENT = Convert.ToDecimal(item.car_rent_for_cost_center);
                    CarExpDetails.TOLL_TAX = Convert.ToDecimal(item.toll_tax);
                    CarExpDetails.TOTAL = Convert.ToDecimal(item.total);
                    CarExpDetails.CREATED_BY = OldExpCarDetails[0].CREATED_BY;
                    CarExpDetails.CREATED_ON = OldExpDetails[0].CREATED_ON;
                    CarExpDetails.UPDATED_ON = DateTime.Now;
                    CarExpDetails.UPDATED_BY = USERNAME;
                    CarExpDetails.ISACTIVE = true;
                    ListCarExpDetails.Add(CarExpDetails);

                    List<TRN_CAR_DETAILS> OldCarDetails = db.TRN_CAR_DETAILS.Where(x => x.REF_EXPENDETURE_NO == approve.expendeture_no
                    && x.ISACTIVE == true).ToList();

                    var RefUserID = db.TRN_EXPENDETURE_DETAILS.Where(x => x.REF_EXPENDETURE_NO == approve.expendeture_no && x.ISACTIVE == true).Select(x => x.REF_USER_ID).FirstOrDefault();
                    var CarFuel = db.TRN_EXPENDETURE_DETAILS.Where(x => x.REF_EXPENDETURE_NO == approve.expendeture_no && x.ISACTIVE == true && x.REF_EXPENDETURE_FOR_ID =="0002" && x.REF_USER_ID !=null).Select(x => x.ENTRY_AMOUNT).FirstOrDefault();

                    TRN_EXPENDETURE_DETAILS ExpDetail = new TRN_EXPENDETURE_DETAILS();
                    ExpDetail.EXPENDETURE_DETAILS_ID = Guid.NewGuid().ToString();
                    ExpDetail.REF_EXPENDETURE_NO = approve.expendeture_no;
                    ExpDetail.REF_COST_CENTER_ID = item.Cost_Center;
                    ExpDetail.REF_EXPENDETURE_FOR_ID = "0002";
                    ExpDetail.ENTRY_AMOUNT = Convert.ToDecimal(item.fuel_cost_for_cost_center);
                    var Carfuelclaim = ListExpDetails.Where(x => x.REF_EXPENDETURE_FOR_ID == "0002").Select(x => x.ENTRY_AMOUNT).ToList();
                    if (Carfuelclaim.Count > 0)
                    {
                        ExpDetail.ClaimedAmount = Convert.ToDecimal(item.fuel_cost_for_cost_center) - Convert.ToDecimal(Carfuelclaim[0]);
                    }
                    else
                    {
                        ExpDetail.ClaimedAmount = Convert.ToDecimal(item.fuel_cost_for_cost_center);
                    }
                    if (CarFuel != null)
                    {                        
                        ExpDetail.APPROVED_AMOUNT = Convert.ToDecimal(item.fuel_cost_for_cost_center) - Convert.ToDecimal(CarFuel);
                    }
                    else
                    {
                        ExpDetail.APPROVED_AMOUNT = Convert.ToDecimal(item.fuel_cost_for_cost_center);
                    }
                    
                    ExpDetail.CREATED_ON = OldExpDetails[0].CREATED_ON;
                    ExpDetail.CREATED_BY = OldExpCarDetails[0].CREATED_BY;
                    ExpDetail.UPDATED_ON = DateTime.Now;
                    ExpDetail.UPDATED_BY = USERNAME;
                    ExpDetail.REF_AGENCY_ID = OldCarDetails[0].REF_AGENCY_ID;
                    ExpDetail.ISACTIVE = true;
                    ListExpDetail.Add(ExpDetail);

                    TRN_EXPENDETURE_DETAILS ExpDetai = new TRN_EXPENDETURE_DETAILS();
                    ExpDetai.EXPENDETURE_DETAILS_ID = Guid.NewGuid().ToString();
                    ExpDetai.REF_EXPENDETURE_NO = approve.expendeture_no;
                    ExpDetai.REF_COST_CENTER_ID = item.Cost_Center;
                    ExpDetai.REF_EXPENDETURE_FOR_ID = "0003";
                    ExpDetai.ENTRY_AMOUNT = Convert.ToDecimal(item.car_rent_for_cost_center);
                    ExpDetai.ClaimedAmount = Convert.ToDecimal(item.car_rent_for_cost_center);
                    ExpDetai.APPROVED_AMOUNT = Convert.ToDecimal(item.car_rent_for_cost_center);
                    ExpDetai.CREATED_ON = OldExpDetails[0].CREATED_ON;
                    ExpDetai.CREATED_BY = OldExpCarDetails[0].CREATED_BY; 
                    ExpDetai.UPDATED_ON = DateTime.Now;
                    ExpDetai.UPDATED_BY = USERNAME;
                    ExpDetai.REF_AGENCY_ID = OldCarDetails[0].REF_AGENCY_ID;
                    ExpDetai.ISACTIVE = true;
                    ListExpDetail.Add(ExpDetai);

                    TRN_EXPENDETURE_DETAILS ExpDeta = new TRN_EXPENDETURE_DETAILS();
                    ExpDeta.EXPENDETURE_DETAILS_ID = Guid.NewGuid().ToString();
                    ExpDeta.REF_EXPENDETURE_NO = approve.expendeture_no;
                    ExpDeta.REF_COST_CENTER_ID = item.Cost_Center;
                    ExpDeta.REF_EXPENDETURE_FOR_ID = "0004";
                    ExpDeta.ENTRY_AMOUNT = Convert.ToDecimal(item.toll_tax);
                    ExpDeta.ClaimedAmount = Convert.ToDecimal(item.toll_tax);
                    ExpDeta.APPROVED_AMOUNT = Convert.ToDecimal(item.toll_tax);
                    ExpDeta.CREATED_ON = OldExpDetails[0].CREATED_ON;
                    ExpDeta.CREATED_BY = OldExpCarDetails[0].CREATED_BY; 
                    ExpDeta.UPDATED_ON = DateTime.Now;
                    ExpDeta.UPDATED_BY = USERNAME;
                    ExpDeta.REF_AGENCY_ID = OldCarDetails[0].REF_AGENCY_ID;
                    ExpDeta.ISACTIVE = true;
                    ListExpDetail.Add(ExpDeta);
                }
                if (ListExpDetail.Count > 0)
                {
                    db.TRN_EXPENDETURE_DETAILS.AddRange(ListExpDetail);
                }
                if (ListCarExpDetails.Count > 0)
                {
                    db.TRN_CAR_EXPENDETURE_DETAILS.AddRange(ListCarExpDetails);
                }
                
            }
            db.SaveChanges();

            EXPENDETURE_STATUS_LOG log = db.EXPENDETURE_STATUS_LOG.FirstOrDefault(x => x.REF_EXPENDETURE_NO == approve.expendeture_no && x.ISACTIVE == true);
            if (log != null)
            {
                log.ISACTIVE = false;
            }

            EXPENDETURE_STATUS_LOG StatusLog = new EXPENDETURE_STATUS_LOG();
            StatusLog.EXPENDETURE_APPROVAL_STATUS_LOG_ID = Guid.NewGuid().ToString();
            StatusLog.REF_EXPENDETURE_NO = approve.expendeture_no;
            StatusLog.REF_APPROVE_STATUS_ID = "AS0001";
            StatusLog.CREATED_BY = USERNAME;
            StatusLog.CREATED_ON = DateTime.Now;
            StatusLog.UPDATED_ON = DateTime.Now;
            StatusLog.UPDATED_BY = USERNAME;
            StatusLog.ISACTIVE = true;
            db.EXPENDETURE_STATUS_LOG.Add(StatusLog);

            TRN_EXPENDITURE_LOG UserExpLog = new TRN_EXPENDITURE_LOG();
            var LogID = db.TRN_EXPENDITURE_LOG.OrderByDescending(x => x.EXPENDITURE_LOG_ID).FirstOrDefault();
            if (LogID == null)
            {
                UserExpLog.EXPENDITURE_LOG_ID = 1;
            }
            else
            {
                UserExpLog.EXPENDITURE_LOG_ID = (Convert.ToInt32(LogID.EXPENDITURE_LOG_ID) + 1);
            }            
            UserExpLog.EXPENDITURE_NO = approve.expendeture_no;

            var RefUser = db.TRN_EXPENDETURE_DETAILS.Where(x => x.REF_EXPENDETURE_NO == approve.expendeture_no && x.ISACTIVE == true && x.REF_USER_ID != null).Select(x=>x.REF_USER_ID).FirstOrDefault();
            UserExpLog.REF_USER_ID = RefUser;

            var UserClaimAmount = db.TRN_EXPENDETURE_DETAILS.Where(x => x.REF_EXPENDETURE_NO == approve.expendeture_no
            && x.ISACTIVE == true && x.REF_USER_ID !=null).ToList();
            UserExpLog.EXPENDITURE_AMOUNT = UserClaimAmount.Sum(x => x.APPROVED_AMOUNT);
            UserExpLog.REMAINING_AMOUNT = UserClaimAmount.Sum(x => x.APPROVED_AMOUNT).ToString();

            var ExpDate = db.TRN_EXPENDETURE_SUMMARY.Where(x => x.ISACTIVE == true && x.EXPENDETURE_NO == approve.expendeture_no).ToList();
            UserExpLog.EFFECTIVE_DATE = ExpDate[0].JOURNEY_DATE;

            UserExpLog.PAYMENT_STATUS = "Unpaid";
            UserExpLog.CreatedBy = USERNAME;
            UserExpLog.CreatedOn = DateTime.Now;            
            UserExpLog.IsActive = true;
            UserExpLog.Comment = approve.ApproverRemarks;
            db.TRN_EXPENDITURE_LOG.Add(UserExpLog);
            var ID = UserExpLog.EXPENDITURE_LOG_ID;

            TRN_LEDGER UserLedger = new TRN_LEDGER();
            var LedgerID = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).FirstOrDefault();
            if (LedgerID == null)
            {
                UserLedger.LEDGERID = 1;
            }
            else
            {
                UserLedger.LEDGERID = (Convert.ToInt32(LedgerID.LEDGERID) + 1);
            }
            
            UserLedger.REF_USER_ID = RefUser;
            var ExpLogDetails = db.TRN_EXPENDITURE_LOG.Where(x => x.IsActive == true && x.REF_USER_ID == RefUser).FirstOrDefault();
            UserLedger.REF_EXPENDITURE_LOG_ID = ID;
            UserLedger.VoucherNo = approve.expendeture_no;
            UserLedger.CrAmount = UserClaimAmount.Sum(x => x.APPROVED_AMOUNT);
            UserLedger.DrAmount = 0;
            var userbalance = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).Where(x => x.IsActive == true && x.REF_USER_ID == RefUser).FirstOrDefault();
            if (userbalance != null)
            {
                if(userbalance.BalanceAmount != null)
                {
                    UserLedger.BalanceAmount = Convert.ToDecimal(Convert.ToDecimal(UserLedger.CrAmount) + Convert.ToDecimal(userbalance.BalanceAmount));
                }
                else
                {
                    UserLedger.BalanceAmount = UserLedger.CrAmount;
                }
                
            }
            else
            {
                UserLedger.BalanceAmount = UserLedger.CrAmount;
            }
            
            UserLedger.EffectiveDate = ExpDate[0].JOURNEY_DATE;
            UserLedger.CreatedBy = USERNAME;
            UserLedger.CreatedOn = DateTime.Now;
            UserLedger.IsActive = true;
            db.TRN_LEDGER.Add(UserLedger);

            if (approve.car_hire == true)
            {
                TRN_EXPENDITURE_LOG AgencyExpLog = new TRN_EXPENDITURE_LOG();

                var AgencyLogID = db.TRN_EXPENDITURE_LOG.OrderByDescending(x => x.EXPENDITURE_LOG_ID).FirstOrDefault();
                if (AgencyLogID == null)
                {
                    AgencyExpLog.EXPENDITURE_LOG_ID = 2;
                }
                else
                {
                    AgencyExpLog.EXPENDITURE_LOG_ID = (Convert.ToInt32(AgencyLogID.EXPENDITURE_LOG_ID) + 2);
                }
                AgencyExpLog.EXPENDITURE_NO = approve.expendeture_no;

                var RefAgency = db.TRN_EXPENDETURE_DETAILS.Where(x => x.REF_EXPENDETURE_NO == approve.expendeture_no && x.ISACTIVE == true && x.REF_AGENCY_ID!=null).Select(x => x.REF_AGENCY_ID).FirstOrDefault();
                AgencyExpLog.REF_AGENCY_ID = RefAgency;

                var AgencyClaimAmount = db.TRN_EXPENDETURE_DETAILS.Where(x => x.REF_EXPENDETURE_NO == approve.expendeture_no && x.ISACTIVE == true && x.REF_AGENCY_ID !=null).ToList();
                AgencyExpLog.EXPENDITURE_AMOUNT = AgencyClaimAmount.Sum(x => x.APPROVED_AMOUNT);
                AgencyExpLog.REMAINING_AMOUNT = AgencyClaimAmount.Sum(x => x.APPROVED_AMOUNT).ToString();

                AgencyExpLog.EFFECTIVE_DATE = ExpDate[0].JOURNEY_DATE;

                AgencyExpLog.PAYMENT_STATUS = "Unpaid";
                AgencyExpLog.CreatedBy = USERNAME;
                AgencyExpLog.CreatedOn = DateTime.Now;
                AgencyExpLog.IsActive = true;
                db.TRN_EXPENDITURE_LOG.Add(AgencyExpLog);

                var AgencyID = AgencyExpLog.EXPENDITURE_LOG_ID;

                TRN_LEDGER AgencyLedger = new TRN_LEDGER();
                var AgencyLedgerID = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).FirstOrDefault();
                if (AgencyLedgerID == null)
                {
                    AgencyLedger.LEDGERID = 2;
                }
                else
                {
                    AgencyLedger.LEDGERID = (Convert.ToInt32(AgencyLedgerID.LEDGERID) + 2);
                }
                AgencyLedger.REF_AGENCY_ID = RefAgency;
                var ExpLogDetailsAgency = db.TRN_EXPENDITURE_LOG.Where(x => x.IsActive == true && x.REF_AGENCY_ID == RefAgency).FirstOrDefault();
                AgencyLedger.REF_EXPENDITURE_LOG_ID = AgencyID;
                AgencyLedger.VoucherNo = approve.expendeture_no;
                AgencyLedger.CrAmount = AgencyClaimAmount.Sum(x => x.APPROVED_AMOUNT);
                AgencyLedger.DrAmount = 0;
                var balance = db.TRN_LEDGER.OrderByDescending(x => x.LEDGERID).Where(x => x.IsActive == true && x.REF_AGENCY_ID == RefAgency).FirstOrDefault();
                if(ExpLogDetailsAgency != null)
                {
                    AgencyLedger.BalanceAmount = Convert.ToDecimal(Convert.ToDecimal(AgencyLedger.CrAmount) + Convert.ToDecimal(balance.BalanceAmount));
                }
                else
                {
                    AgencyLedger.BalanceAmount = AgencyLedger.CrAmount;
                }
                
                AgencyLedger.EffectiveDate = ExpDate[0].JOURNEY_DATE;
                AgencyLedger.CreatedBy = USERNAME;
                AgencyLedger.CreatedOn = DateTime.Now;
                AgencyLedger.IsActive = true;
                db.TRN_LEDGER.Add(AgencyLedger);
            }


            db.SaveChanges();
            return Savedata;
        }

        public List<SelectListItem> DeclineData(string ID, string USERNAME)
        {
            List<SelectListItem> Decline = new List<SelectListItem>();
            EXPENDETURE_STATUS_LOG log = db.EXPENDETURE_STATUS_LOG.FirstOrDefault(x => x.REF_EXPENDETURE_NO == ID && x.ISACTIVE == true);
            if(log!=null)
            {
                log.ISACTIVE = false;
            }

            EXPENDETURE_STATUS_LOG StatusLog = new EXPENDETURE_STATUS_LOG();
            StatusLog.EXPENDETURE_APPROVAL_STATUS_LOG_ID = Guid.NewGuid().ToString();
            StatusLog.REF_EXPENDETURE_NO = ID;
            StatusLog.REF_APPROVE_STATUS_ID = "AS0009";
            StatusLog.CREATED_BY = USERNAME;
            StatusLog.CREATED_ON = DateTime.Now;
            StatusLog.UPDATED_ON = DateTime.Now;
            StatusLog.UPDATED_BY = USERNAME;
            StatusLog.ISACTIVE = true;
            db.EXPENDETURE_STATUS_LOG.Add(StatusLog);

            db.SaveChanges();
            return Decline;
        }

        public bool DeleteConfirmedExpNO(string ID, string USERNAME)
        {
            bool DeleteStatus = true;
            try
            {
                TRN_EXPENDETURE_SUMMARY summary = db.TRN_EXPENDETURE_SUMMARY.FirstOrDefault(x => x.EXPENDETURE_NO == ID && x.ISACTIVE == true);
                if(summary != null)
                {
                    summary.ISACTIVE = false;
                    List<TRN_EXPENDETURE_DETAILS> OldExpDetails = db.TRN_EXPENDETURE_DETAILS.Where(x => x.REF_EXPENDETURE_NO == ID && x.ISACTIVE == true).ToList();
                    if(OldExpDetails.Count > 0)
                    {
                        foreach(var item in OldExpDetails)
                        {
                            item.ISACTIVE = false;
                        }
                    }

                    if(summary.CAR_HIRE == true)
                    {

                        TRN_CAR_DETAILS CarDetails = db.TRN_CAR_DETAILS.FirstOrDefault(x => x.ISACTIVE == true && x.REF_EXPENDETURE_NO == ID);
                        if (CarDetails != null)
                        {
                            CarDetails.ISACTIVE = false;
                        }

                        List<TRN_CAR_EXPENDETURE_DETAILS> OldCarExpDetails = db.TRN_CAR_EXPENDETURE_DETAILS.Where(x => x.REF_EXPENDETURE_NO == ID && x.ISACTIVE == true).ToList();
                        if (OldCarExpDetails.Count > 0)
                        {
                            foreach (var item in OldCarExpDetails)
                            {
                                item.ISACTIVE = false;
                            }
                        }
                    }

                    EXPENDETURE_STATUS_LOG log = db.EXPENDETURE_STATUS_LOG.FirstOrDefault(x => x.ISACTIVE == true && x.REF_EXPENDETURE_NO == ID);
                    if(log != null)
                    {
                        log.ISACTIVE = false;
                    }

                    TRN_DELETE_LOG DeleteLog = new TRN_DELETE_LOG();
                    DeleteLog.DELETE_LOG_ID = Guid.NewGuid().ToString();
                    DeleteLog.REF_EXPENDETURE_NO = ID;
                    DeleteLog.DELETED_BY = USERNAME;
                    DeleteLog.DELETED_ON = DateTime.Now;
                    DeleteLog.ISACTIVE = true;
                    db.TRN_DELETE_LOG.Add(DeleteLog);
                }
                db.SaveChanges();
            }
            catch (Exception Ex)
            {
                DeleteStatus = false;
            }
            return DeleteStatus;
        }
    }
}
