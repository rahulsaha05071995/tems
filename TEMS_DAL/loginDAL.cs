﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TEMS_BOL;

namespace TEMS_DAL
{
    public class loginDAL
    {
        TEMSEntities db = new TEMSEntities();
        public MSTR_USER checkuser(loginBOL emp)
        {            
            var user = db.MSTR_USER.Where(x => x.USER_NAME == emp.USER_NAME && x.PASSWORD == emp.PASSWORD).FirstOrDefault();
            return user;
        }
    }
}
