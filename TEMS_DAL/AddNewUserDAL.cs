﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TEMS_BOL;

namespace TEMS_DAL
{
    public class AddNewUserDAL
    {
        TEMSEntities db = new TEMSEntities();
        public List<SelectListItem> RoleList()
        {
            List<SelectListItem> ListRole = new List<SelectListItem>();
            List<MSTR_USER_ROLE> RoleData = db.MSTR_USER_ROLE.Where(x => x.ISACTIVE == true).ToList();
            RoleData.ForEach(x => ListRole.Add(new SelectListItem { Text = x.ROLE_NAME, Value = x.ROLE_ID }));
            return ListRole;
        }

        public List<SelectListItem> GradeList()
        {
            List<SelectListItem> ListGrade = new List<SelectListItem>();
            var GradeData = db.MSTR_EMPLOYEE_GRADE.Where(x => x.ISACTIVE == true).Select(x => new { Grade = x.EMPLOYEE_GRADE_NAME }).Distinct().ToList(); ;
            GradeData.ForEach(item => ListGrade.Add(new SelectListItem { Text = item.Grade, Value = item.Grade }));
            return ListGrade;
        }

        public int CheckDuplicateUserEmail(AddNewUserBOL User)
        {            
            int DuplicateEmail = db.MSTR_USER.Where(x => x.USER_NAME.ToLower() == User.EmailID.ToLower() && x.ISACTIVE == true).Count();
            return DuplicateEmail;
        }

        public bool SaveUserDetails(AddNewUserBOL User, string USERNAME)
        {
            bool SaveData = true;
            try
            {
                MSTR_USER Userdetails = new MSTR_USER();
                var UserID = db.MSTR_USER.OrderByDescending(x => x.USER_ID).FirstOrDefault();
                if(UserID == null)
                {
                    Userdetails.USER_ID = "001";
                }
                else
                {
                    Userdetails.USER_ID = (Convert.ToInt32(UserID.USER_ID.Substring(1, UserID.USER_ID.Length - 1)) + 1).ToString("D3");
                }

                Userdetails.USER_NAME = User.EmailID;
                Userdetails.PASSWORD = User.Password;
                Userdetails.FULL_NAME = User.Name;
                Userdetails.CREATED_ON = DateTime.Now;
                Userdetails.CREATED_BY = USERNAME;
                Userdetails.UPDATED_ON = DateTime.Now;
                Userdetails.UPDATED_BY = USERNAME;
                Userdetails.ISACTIVE = true;
                Userdetails.REF_ROLE_ID = User.Role;
                db.MSTR_USER.Add(Userdetails);

                var REFUSERID = Userdetails.USER_ID;

                MSTR_EMPLOYEE_GRADE EmpGrade = new MSTR_EMPLOYEE_GRADE();

                var EmpGradeID = db.MSTR_EMPLOYEE_GRADE.OrderByDescending(x => x.EMPLOYEE_GRADE_ID).FirstOrDefault();
                if (EmpGradeID == null)
                {
                    EmpGrade.EMPLOYEE_GRADE_ID = "0001";
                }
                else
                {
                    EmpGrade.EMPLOYEE_GRADE_ID = (Convert.ToInt32(EmpGradeID.EMPLOYEE_GRADE_ID.Substring(2, EmpGradeID.EMPLOYEE_GRADE_ID.Length - 2)) + 1).ToString("D4");
                }

                EmpGrade.EMPLOYEE_GRADE_NAME = User.Grade;
                EmpGrade.CREATED_ON = DateTime.Now;
                EmpGrade.CREATED_BY = USERNAME;
                EmpGrade.UPDATED_ON = DateTime.Now;
                EmpGrade.UPDATED_BY = USERNAME;
                EmpGrade.ISACTIVE = true;
                EmpGrade.REF_USER_ID = REFUSERID;
                db.MSTR_EMPLOYEE_GRADE.Add(EmpGrade);

                var RefID = EmpGrade.EMPLOYEE_GRADE_ID;

                MSTR_EMPLOYEE Emp = new MSTR_EMPLOYEE();
                var EmpID = db.MSTR_EMPLOYEE.OrderByDescending(a => a.EMPLOYEE_ID).Where(a => a.EMPLOYEE_ID.StartsWith("E")).FirstOrDefault();
                if (EmpID == null)
                {
                   Emp.EMPLOYEE_ID = "E001";
                }
                else
                {
                   Emp.EMPLOYEE_ID = "E" + (Convert.ToInt32(EmpID.EMPLOYEE_ID.Substring(3, EmpID.EMPLOYEE_ID.Length - 3)) + 1).ToString("D3");
                }

                Emp.REF_EMPLOYEE_GRADE_ID = RefID;
                Emp.EMPLOYEE_NAME = User.Name;
                Emp.CREATED_ON = DateTime.Now;
                Emp.CREATED_BY = USERNAME;
                Emp.UPDATED_ON = DateTime.Now;
                Emp.UPDATED_BY = USERNAME;
                Emp.ISACTIVE = true;
                Emp.DESIGNATION = User.Designation;
                db.MSTR_EMPLOYEE.Add(Emp);

                TRN_ALLOWANCE Allowance = new TRN_ALLOWANCE();
                var AllowanceID = db.TRN_ALLOWANCE.OrderByDescending(x => x.ALLOWANCE_ID).FirstOrDefault();
                if (AllowanceID == null)
                {
                    Allowance.ALLOWANCE_ID = "ALW0001";
                }
                else
                {
                    Allowance.ALLOWANCE_ID = "ALW" + (Convert.ToInt32(AllowanceID.ALLOWANCE_ID.Substring(3, AllowanceID.ALLOWANCE_ID.Length - 3)) + 1).ToString("D4");
                }

                Allowance.REF_EXPENDETURE_FOR_ID = "0006";
                Allowance.REF_EMPLOYEE_GRADE_ID = RefID;
                if(Allowance.REF_EXPENDETURE_FOR_ID == "0006")
                {
                    Allowance.ALLOWANCE_AMOUNT = User.FoodingExpense;
                }
                Allowance.CREATED_BY = USERNAME;
                Allowance.CREATED_ON = DateTime.Now;
                Allowance.UPDATED_ON = DateTime.Now;
                Allowance.UPDATED_BY = USERNAME;
                Allowance.ISACTIVE = true;
                Allowance.REF_USER_ID = REFUSERID;
                db.TRN_ALLOWANCE.Add(Allowance);

                TRN_ALLOWANCE Allowance1 = new TRN_ALLOWANCE();
                var AllowanceID1 = db.TRN_ALLOWANCE.OrderByDescending(x => x.ALLOWANCE_ID).FirstOrDefault();
                if (AllowanceID1 == null)
                {
                    Allowance1.ALLOWANCE_ID = "ALW0001";
                }
                else
                {
                    Allowance1.ALLOWANCE_ID = "ALW" + (Convert.ToInt32(AllowanceID1.ALLOWANCE_ID.Substring(3, AllowanceID1.ALLOWANCE_ID.Length - 3)) + 2).ToString("D4");
                }

                Allowance1.REF_EXPENDETURE_FOR_ID = "0007";
                Allowance1.REF_EMPLOYEE_GRADE_ID = RefID;
                if (Allowance1.REF_EXPENDETURE_FOR_ID == "0007")
                {
                    Allowance1.ALLOWANCE_AMOUNT = User.LodgingExpense;
                }
                Allowance1.CREATED_BY = USERNAME;
                Allowance1.CREATED_ON = DateTime.Now;
                Allowance1.UPDATED_ON = DateTime.Now;
                Allowance1.UPDATED_BY = USERNAME;
                Allowance1.ISACTIVE = true;
                Allowance1.REF_USER_ID = REFUSERID;
                db.TRN_ALLOWANCE.Add(Allowance1);
                db.SaveChanges();
            }

            catch (Exception Ex)
            {
                Console.WriteLine(Ex);
                SaveData = false;
            }
            return SaveData;
        }

        public int CheckDuplicateUserEditEmail(AddNewUserBOL User)
        {
            int DuplicateEmail = db.MSTR_USER.Where(x => x.USER_NAME.ToLower() == User.EmailID.ToLower() && x.USER_ID != User.UserID && x.ISACTIVE == true).Count();
            return DuplicateEmail;
        }

        public AddNewUserBOL EditUser(string UID)
        {
            AddNewUserBOL User = new AddNewUserBOL();

            var Data = db.MSTR_USER
                .Join(db.MSTR_EMPLOYEE_GRADE,
                    c => c.USER_ID,
                    o => o.REF_USER_ID,
                    (c, o) => new { c, o })
                    .Join(db.TRN_ALLOWANCE,
                    m => m.c.USER_ID,
                    n => n.REF_USER_ID,
                    (m, n) => new { m, n }).
                    Where(x => x.m.c.ISACTIVE == true && x.m.c.USER_ID == UID).
                    ToList();
            if (Data.Count > 0)
            {
                User.Name = Data[0].m.c.FULL_NAME;
                User.EmailID = Data[0].m.c.USER_NAME;
                User.Role = Data[0].m.c.REF_ROLE_ID;                
                User.UserID = UID;
                User.Grade = Data[0].m.o.EMPLOYEE_GRADE_NAME;
                User.FoodingExpense = Convert.ToDecimal(Data[0].n.ALLOWANCE_AMOUNT);
                User.LodgingExpense = Convert.ToDecimal(Data[1].n.ALLOWANCE_AMOUNT);
            }           

            return User;
        }

        public bool EditUserDetails(AddNewUserBOL User, string USERNAME)
        {
            bool EditUser = true;
            try
            {
                MSTR_USER Data = db.MSTR_USER.FirstOrDefault(x => x.ISACTIVE == true && x.USER_ID == User.UserID);
                MSTR_EMPLOYEE_GRADE Grade = db.MSTR_EMPLOYEE_GRADE.FirstOrDefault(x => x.ISACTIVE == true && x.REF_USER_ID == User.UserID);
                TRN_ALLOWANCE Fooding = db.TRN_ALLOWANCE.Where(x => x.ISACTIVE == true && x.REF_USER_ID == User.UserID && x.REF_EXPENDETURE_FOR_ID == "0006").FirstOrDefault();
                TRN_ALLOWANCE Loding = db.TRN_ALLOWANCE.Where(x => x.ISACTIVE == true && x.REF_USER_ID == User.UserID && x.REF_EXPENDETURE_FOR_ID == "0007").FirstOrDefault();

                if (Data != null)
                {
                    if(User.Password == null)
                    {
                        Data.FULL_NAME = User.Name;
                        Data.USER_NAME = User.EmailID;
                        Data.REF_ROLE_ID = User.Role;
                        Data.UPDATED_BY = USERNAME;
                        Data.UPDATED_ON = DateTime.Now;

                        Grade.EMPLOYEE_GRADE_NAME = User.Grade;
                        Grade.UPDATED_BY = USERNAME;
                        Grade.UPDATED_ON = DateTime.Now;

                        Fooding.ALLOWANCE_AMOUNT = User.FoodingExpense;                        
                        Fooding.UPDATED_BY = USERNAME;
                        Fooding.UPDATED_ON = DateTime.Now;

                        Loding.ALLOWANCE_AMOUNT = User.LodgingExpense;
                        Loding.UPDATED_BY = USERNAME;
                        Loding.UPDATED_ON = DateTime.Now;
                    }
                    else
                    {
                        Data.FULL_NAME = User.Name;
                        Data.USER_NAME = User.EmailID;
                        Data.PASSWORD = User.Password;
                        Data.REF_ROLE_ID = User.Role;
                        Data.UPDATED_BY = USERNAME;
                        Data.UPDATED_ON = DateTime.Now;

                        Grade.EMPLOYEE_GRADE_NAME = User.Grade;
                        Grade.UPDATED_BY = USERNAME;
                        Grade.UPDATED_ON = DateTime.Now;

                        Fooding.ALLOWANCE_AMOUNT = User.FoodingExpense;
                        Fooding.UPDATED_BY = USERNAME;
                        Fooding.UPDATED_ON = DateTime.Now;

                        Loding.ALLOWANCE_AMOUNT = User.LodgingExpense;
                        Loding.UPDATED_BY = USERNAME;
                        Loding.UPDATED_ON = DateTime.Now;
                    }                    
                }
                db.SaveChanges();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex);
                EditUser = false;
            }
            return EditUser;
        }
    }
}
