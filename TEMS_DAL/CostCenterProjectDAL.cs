﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TEMS_BOL;
using static TEMS_BOL.CostCenterProjectBOL;

namespace TEMS_DAL
{
    public class CostCenterProjectDAL
    {
        TEMSEntities db = new TEMSEntities();
        public bool AddCostCenterProjectMap(CostCenterProjectBOL costproject, string USERID)
        {
            bool CheckAddCostCenterProjectMap = true;
            try
            {                
                var MapID = db.MSTR_PROJECT_COSTHEAD_MAP.OrderByDescending(x => x.MAP_ID).FirstOrDefault();
                MSTR_PROJECT_COSTHEAD_MAP map = new MSTR_PROJECT_COSTHEAD_MAP();
                if (MapID == null)
                {
                    map.MAP_ID = "MAP0001";
                }
                else
                {
                    map.MAP_ID = "MAP" + (Convert.ToInt32(MapID.MAP_ID.Substring(3, MapID.MAP_ID.Length - 3)) + 1).ToString("D4");
                }
                map.REF_PROJECT_ID = costproject.ProjectId;
                map.REF_COST_CENTER_ID = costproject.CostCenterId;
                map.CREATED_BY = USERID;
                map.CREATED_ON = DateTime.Now;
                map.ISACTIVE = true;
                db.MSTR_PROJECT_COSTHEAD_MAP.Add(map);
                db.SaveChanges();
            }

            catch (Exception ex)
            {
                CheckAddCostCenterProjectMap = false;
            }
            return CheckAddCostCenterProjectMap;
        }

        public List<CostCenterProjectData> CostProjectData(CostCenterProjectBOL costproject)
        {
            List<CostCenterProjectData> CostCenterProjectListData = new List<CostCenterProjectData>();

            var Data = db.MSTR_PROJECT_COSTHEAD_MAP.
                Where(x => (costproject.CostCenterId != null && costproject.CostCenterId != "") ? x.REF_COST_CENTER_ID == costproject.CostCenterId : true).
                Where(x => (costproject.ProjectId != null && costproject.ProjectId != "") ? x.REF_PROJECT_ID == costproject.ProjectId : true).
                
                Where(x => (costproject.Status != null && costproject.Status != "") ? x.ISACTIVE.ToString() == costproject.Status : true).
                Select(x => new
                {
                    CostCenter = x.MSTR_COST_CENTER.COST_CENTER_NAME,
                    Project = x.MSTR_PROJECT.PROJECT_NAME,
                    
                    Id = x.MAP_ID,
                    Status = x.ISACTIVE
                }).ToList();

            foreach (var item in Data)
            {
                CostCenterProjectData Cost = new CostCenterProjectData();
                Cost.CostCenterId = item.CostCenter;
                Cost.ProjectId = item.Project;
                Cost.MapId = item.Id;
                if (item.Status == true)
                {
                    Cost.Status = "Active";
                }
                else
                {
                    Cost.Status = "Inactive";
                }
                CostCenterProjectListData.Add(Cost);
            }
            return CostCenterProjectListData;
        }

        public CostCenterProjectBOL EditCostCenterProject(string CostProjectMapId)
        {
            CostCenterProjectBOL costproject = new CostCenterProjectBOL();

            var Data = db.MSTR_PROJECT_COSTHEAD_MAP.Where(x => x.MAP_ID == CostProjectMapId).ToList();

            if (Data.Count > 0)
            {
                costproject.CostCenterId = Data[0].REF_COST_CENTER_ID;
                costproject.ProjectId = Data[0].REF_PROJECT_ID;
                costproject.Status = Convert.ToString(Data[0].ISACTIVE);
            }
            return costproject;
        }

        public bool EditCostCenterProjectData(CostCenterProjectBOL costproject, string USERID)
        {
            bool EditCostCenterProjectdata = true;
            try
            {
                MSTR_PROJECT_COSTHEAD_MAP Data = db.MSTR_PROJECT_COSTHEAD_MAP.FirstOrDefault(x => x.MAP_ID == costproject.MapId);
                if (Data != null)
                {
                    Data.REF_COST_CENTER_ID = costproject.CostCenterId;
                    Data.REF_PROJECT_ID = costproject.ProjectId;                    
                    Data.UPDATED_BY = USERID;
                    Data.UPDATED_ON = DateTime.Now;
                    Data.ISACTIVE = Convert.ToBoolean(costproject.Status);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                EditCostCenterProjectdata = false;
            }
            return EditCostCenterProjectdata;
        }
    }
}
