//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TEMS_DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class TRN_Advance_Adjustment
    {
        public int AdvanceAdjId { get; set; }
        public string AdvanceAdjNumber { get; set; }
        public string RefAdvanceId { get; set; }
        public int RefPaymentId { get; set; }
        public string Amount { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public bool IsActive { get; set; }
    
        public virtual MSTR_USER MSTR_USER { get; set; }
        public virtual TRN_Advance TRN_Advance { get; set; }
        public virtual TRN_Payment TRN_Payment { get; set; }
    }
}
