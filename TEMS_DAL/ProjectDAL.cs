﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TEMS_BOL;
using static TEMS_BOL.ProjectBOL;

namespace TEMS_DAL
{
    public class ProjectDAL
    {
        TEMSEntities db = new TEMSEntities();

        public List<ProjectData> GetProjectList(ProjectBOL Project)
        {
            List<ProjectData> ProjectListData = new List<ProjectData>();

            var Data = db.MSTR_PROJECT.
                Where(x => (Project.ProjectName != null && Project.ProjectName != "") ? x.PROJECT_ID == Project.ProjectName : true).
                Where(x => (Project.Status != null && Project.Status != "") ? x.ISACTIVE.ToString() == Project.Status : true).
                Select(x => new
                {
                    Name = x.PROJECT_NAME,
                    Id = x.PROJECT_ID,
                    Status = x.ISACTIVE
                }).ToList();

            foreach (var item in Data)
            {
                ProjectData proj = new ProjectData();
                proj.ProjectId = item.Id;
                proj.ProjectName = item.Name;
                if (item.Status == true)
                {
                    proj.Status = "Active";
                }
                else
                {
                    proj.Status = "Inactive";
                }
                ProjectListData.Add(proj);
            }
            return ProjectListData;
        }

        public bool AddProjectData(ProjectBOL Project, string USERID)
        {
            bool checkprojectdata = true;
            try
            {
                var ProjectID = db.MSTR_PROJECT.OrderByDescending(x => x.PROJECT_ID).FirstOrDefault();
                MSTR_PROJECT proj = new MSTR_PROJECT();
                if (ProjectID == null)
                {
                    proj.PROJECT_ID = "PRO0001";
                }
                else
                {
                    proj.PROJECT_ID = "PRO" + (Convert.ToInt32(ProjectID.PROJECT_ID.Substring(3, ProjectID.PROJECT_ID.Length - 3)) + 1).ToString("D4");
                }
                proj.PROJECT_NAME = Project.ProjectName;
                proj.CREATED_BY = USERID;
                proj.CREATED_ON = DateTime.Now;
                proj.ISACTIVE = Convert.ToBoolean(Project.Status);

                db.MSTR_PROJECT.Add(proj);
                db.SaveChanges();
            }
            catch(Exception ex)
            {
                checkprojectdata = false;
            }
            return checkprojectdata;
        }

        public ProjectBOL Editproject(string PId)
        {
            ProjectBOL Project = new ProjectBOL();

            var Data = db.MSTR_PROJECT.Where(x => x.PROJECT_ID == PId).ToList();

            if (Data.Count > 0)
            {                
                Project.ProjectId = Data[0].PROJECT_ID;
                Project.ProjectName = Data[0].PROJECT_NAME;
                Project.Status = Convert.ToString(Data[0].ISACTIVE);
            }
            return Project;
        }

        public bool EditProjectData(ProjectBOL Project, string USERID)
        {
            bool EditProjectdata = true;
            try
            {
                MSTR_PROJECT Data = db.MSTR_PROJECT.FirstOrDefault(x => x.PROJECT_ID == Project.ProjectId);
                if (Data != null)
                {
                    Data.PROJECT_NAME = Project.ProjectName;
                    Data.UPDATED_BY = USERID;
                    Data.UPDATED_ON = DateTime.Now;
                    Data.ISACTIVE = Convert.ToBoolean(Project.Status);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                EditProjectdata = false;
            }
            return EditProjectdata;
        }
    }
}
