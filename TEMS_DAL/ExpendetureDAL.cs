﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TEMS_BOL;
using static TEMS_BOL.SearchExpenditureBOL;
using System.Web;

namespace TEMS_DAL
{
    public class ExpendetureDAL
    {
        TEMSEntities db = new TEMSEntities();

        public int checkduplicate(ExpendetureBOL emp, string UserName, string UserID)
        {
            DateTime DT = DateTime.Today;
            int list = new int();
            var duplicateexpID = db.TRN_EXPENDETURE_SUMMARY.OrderByDescending(x => x.EXPENDETURE_NO).Where(x => x.CREATED_BY == UserID).FirstOrDefault();
            if (duplicateexpID == null)
            {
                emp.expendeture_no = "1" + DT.ToString("yy") + DT.AddYears(1).ToString("yy") + UserID + "0001";
            }
            else
            {
                emp.expendeture_no = "1" + DT.ToString("yy") + DT.AddYears(1).ToString("yy") + UserID + (Convert.ToInt32(duplicateexpID.EXPENDETURE_NO.Substring(8, duplicateexpID.EXPENDETURE_NO.Length - 8)) + 1).ToString("D4");
            }
            return list;
        }

        public List<SelectListItem> ListPayment()
        {
            List<SelectListItem> PaymentForList = new List<SelectListItem>();
            List<MSTR_EXPENDETURE_FOR> PaymentData = db.MSTR_EXPENDETURE_FOR.Where(x => x.ISACTIVE == true).ToList();
            PaymentData.ForEach(x => PaymentForList.Add(new SelectListItem { Text = x.EXPENDETURE_FOR_NAME, Value = x.EXPENDETURE_FOR_ID }));
            return PaymentForList;
        }

        public List<SelectListItem> ListCostCenter()
        {
            List<SelectListItem> CostCenterList = new List<SelectListItem>();
            List<MSTR_COST_CENTER> CostCenterData = db.MSTR_COST_CENTER.Where(x => x.ISACTIVE == true).ToList();
            CostCenterData.ForEach(x => CostCenterList.Add(new SelectListItem { Text = x.COST_CENTER_NAME, Value = x.COST_CENTER_ID }));
            return CostCenterList;
        }

        public List<SelectListItem> ListSate()
        {
            List<SelectListItem> StateList = new List<SelectListItem>();
            List<MSTR_STATE> StateData = db.MSTR_STATE.Where(x => x.ISACTIVE == true).ToList();
            StateData.ForEach(x => StateList.Add(new SelectListItem { Text = x.STATE_NAME, Value = x.STATE_ID }));
            return StateList;
        }

        public List<SelectListItem> ListProject()
        {
            List<SelectListItem> ProjectList = new List<SelectListItem>();
            List<MSTR_PROJECT> ProjectData = db.MSTR_PROJECT.Where(x => x.ISACTIVE == true).ToList();
            ProjectData.ForEach(x => ProjectList.Add(new SelectListItem { Text = x.PROJECT_NAME, Value = x.PROJECT_ID }));
            return ProjectList;
        }

        public List<MSTR_CITY> FetchCity(string STATE_ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<MSTR_CITY> citylist = db.MSTR_CITY.Where(x => x.REF_STATE_ID == STATE_ID && x.ISACTIVE == true).ToList();
            return citylist;
        }

        public List<TRN_ALLOWANCE> FetchAllowance(string Expenditure_ID, string UserName)
        {
            db.Configuration.ProxyCreationEnabled = false;
            string[] empname = db.MSTR_EMPLOYEE.Where(x => x.EMPLOYEE_NAME == UserName && x.ISACTIVE == true).Select(x => x.REF_EMPLOYEE_GRADE_ID).ToArray();

            List<TRN_ALLOWANCE> allowancelist = db.TRN_ALLOWANCE.Where(x => empname.Contains(x.REF_EMPLOYEE_GRADE_ID) && x.ISACTIVE == true && x.REF_EXPENDETURE_FOR_ID == Expenditure_ID).ToList();
            return allowancelist;
        }

        public List<SelectListItem> ListAgency()
        {
            List<SelectListItem> AgencyList = new List<SelectListItem>();
            List<MSTR_AGENCY> AgencyData = db.MSTR_AGENCY.Where(x => x.ISACTIVE == true).ToList();
            AgencyData.ForEach(x => AgencyList.Add(new SelectListItem { Text = x.AGENCY_NAME, Value = x.AGENCY_ID }));
            return AgencyList;
        }

        public List<SelectListItem> getdata(ExpendetureBOL emp, string UserName)
        {
            List<SelectListItem> savedata = new List<SelectListItem>();
            TRN_EXPENDETURE_SUMMARY summary = new TRN_EXPENDETURE_SUMMARY();
            summary.EXPENDETURE_NO = emp.expendeture_no;
            summary.REF_STATE_ID = emp.State;
            summary.JOURNEY_FROM = emp.Journey_from;
            summary.JOURNEY_TO = emp.Journey_to;
            summary.Journey_Type = emp.Journey_type;
            summary.JOURNEY_DATE = DateTime.ParseExact(emp.journey_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            summary.DISTANCE_COVERED = emp.distance_covered;
            summary.CREATED_ON = DateTime.Now;
            summary.CREATED_BY = UserName;
            summary.UPDATED_ON = DateTime.Now;
            summary.UPDATED_BY = UserName;
            summary.ISACTIVE = true;
            summary.REF_APPROVE_STATUS_ID = "AS0006";
            summary.CAR_HIRE = emp.car_hire;
            summary.REMARKS = emp.Remarks;
            db.TRN_EXPENDETURE_SUMMARY.Add(summary);

            List<TRN_EXPENDETURE_DETAILS> ListExpDetails = new List<TRN_EXPENDETURE_DETAILS>();
            foreach (var item in emp.ExpendetureDetailsList)
            {
                TRN_EXPENDETURE_DETAILS ExpDetails = new TRN_EXPENDETURE_DETAILS();
                ExpDetails.EXPENDETURE_DETAILS_ID = Guid.NewGuid().ToString();
                ExpDetails.REF_EXPENDETURE_NO = emp.expendeture_no;
                ExpDetails.REF_COST_CENTER_ID = item.Cost_Center;
                ExpDetails.REF_PROJECT_ID = item.project;
                ExpDetails.REF_EXPENDETURE_FOR_ID = item.ExpendetureFor;
                ExpDetails.ENTRY_AMOUNT = Convert.ToDecimal(item.expendeture_amount);
                ExpDetails.ClaimedAmount = Convert.ToDecimal(item.expendeture_amount);
                ExpDetails.ATTACHMENT = item.expendeture_attachmentpATH;
                ExpDetails.CREATED_ON = DateTime.Now;
                ExpDetails.CREATED_BY = UserName;
                ExpDetails.ISACTIVE = true;
                ExpDetails.REF_USER_ID = UserName;
                ListExpDetails.Add(ExpDetails);
            }
            if (ListExpDetails.Count > 0)
            {
                db.TRN_EXPENDETURE_DETAILS.AddRange(ListExpDetails);
            }

            if (emp.car_hire == true)
            {
                TRN_CAR_DETAILS CarDetails = new TRN_CAR_DETAILS();
                CarDetails.REF_AGENCY_ID = emp.Agency;
                CarDetails.CAR_DETAILS_ID = Guid.NewGuid().ToString();
                CarDetails.REF_EXPENDETURE_NO = emp.expendeture_no;
                CarDetails.CAR_NO = emp.car_no;
                CarDetails.DRIVER_NAME = emp.driver_name;
                CarDetails.CAR_RENTAL_CHARGES = Convert.ToDecimal(emp.car_rental_charge);
                CarDetails.MILEAGE = emp.Car_Mileage;
                CarDetails.FUEL_RATE = Convert.ToDecimal(emp.fuel_rate);
                CarDetails.CAR_RENT_RECEIPT = emp.car_rent_reciptPath;
                CarDetails.TOLL_TAX_RECEIPT = emp.toll_tax_reciptPath;
                CarDetails.FUEL_COST_RECEIPT = emp.fuel_cost_reciptPath;
                CarDetails.CREATED_ON = DateTime.Now;
                CarDetails.CREATED_BY = UserName;
                CarDetails.UPDATED_ON = DateTime.Now;
                CarDetails.UPDATED_BY = UserName;
                CarDetails.ISACTIVE = true;
                db.TRN_CAR_DETAILS.Add(CarDetails);

                List<TRN_EXPENDETURE_DETAILS> ListExpDetail = new List<TRN_EXPENDETURE_DETAILS>();
                List<TRN_CAR_EXPENDETURE_DETAILS> ListCarExpDetails = new List<TRN_CAR_EXPENDETURE_DETAILS>();
                foreach (var item in emp.CarExpendetureList)
                {
                    TRN_CAR_EXPENDETURE_DETAILS CarExpDetails = new TRN_CAR_EXPENDETURE_DETAILS();
                    CarExpDetails.CAR_EXPENDETURE_DETAILS_ID = Guid.NewGuid().ToString();
                    CarExpDetails.REF_EXPENDETURE_NO = emp.expendeture_no;
                    CarExpDetails.REF_COST_CENTER_ID = item.Cost_Center;
                    CarExpDetails.DISTANCE_TRAVEL = item.distance_travel;
                    CarExpDetails.FUEL_COST = Convert.ToDecimal(item.fuel_cost_for_cost_center);
                    CarExpDetails.CAR_RENT = Convert.ToDecimal(item.car_rent_for_cost_center);
                    CarExpDetails.TOLL_TAX = Convert.ToDecimal(item.toll_tax);
                    CarExpDetails.TOTAL = Convert.ToDecimal(item.total);
                    CarExpDetails.CREATED_BY = UserName;
                    CarExpDetails.CREATED_ON = DateTime.Now;
                    CarExpDetails.ISACTIVE = true;
                    ListCarExpDetails.Add(CarExpDetails);

                    TRN_EXPENDETURE_DETAILS ExpDetail = new TRN_EXPENDETURE_DETAILS();
                    ExpDetail.EXPENDETURE_DETAILS_ID = Guid.NewGuid().ToString();
                    ExpDetail.REF_EXPENDETURE_NO = emp.expendeture_no;
                    ExpDetail.REF_COST_CENTER_ID = item.Cost_Center;
                    ExpDetail.REF_EXPENDETURE_FOR_ID = "0002";
                    ExpDetail.ENTRY_AMOUNT = Convert.ToDecimal(item.fuel_cost_for_cost_center);
                    var Carfuel = ListExpDetails.Where(x => x.REF_EXPENDETURE_FOR_ID == "0002").Select(x => x.ENTRY_AMOUNT).ToList();
                    if (Carfuel.Count > 0)
                    {
                        ExpDetail.ClaimedAmount = Convert.ToDecimal(item.fuel_cost_for_cost_center) - Convert.ToDecimal(Carfuel[0]);
                    }
                    else
                    {
                        ExpDetail.ClaimedAmount = Convert.ToDecimal(item.fuel_cost_for_cost_center);
                    }

                    ExpDetail.CREATED_ON = DateTime.Now;
                    ExpDetail.CREATED_BY = UserName;
                    ExpDetail.ISACTIVE = true;
                    ExpDetail.REF_AGENCY_ID = emp.Agency;
                    ListExpDetail.Add(ExpDetail);

                    TRN_EXPENDETURE_DETAILS ExpDetai = new TRN_EXPENDETURE_DETAILS();
                    ExpDetai.EXPENDETURE_DETAILS_ID = Guid.NewGuid().ToString();
                    ExpDetai.REF_EXPENDETURE_NO = emp.expendeture_no;
                    ExpDetai.REF_COST_CENTER_ID = item.Cost_Center;
                    ExpDetai.REF_EXPENDETURE_FOR_ID = "0003";
                    ExpDetai.ENTRY_AMOUNT = Convert.ToDecimal(item.car_rent_for_cost_center);
                    ExpDetai.ClaimedAmount = Convert.ToDecimal(item.fuel_cost_for_cost_center);
                    ExpDetai.ClaimedAmount = Convert.ToDecimal(item.car_rent_for_cost_center);
                    ExpDetai.CREATED_ON = DateTime.Now;
                    ExpDetai.CREATED_BY = UserName;
                    ExpDetai.ISACTIVE = true;
                    ExpDetai.REF_AGENCY_ID = emp.Agency;
                    ListExpDetail.Add(ExpDetai);

                    TRN_EXPENDETURE_DETAILS ExpDeta = new TRN_EXPENDETURE_DETAILS();
                    ExpDeta.EXPENDETURE_DETAILS_ID = Guid.NewGuid().ToString();
                    ExpDeta.REF_EXPENDETURE_NO = emp.expendeture_no;
                    ExpDeta.REF_COST_CENTER_ID = item.Cost_Center;
                    ExpDeta.REF_EXPENDETURE_FOR_ID = "0004";
                    ExpDeta.ENTRY_AMOUNT = Convert.ToDecimal(item.toll_tax);
                    ExpDeta.ClaimedAmount = Convert.ToDecimal(item.toll_tax);
                    ExpDeta.CREATED_ON = DateTime.Now;
                    ExpDeta.CREATED_BY = UserName;
                    ExpDeta.ISACTIVE = true;
                    ExpDeta.REF_AGENCY_ID = emp.Agency;
                    ListExpDetail.Add(ExpDeta);
                }
                if (ListExpDetail.Count > 0)
                {
                    db.TRN_EXPENDETURE_DETAILS.AddRange(ListExpDetail);
                }
                if (ListCarExpDetails.Count > 0)
                {
                    db.TRN_CAR_EXPENDETURE_DETAILS.AddRange(ListCarExpDetails);
                }
            }

            EXPENDETURE_STATUS_LOG StatusLog = new EXPENDETURE_STATUS_LOG();
            StatusLog.EXPENDETURE_APPROVAL_STATUS_LOG_ID = Guid.NewGuid().ToString();
            StatusLog.REF_EXPENDETURE_NO = emp.expendeture_no;
            StatusLog.REF_APPROVE_STATUS_ID = "AS0006";
            StatusLog.CREATED_BY = UserName;
            StatusLog.CREATED_ON = DateTime.Now;
            StatusLog.UPDATED_ON = DateTime.Now;
            StatusLog.UPDATED_BY = UserName;
            StatusLog.ISACTIVE = true;
            db.EXPENDETURE_STATUS_LOG.Add(StatusLog);

            db.SaveChanges();
            return savedata;
        }

        public List<TRN_EXPENDETURE_SUMMARY> FetchExpendituredata(string Expenditure_No)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<TRN_EXPENDETURE_SUMMARY> Datalist = db.TRN_EXPENDETURE_SUMMARY.Where(x => x.ISACTIVE == true && x.EXPENDETURE_NO == Expenditure_No).ToList();
            return Datalist;
        }

        public List<SelectListItem> ListExpenditureBy(string USERNAME)
        {
            List<SelectListItem> ExpenditureByList = new List<SelectListItem>();
            List<MSTR_USER> ExpenditureByData = db.MSTR_USER.Where(x => x.ISACTIVE == true /*&& x.REF_ROLE_ID == "0002"*/ && x.FULL_NAME == USERNAME).ToList();
            ExpenditureByData.ForEach(x => ExpenditureByList.Add(new SelectListItem { Text = x.FULL_NAME, Value = x.USER_ID }));
            return ExpenditureByList;
        }

        public List<SelectListItem> ListExpenditureBy()
        {
            List<SelectListItem> ExpenditureByList = new List<SelectListItem>();
            List<MSTR_USER> ExpenditureByData = db.MSTR_USER.Where(x => x.ISACTIVE == true /*&& x.REF_ROLE_ID == "0002"*/).ToList();
            ExpenditureByData.ForEach(x => ExpenditureByList.Add(new SelectListItem { Text = x.FULL_NAME, Value = x.USER_ID }));
            return ExpenditureByList;
        }

        public List<SelectListItem> ListCarNo()
        {
            List<SelectListItem> CarNOList = new List<SelectListItem>();
            var CarNOData = db.TRN_CAR_DETAILS.Where(x => x.ISACTIVE == true).Select(x => new { CarNo = x.CAR_NO }).Distinct().ToList();
            CarNOData.ForEach(item => CarNOList.Add(new SelectListItem { Text = item.CarNo, Value = item.CarNo }));
            return CarNOList;
        }

        public List<SelectListItem> ListStatus()
        {
            List<SelectListItem> StatusList = new List<SelectListItem>();
            List<MSTR_APPROVE_STATUS> StatusData = db.MSTR_APPROVE_STATUS.Where(x => x.ISACTIVE == true).ToList();
            StatusData.ForEach(x => StatusList.Add(new SelectListItem { Text = x.APPROVE_STATUS_NAME, Value = x.APPROVE_STATUS_ID }));
            return StatusList;
        }

        public List<SearchTableList> FetchTableData(SearchExpenditureBOL searchdetails)
        {
            List<SearchTableList> TableDataList = new List<SearchTableList>();
            DateTime FromDate = (searchdetails.FromDate != null && searchdetails.FromDate != "") ? DateTime.ParseExact(searchdetails.FromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now;
            DateTime ToDate = (searchdetails.FromDate != null && searchdetails.ToDate != "") ? DateTime.ParseExact(searchdetails.ToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now;

            //if()
            var ExpDetails = db.TRN_EXPENDETURE_DETAILS
                .Join(db.TRN_EXPENDETURE_SUMMARY,
                c => c.REF_EXPENDETURE_NO,
                o => o.EXPENDETURE_NO,
                (c, o) => new { c, o })
                .Where(x => x.c.ISACTIVE == true && searchdetails.View == "2").
                    Where(x => (searchdetails.ExpenditureNo != null && searchdetails.ExpenditureNo != "") ? x.c.REF_EXPENDETURE_NO == searchdetails.ExpenditureNo : true).
                    Where(x => (searchdetails.CostHead != null && searchdetails.CostHead != "") ? x.c.REF_COST_CENTER_ID == searchdetails.CostHead : true).
                    Where(x => (searchdetails.ExpenditureHead != null && searchdetails.ExpenditureHead != "") ? x.c.REF_EXPENDETURE_FOR_ID == searchdetails.ExpenditureHead : true).
                    Where(x => (searchdetails.FromDate != null && searchdetails.FromDate != "") ? x.o.JOURNEY_DATE >= FromDate : true).
                    Where(x => (searchdetails.ToDate != null && searchdetails.ToDate != "") ? x.o.JOURNEY_DATE <= ToDate : true).
                    ToList();

            decimal ExpDetailsAmount = 0;
            foreach (var item in ExpDetails)
            {
                SearchTableList TableList = new SearchTableList();
                TableList.CostHead = item.c.MSTR_COST_CENTER.COST_CENTER_NAME;
                TableList.ExpenditureHead = item.c.MSTR_EXPENDETURE_FOR.EXPENDETURE_FOR_NAME;
                TableList.Amount = Convert.ToDecimal(item.c.ENTRY_AMOUNT);
                TableDataList.Add(TableList);
            }
            ExpDetailsAmount = Convert.ToDecimal(ExpDetails.Sum(x => x.c.ENTRY_AMOUNT));

            return TableDataList;
        }

        public List<SummaryTableList> FetchSummaryTableData(string USERNAME)
        {
            List<SummaryTableList> SummaryTableDataList = new List<SummaryTableList>();
            SearchExpenditureBOL searchdetails = new SearchExpenditureBOL();

            var Expendituresummary = db.TRN_EXPENDETURE_SUMMARY
                .Join(db.TRN_EXPENDETURE_DETAILS,
                c => c.EXPENDETURE_NO,
                o => o.REF_EXPENDETURE_NO,
                (c, o) => new { c, o }).
                Join(db.EXPENDETURE_STATUS_LOG,
                m => m.c.EXPENDETURE_NO,
                n => n.REF_EXPENDETURE_NO,
                (m, n) => new { m, n }).
                Where(x => x.m.c.ISACTIVE == true && x.n.ISACTIVE == true && x.m.c.CREATED_BY == USERNAME/*&& x.m.c.CAR_HIRE == false*/).
                Select(x => new
                {
                    Date = x.m.c.CREATED_ON,
                    Exp_no = x.m.c.EXPENDETURE_NO,
                    Exp_by = x.m.c.MSTR_USER.FULL_NAME,
                    Journeydate = x.m.c.JOURNEY_DATE,
                    TotalAmount = x.m.o.ENTRY_AMOUNT,
                    ApprovedAmount = x.m.o.APPROVED_AMOUNT,
                    Status = x.n.MSTR_APPROVE_STATUS.APPROVE_STATUS_NAME,
                    CarHire = x.m.c.CAR_HIRE
                }).OrderBy(x => x.Journeydate).OrderByDescending(x => x.Exp_no).ToList();

            string[] DistictExpNo = Expendituresummary.Select(x => x.Exp_no).Distinct().ToArray();

            foreach (var item in DistictExpNo)
            {
                var particularExpno = Expendituresummary.Where(x => x.Exp_no == item).ToList();
                SummaryTableList TableList = new SummaryTableList();
                TableList.Date = String.Format("{0:dd/MM/yyyy}", particularExpno[0].Date);
                TableList.ExpenditureNo = particularExpno[0].Exp_no;
                TableList.ExpenditureBy = particularExpno[0].Exp_by;
                TableList.JourneyDate = String.Format("{0:dd/MM/yyyy}", particularExpno[0].Journeydate);
                TableList.TotalAmount = /*String.Format("{0:0.00}",*/Convert.ToDecimal(particularExpno.Sum(x => x.TotalAmount));

                TableList.ApprovedAmount = Convert.ToDecimal(particularExpno.Sum(x => x.ApprovedAmount));

                TableList.Status = particularExpno[0].Status;
                SummaryTableDataList.Add(TableList);
            }
            return SummaryTableDataList;
        }

        public List<SummaryTableList> FetchSummaryTableData()
        {
            List<SummaryTableList> SummaryTableDataList = new List<SummaryTableList>();
            SearchExpenditureBOL searchdetails = new SearchExpenditureBOL();

            var Expendituresummary = db.TRN_EXPENDETURE_SUMMARY
                .Join(db.TRN_EXPENDETURE_DETAILS,
                c => c.EXPENDETURE_NO,
                o => o.REF_EXPENDETURE_NO,
                (c, o) => new { c, o }).
                Join(db.EXPENDETURE_STATUS_LOG,
                m => m.c.EXPENDETURE_NO,
                n => n.REF_EXPENDETURE_NO,
                (m, n) => new { m, n }).
                Where(x => x.m.c.ISACTIVE == true && x.n.ISACTIVE == true && x.m.o.ISACTIVE == true).
                Select(x => new
                {
                    Date = x.m.c.CREATED_ON,
                    Exp_no = x.m.c.EXPENDETURE_NO,
                    Exp_by = x.m.c.MSTR_USER.FULL_NAME,
                    Journeydate = x.m.c.JOURNEY_DATE,
                    TotalAmount = x.m.o.ENTRY_AMOUNT,
                    ApprovedAmount = x.m.o.APPROVED_AMOUNT,
                    Status = x.n.MSTR_APPROVE_STATUS.APPROVE_STATUS_NAME,
                    CarHire = x.m.c.CAR_HIRE
                }).OrderBy(x => x.Journeydate).OrderByDescending(x => x.Exp_no).ToList();

            string[] DistictExpNo = Expendituresummary.Select(x => x.Exp_no).Distinct().ToArray();

            foreach (var item in DistictExpNo)
            {
                var particularExpno = Expendituresummary.Where(x => x.Exp_no == item).ToList();
                var usercarfuelclaim = db.TRN_EXPENDETURE_DETAILS.Where(x => x.ISACTIVE == true && x.REF_EXPENDETURE_NO == item && x.REF_USER_ID != null && x.REF_EXPENDETURE_FOR_ID == "0002").Select(x => x.ENTRY_AMOUNT).FirstOrDefault();
                var Agencycarfuelclaim = db.TRN_EXPENDETURE_DETAILS.Where(x => x.ISACTIVE == true && x.REF_EXPENDETURE_NO == item && x.REF_AGENCY_ID != null && x.REF_EXPENDETURE_FOR_ID == "0002").Select(x => x.ENTRY_AMOUNT).FirstOrDefault();
                SummaryTableList TableList = new SummaryTableList();
                TableList.Date = String.Format("{0:dd/MM/yyyy}", particularExpno[0].Date);
                TableList.ExpenditureNo = particularExpno[0].Exp_no;
                TableList.ExpenditureBy = particularExpno[0].Exp_by;
                TableList.JourneyDate = String.Format("{0:dd/MM/yyyy}", particularExpno[0].Journeydate);
                if (Agencycarfuelclaim == null)
                {
                    TableList.TotalAmount = Convert.ToDecimal(particularExpno.Sum(x => x.TotalAmount)) - Convert.ToDecimal(usercarfuelclaim);
                }
                else
                {
                    TableList.TotalAmount = Convert.ToDecimal(particularExpno.Sum(x => x.TotalAmount));
                }

                TableList.ApprovedAmount = Convert.ToDecimal(particularExpno.Sum(x => x.ApprovedAmount));

                TableList.Status = particularExpno[0].Status;
                SummaryTableDataList.Add(TableList);
            }
            return SummaryTableDataList;
        }

        public List<SummaryTableList> FetchSummaryTableData(SearchExpenditureBOL searchdetails)
        {
            List<SummaryTableList> SummaryTableDataList = new List<SummaryTableList>();

            DateTime FromDate = (searchdetails.FromDate != null && searchdetails.FromDate != "") ? DateTime.ParseExact(searchdetails.FromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now;
            DateTime ToDate = (searchdetails.FromDate != null && searchdetails.ToDate != "") ? DateTime.ParseExact(searchdetails.ToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now;

            var Expendituresummary = db.TRN_EXPENDETURE_SUMMARY
                .Join(db.TRN_EXPENDETURE_DETAILS,
                c => c.EXPENDETURE_NO,
                o => o.REF_EXPENDETURE_NO,
                (c, o) => new { c, o }).
                Join(db.EXPENDETURE_STATUS_LOG,
                m => m.c.EXPENDETURE_NO,
                n => n.REF_EXPENDETURE_NO,
                (m, n) => new { m, n }).
                Where(x => x.m.c.ISACTIVE == true && x.n.ISACTIVE == true && x.m.o.ISACTIVE == true && searchdetails.View == "1").
                Where(x => (searchdetails.ExpenditureNo != null && searchdetails.ExpenditureNo != "") ? x.m.c.EXPENDETURE_NO == searchdetails.ExpenditureNo : true).
                Where(x => (searchdetails.ExpenditureBy != null && searchdetails.ExpenditureBy != "") ? x.m.c.CREATED_BY == searchdetails.ExpenditureBy : true).
                Where(x => (searchdetails.CostHead != null && searchdetails.CostHead != "") ? x.m.o.REF_COST_CENTER_ID == searchdetails.CostHead : true).
                Where(x => (searchdetails.ExpenditureHead != null && searchdetails.ExpenditureHead != "") ? x.m.o.REF_EXPENDETURE_FOR_ID == searchdetails.ExpenditureHead : true).
                Where(x => (searchdetails.FromDate != null && searchdetails.FromDate != "") ? x.m.c.JOURNEY_DATE >= FromDate : true).
                Where(x => (searchdetails.ToDate != null && searchdetails.ToDate != "") ? x.m.c.JOURNEY_DATE <= ToDate : true).
                Where(x => (searchdetails.Status != null && searchdetails.Status != "") ? x.n.REF_APPROVE_STATUS_ID == searchdetails.Status : true).

                Select(x => new
                {
                    Date = x.m.c.CREATED_ON,
                    Exp_no = x.m.c.EXPENDETURE_NO,
                    Exp_by = x.m.c.MSTR_USER.FULL_NAME,
                    Journeydate = x.m.c.JOURNEY_DATE,
                    USER_ID = x.m.o.REF_USER_ID,
                    AGENCY_ID = x.m.o.REF_AGENCY_ID,
                    TotalAmount = x.m.o.ClaimedAmount,
                    ApprovedAmount = x.m.o.APPROVED_AMOUNT,
                    Staus = x.n.MSTR_APPROVE_STATUS.APPROVE_STATUS_NAME
                }).OrderBy(x => x.Journeydate).OrderByDescending(x => x.Exp_no).ToList();

            string[] DistictExpNo = Expendituresummary.Select(x => x.Exp_no).Distinct().ToArray();



            foreach (var item in DistictExpNo)
            {
                var particularExpno = Expendituresummary.Where(x => x.Exp_no == item).ToList();
                var usercarfuelclaim = db.TRN_EXPENDETURE_DETAILS.Where(x => x.ISACTIVE == true && x.REF_EXPENDETURE_NO == item && x.REF_USER_ID != null && x.REF_EXPENDETURE_FOR_ID == "0002").Select(x => x.ENTRY_AMOUNT).FirstOrDefault();
                var Agencycarfuelclaim = db.TRN_EXPENDETURE_DETAILS.Where(x => x.ISACTIVE == true && x.REF_EXPENDETURE_NO == item && x.REF_AGENCY_ID != null && x.REF_EXPENDETURE_FOR_ID == "0002").Select(x => x.ENTRY_AMOUNT).FirstOrDefault();
                SummaryTableList TableList = new SummaryTableList();
                TableList.Date = String.Format("{0:dd/MM/yyyy}", particularExpno[0].Date);
                TableList.ExpenditureNo = particularExpno[0].Exp_no;
                TableList.ExpenditureBy = particularExpno[0].Exp_by;
                TableList.JourneyDate = String.Format("{0:dd/MM/yyyy}", particularExpno[0].Journeydate);
                if (Agencycarfuelclaim != null)
                {
                    TableList.TotalAmount = Convert.ToDecimal(particularExpno.Sum(x => x.TotalAmount)) - Convert.ToDecimal(usercarfuelclaim);
                }
                else
                {
                    TableList.TotalAmount = Convert.ToDecimal(particularExpno.Sum(x => x.TotalAmount));
                }

                TableList.ApprovedAmount = Convert.ToDecimal(particularExpno.Sum(x => x.ApprovedAmount));
                TableList.Status = particularExpno[0].Staus;
                SummaryTableDataList.Add(TableList);
            }
            return SummaryTableDataList;
        }

        public List<CarSummaryTableList> FetchCarSummaryData(SearchExpenditureBOL searchdetails)
        {
            List<CarSummaryTableList> CarSummaryTableDataList = new List<CarSummaryTableList>();
            DateTime FromDate = (searchdetails.FromDate != null && searchdetails.FromDate != "") ? DateTime.ParseExact(searchdetails.FromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now;
            DateTime ToDate = (searchdetails.FromDate != null && searchdetails.ToDate != "") ? DateTime.ParseExact(searchdetails.ToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now;

            var CarSummaryData = db.TRN_CAR_EXPENDETURE_DETAILS
                .Join(db.EXPENDETURE_STATUS_LOG,
                c => c.REF_EXPENDETURE_NO,
                o => o.REF_EXPENDETURE_NO,
                (c, o) => new { c, o }).
                Join(db.TRN_CAR_DETAILS,
                a => a.c.REF_EXPENDETURE_NO,
                b => b.REF_EXPENDETURE_NO,
                (a, b) => new { a, b }).
                Join(db.TRN_EXPENDETURE_SUMMARY,
                m => m.a.c.REF_EXPENDETURE_NO,
                n => n.EXPENDETURE_NO,
                (m, n) => new { m, n }).
                Where(x => x.m.a.c.ISACTIVE == true && searchdetails.View == "3").
                Where(x => (searchdetails.CarNO != null && searchdetails.CarNO != "") ? x.m.b.CAR_NO == searchdetails.CarNO : true).
                Where(x => (searchdetails.ExpenditureBy != null && searchdetails.ExpenditureBy != "") ? x.m.a.c.CREATED_BY == searchdetails.ExpenditureBy : true).
                Where(x => (searchdetails.CostHead != null && searchdetails.CostHead != "") ? x.m.a.c.REF_COST_CENTER_ID == searchdetails.CostHead : true).
                Where(x => (searchdetails.FromDate != null && searchdetails.FromDate != "") ? x.n.JOURNEY_DATE >= FromDate : true).
                Where(x => (searchdetails.ToDate != null && searchdetails.ToDate != "") ? x.n.JOURNEY_DATE <= ToDate : true).
                Select(x => new
                {
                    Date = x.n.JOURNEY_DATE,
                    Exp_no = x.m.a.c.REF_EXPENDETURE_NO,
                    Exp_By = x.n.MSTR_USER.FULL_NAME,
                    Submit_Date = x.n.CREATED_ON,
                    CarNo = x.m.b.CAR_NO,
                    CostHead = x.m.a.c.MSTR_COST_CENTER.COST_CENTER_NAME,
                    Amount = x.m.a.c.TOTAL,
                    Staus = x.m.a.o.MSTR_APPROVE_STATUS.APPROVE_STATUS_NAME,
                }).OrderBy(x => x.Date).ToList();

            var DriverFooding_Lodging = db.TRN_EXPENDETURE_DETAILS.Where(x => x.ISACTIVE == true).ToList();

            //string[] DistinctExpNO = CarSummaryData.Select(x => x.Exp_no).Distinct().ToArray();
            foreach (var item in CarSummaryData/*DistinctExpNO*/)
            {
                //var particularExpno = CarSummaryData.Where(x => x.Exp_no == item).ToList();
                CarSummaryTableList TableList = new CarSummaryTableList();
                TableList.Date = String.Format("{0:dd/MM/yyyy}", /*particularExpno[0]*/item.Date);
                TableList.ExpenditureNo = /*particularExpno[0]*/item.Exp_no;
                TableList.ExpenditureBy = item.Exp_By;
                TableList.SubmitDate = String.Format("{0:dd/MM/yyyy}", item.Submit_Date);
                TableList.CarNo = /*particularExpno[0]*/item.CarNo;
                TableList.CostHead = /*particularExpno[0]*/item.CostHead;

                foreach (var i in DriverFooding_Lodging)
                {
                    var fooding = DriverFooding_Lodging.Where(x => x.REF_EXPENDETURE_NO == item.Exp_no && x.REF_EXPENDETURE_FOR_ID == "0011").ToList();
                    var lodging = DriverFooding_Lodging.Where(x => x.REF_EXPENDETURE_NO == item.Exp_no && x.REF_EXPENDETURE_FOR_ID == "0012").ToList();
                    if (fooding.Count > 0 && lodging.Count > 0)
                    {
                        TableList.TotalAmount = Convert.ToDecimal((item.Amount) + fooding[0].ENTRY_AMOUNT + lodging[0].ENTRY_AMOUNT);
                    }
                    else
                    {
                        TableList.TotalAmount = Convert.ToDecimal(item.Amount);
                    }
                }
                TableList.Status = item.Staus;
                CarSummaryTableDataList.Add(TableList);
            }

            return CarSummaryTableDataList;
        }

        public List<CarDetailsTableList> FetchCarDetailsData(SearchExpenditureBOL searchdetails)
        {
            List<CarDetailsTableList> CarDetailsTableDataList = new List<CarDetailsTableList>();
            DateTime FromDate = (searchdetails.FromDate != null && searchdetails.FromDate != "") ? DateTime.ParseExact(searchdetails.FromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now;
            DateTime ToDate = (searchdetails.FromDate != null && searchdetails.ToDate != "") ? DateTime.ParseExact(searchdetails.ToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now;

            var CarDetailsData = db.TRN_EXPENDETURE_DETAILS/*TRN_CAR_EXPENDETURE_DETAILS*/
                .Join(db.EXPENDETURE_STATUS_LOG,
                c => c.REF_EXPENDETURE_NO,
                o => o.REF_EXPENDETURE_NO,
                (c, o) => new { c, o }).
                Join(db.TRN_CAR_DETAILS,
                a => a.c.REF_EXPENDETURE_NO,
                b => b.REF_EXPENDETURE_NO,
                (a, b) => new { a, b }).
                Join(db.TRN_EXPENDETURE_SUMMARY,
                m => m.a.c.REF_EXPENDETURE_NO,
                n => n.EXPENDETURE_NO,
                (m, n) => new { m, n }).
                //Join(db.TRN_EXPENDETURE_DETAILS,
                //e => e.m.a.c.REF_EXPENDETURE_NO,
                //f => f.REF_EXPENDETURE_NO,
                //(e, f) => new { e, f }).
                Where(x => x.m.a.c.ISACTIVE == true && x.m.b.CAR_NO == searchdetails.CarNO && searchdetails.View == "4").
                Where(x => (searchdetails.ExpenditureBy != null && searchdetails.ExpenditureBy != "") ? x.m.a.c.CREATED_BY == searchdetails.ExpenditureBy : true).
                Where(x => (searchdetails.CostHead != null && searchdetails.CostHead != "") ? x.m.a.c.REF_COST_CENTER_ID == searchdetails.CostHead : true).
                Where(x => (searchdetails.ExpenditureHead != null && searchdetails.ExpenditureHead != "") ? x.m.a.c.REF_EXPENDETURE_FOR_ID == searchdetails.ExpenditureHead : true).
                Where(x => (searchdetails.FromDate != null && searchdetails.FromDate != "") ? x.n.JOURNEY_DATE >= FromDate : true).
                Where(x => (searchdetails.ToDate != null && searchdetails.ToDate != "") ? x.n.JOURNEY_DATE <= ToDate : true).
                Select(x => new
                {
                    Date = x.n.JOURNEY_DATE,
                    Exp_no = x.m.a.c.REF_EXPENDETURE_NO,
                    CostHead = x.m.a.c.MSTR_COST_CENTER.COST_CENTER_NAME,
                    CarHireAmount = x.m.a.c.ENTRY_AMOUNT,
                    CarFuelAmount = x.m.a.c.ENTRY_AMOUNT/*e.m.a.c.FUEL_COST*/,
                    CarTollTax = x.m.a.c.ENTRY_AMOUNT/*e.m.a.c.TOLL_TAX*/,
                    //Amount = x.m.a.c.TOTAL,
                    Staus = x.m.a.o.MSTR_APPROVE_STATUS.APPROVE_STATUS_NAME,
                    exphead = x.m.a.c.MSTR_EXPENDETURE_FOR.EXPENDETURE_FOR_NAME
                }).OrderByDescending(x => x.Date).ToList();

            foreach (var item in CarDetailsData)
            {
                CarDetailsTableList TableList = new CarDetailsTableList();
                TableList.Date = String.Format("{0:dd/MM/yyyy}", item.Date);
                TableList.ExpenditureNo = item.Exp_no;
                TableList.ExpenditureHead = item.exphead;
                TableList.CostHead = item.CostHead;
                TableList.TotalAmount = Convert.ToDecimal(item.CarHireAmount);
                TableList.Status = item.Staus;
                CarDetailsTableDataList.Add(TableList);
            }

            //foreach (var item in CarDetailsData)
            //{
            //    CarDetailsTableList TableList = new CarDetailsTableList();
            //    TableList.Date = String.Format("{0:dd/MM/yyyy}", item.Date);
            //    TableList.ExpenditureNo = item.Exp_no;
            //    TableList.ExpenditureHead = "Car Fuel";
            //    TableList.CostHead = item.CostHead;
            //    TableList.TotalAmount = Convert.ToDecimal(item.CarFuelAmount);
            //    TableList.Status = item.Staus;
            //    CarDetailsTableDataList.Add(TableList);
            //}

            //foreach (var item in CarDetailsData)
            //{
            //    CarDetailsTableList TableList = new CarDetailsTableList();
            //    TableList.Date = String.Format("{0:dd/MM/yyyy}", item.Date);
            //    TableList.ExpenditureNo = item.Exp_no;
            //    TableList.ExpenditureHead = "Toll Tax";
            //    TableList.CostHead = item.CostHead;
            //    TableList.TotalAmount = Convert.ToDecimal(item.CarTollTax);
            //    TableList.Status = item.Staus;
            //    CarDetailsTableDataList.Add(TableList);
            //}            

            //var Driverfooding = db.TRN_EXPENDETURE_DETAILS.Where(x => x.ISACTIVE == true && x.REF_EXPENDETURE_FOR_ID == "0032").ToList();
            //if (Driverfooding.Count > 0)
            //{
            //    string[] DistinctExpNO = CarDetailsData.Select(x => x.Exp_no).Distinct().ToArray();
            //    foreach (var item in DistinctExpNO)
            //    {
            //        var particularExpno = CarDetailsData.Where(x => x.Exp_no == item).ToList();
            //        CarDetailsTableList TableList = new CarDetailsTableList();
            //        TableList.Date = String.Format("{0:dd/MM/yyyy}", particularExpno[0].Date);
            //        TableList.ExpenditureNo = particularExpno[0].Exp_no;

            //        foreach (var i in Driverfooding)
            //        {
            //            var fooding = Driverfooding.Where(x => x.REF_EXPENDETURE_NO == item && x.REF_EXPENDETURE_FOR_ID == "0032").ToList();
            //            if (fooding.Count > 0)
            //            {
            //                TableList.ExpenditureHead = fooding[0].MSTR_EXPENDETURE_FOR.EXPENDETURE_FOR_NAME;
            //                TableList.CostHead = fooding[0].MSTR_COST_CENTER.COST_CENTER_NAME;
            //                TableList.TotalAmount = Convert.ToDecimal(fooding[0].AMOUNT);
            //            }
            //            else
            //            {
            //                TableList.ExpenditureHead = "Driver Fooding";
            //                TableList.CostHead = particularExpno[0].CostHead;
            //                TableList.TotalAmount = Convert.ToDecimal("0.00");
            //            }

            //        }
            //        TableList.Status = particularExpno[0].Staus;
            //        CarDetailsTableDataList.Add(TableList);
            //    }
            //}

            //var DriverLodging = db.TRN_EXPENDETURE_DETAILS.Where(x => x.ISACTIVE == true && x.REF_EXPENDETURE_FOR_ID == "0033").ToList();
            //if (DriverLodging.Count > 0)
            //{
            //    string[] DistinctExpNO = CarDetailsData.Select(x => x.Exp_no).Distinct().ToArray();
            //    foreach (var item in DistinctExpNO)
            //    {
            //        var particularExpno = CarDetailsData.Where(x => x.Exp_no == item).ToList();
            //        CarDetailsTableList TableList = new CarDetailsTableList();
            //        TableList.Date = String.Format("{0:dd/MM/yyyy}", particularExpno[0].Date);
            //        TableList.ExpenditureNo = particularExpno[0].Exp_no;

            //        foreach (var i in DriverLodging)
            //        {
            //            var lodging = DriverLodging.Where(x => x.REF_EXPENDETURE_NO == item && x.REF_EXPENDETURE_FOR_ID == "0033").ToList();
            //            if (lodging.Count > 0)
            //            {
            //                TableList.ExpenditureHead = lodging[0].MSTR_EXPENDETURE_FOR.EXPENDETURE_FOR_NAME;
            //                TableList.CostHead = lodging[0].MSTR_COST_CENTER.COST_CENTER_NAME;
            //                TableList.TotalAmount = Convert.ToDecimal(lodging[0].AMOUNT);
            //            }
            //            else
            //            {
            //                TableList.ExpenditureHead = "Driver Lodding";
            //                TableList.CostHead = particularExpno[0].CostHead;
            //                TableList.TotalAmount = Convert.ToDecimal("0.00");
            //            }

            //        }
            //        TableList.Status = particularExpno[0].Staus;
            //        CarDetailsTableDataList.Add(TableList);
            //    }
            //}
            return CarDetailsTableDataList;

        }
    }
}
