﻿/*------------------Reset Button JS------------------*/
$(document).ready(function () {
    $('#btn').click(function () {
        $('.clear').val('');
    });
    $('#cash_radio').checked = true
    {
        $('#provisonal_receipt').show();
        $('#bank_name').hide();
        $('#branch_name').hide();
        $('#cheque').hide();
        $('#neft').hide();
        $('#paying_ac_no').hide();
    }
});

/*------------------Radio Button JS------------------*/



function checkradio(x) {

    var cash_radio = document.getElementById("cash_radio");
    var cheque_radio = document.getElementById("cheque_radio");
    var internet_banking_radio = document.getElementById("internet_banking_radio");

    if (x == 0) {
        if(cash_radio.checked) {
            $('#provisonal_receipt').show();
            $('#bank_name').hide();
            $('#branch_name').hide();
            $('#cheque').hide();
            $('#neft').hide();
            $('#paying_ac_no').hide();
        };
        
    }
    


    else if (x == 1) {
        if (cheque_radio.checked) {
            $('#bank_name').show();
            $('#branch_name').show();
            $('#cheque').show();
            $('#neft').hide();
            $('#paying_ac_no').show();
            $('#provisonal_receipt').hide();
        };
    }
    else {
        if (internet_banking_radio.checked) {
            $('#bank_name').show();
            $('#branch_name').show();
            $('#cheque').hide();
            $('#neft').show();
            $('#paying_ac_no').show();
            $('#provisonal_receipt').hide();
        };
    }
};

/*------------------Field Validation JS------------------*/
$(function () {
    $("applicant_name_error_message").hide();
    $("age_error_message").hide();
    $("father_name_error_message").hide();
    $("mother_name_error_message").hide();
    $("pan_card_error_message").hide();
    $("Aadhar_card_error_message").hide();
    $("#mobile_no_error_message").hide();
    $("#email_id_error_message").hide();
    $("#ward_no_error_message").hide();
    $("#word_no_error_message").hide();
    $("#ward_error_message").hide();


    $("#applicant_name").focusout(function () {
        check_applicantname();
    });
    $("#age").focusout(function () {
        check_age();
    });
    $("#father_name").focusout(function () {
        check_fathername();
    });
    $("#mother_name").focusout(function () {
        check_mothername();
    });
    $("#pan_card").focusout(function () {
        check_pancard();
    });
    $("#Aadhar_card").focusout(function () {
        check_aadharcard();
    });
    $("#mobile_no").focusout(function () {
        check_mobileno();
    });
    $("#email_id").focusout(function () {
        check_email();
    });
    $("#word_no").focusout(function () {
        check_ward();
    });
    $("#ward_no").focusout(function () {
        check_word();
    });

    $("#ward").focusout(function () {
        check_ward_no();
    });


    function check_applicantname() {
        var error_applicantname = false;
        var pattern = /^[A-Z\s]{1,}[A-Za-z\s]{0,}$/;
        var applicantname = $("#applicant_name").val();
        if (pattern.test(applicantname) && applicantname !== '') {
            $("#applicant_name_error_message").hide();
            $("#applicant_name").css("border-bottom", "2px solid #34F458");
        } else {
            $("#applicant_name_error_message").html("Enter a valid name");
            $("#applicant_name_error_message").show();
            $("#applicant_name").css("border-bottom", "2px solid #F90A0A");
            error_fname = true;
        }
    }

    function check_age() {
        var error_applicantname = false;
        var pattern = /^[0-9]{2}$/;
        var age = $("#age").val();
        if (pattern.test(age) && age !== '') {
            $("#age_error_message").hide();
            $("#age").css("border-bottom", "2px solid #34F458");
        } else {
            $("#age_error_message").html("Age should be a No");
            $("#age_error_message").show();
            $("#age").css("border-bottom", "2px solid #F90A0A");
            error_fname = true;
        }
    }

    function check_fathername() {
        var error_fathername = false;
        var pattern = /^[A-Z\s]{1,}[A-Za-z\s]{0,}$/;
        var fathername = $("#father_name").val();
        if (pattern.test(fathername) && fathername !=='') {
            $("#father_name_error_message").hide();
            $("#father_name").css("border-bottom", "2px solid #34F458");
        } else {
            $("#father_name_error_message").html("Enter a valid name");
            $("#father_name_error_message").show();
            $("#father_name").css("border-bottom", "2px solid #F90A0A");
            error_fathername = true;
        }
    }

    function check_mothername() {
        var error_mothername = false;
        var pattern = /^[A-Z\s]{1,}[A-Za-z\s]{0,}$/;
        var mothername = $("#mother_name").val();
        if (pattern.test(mothername) && mothername !== '') {
            $("#mother_name_error_message").hide();
            $("#mother_name").css("border-bottom", "2px solid #34F458");
        } else {
            $("#mother_name_error_message").html("Enter a valid name");
            $("#mother_name_error_message").show();
            $("#mother_name").css("border-bottom", "2px solid #F90A0A");
            error_mothername = true;
        }
    }

    function check_pancard() {
        var error_pancard = false;        
        var pancard = $("#pan_card").val().length;
        if (pancard == 10) {            
            $("#pan_card_error_message").hide();
            $("#pan_card").css("border-bottom", "2px solid #34F458");           
        } else {
            $("#pan_card_error_message").html("Pan card should 10 digit");
            $("#pan_card_error_message").show();
            $("#pan_card").css("border-bottom", "2px solid #F90A0A");
            error_pancard = true;
        }
    }

    function check_aadharcard() {
        var error_aadharcard = false;
        var pattern = /^[0-9]{12}$/;
        var aadharcard = $("#Aadhar_card").val();
        if (pattern.test(aadharcard) &&  aadharcard !== '') {
            $("#Aadhar_card_error_message").hide();
            $("#Aadhar_card").css("border-bottom", "2px solid #34F458");
        } else {
            $("#Aadhar_card_error_message").html("Aadhar No should 12 digit");
            $("#Aadhar_card_error_message").show();
            $("#Aadhar_card").css("border-bottom", "2px solid #F90A0A");
            error_aadharcard = true;
        }
    }    
    
    function check_mobileno() {
        var error_fname = false;
        var pattern = /^[6-9]\d{9}$/;
        var mobileno = $("#mobile_no").val();
        if (pattern.test(mobileno) && mobileno !== '') {
            $("#mobile_no_error_message").hide();
            $("#mobile_no").css("border-bottom", "2px solid #34F458");
        } else {
            $("#mobile_no_error_message").html("Should contain a valid mobile no");
            $("#mobile_no_error_message").show();
            $("#mobile_no").css("border-bottom", "2px solid #F90A0A");
            error_fname = true;
        }
    }

    function check_email() {
        var error_fname = false;
        var pattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var email = $("#email_id").val();
        if (pattern.test(email) && email !== '') {
            $("#email_id_error_message").hide();
            $("#email_id").css("border-bottom", "2px solid #34F458");
        } else {
            $("#email_id_error_message").html("Should contain a valid Email");
            $("#email_id_error_message").show();
            $("#email_id").css("border-bottom", "2px solid #F90A0A");
            error_fname = true;
        }
    }

    function check_ward() {
        var error_fname = false;
        var pattern = /^[1-9]$/;
        var ward_no = $("#word_no").val();
        if (pattern.test(ward_no) && ward_no !== '') {
            $("#ward_no_error_message").hide();
            $("#word_no").css("border-bottom", "2px solid #34F458");
        } else {
            $("#ward_no_error_message").html("Should be 1-9");
            $("#ward_no_error_message").show();
            $("#word_no").css("border-bottom", "2px solid #F90A0A");
            error_fname = true;
        }
    }

    function check_word() {
        var error_fname = false;
        var pattern = /^[1-9]$/;
        var ward_no = $("#ward_no").val();
        if (pattern.test(ward_no) && ward_no !== '') {
            $("#word_no_error_message").hide();
            $("#ward_no").css("border-bottom", "2px solid #34F458");
        } else {
            $("#word_no_error_message").html("Should be 1-9");
            $("#word_no_error_message").show();
            $("#ward_no").css("border-bottom", "2px solid #F90A0A");
            error_fname = true;
        }
    }

    function check_ward_no() {
        var error_fname = false;
        var pattern = /^[1-9]$/;
        var ward_no = $("#ward").val();
        if (pattern.test(ward_no) && ward_no !== '') {
            $("#ward_error_message").hide();
            $("#ward").css("border-bottom", "2px solid #34F458");
        } else {
            $("#ward_error_message").html("Should be 1-9");
            $("#ward_error_message").show();
            $("#ward").css("border-bottom", "2px solid #F90A0A");
            error_fname = true;
        }
    }


});



/*$(function () {
    $("#chk").change(function () {
        $("#address2").val("");
        if (this.checked) {
            $("address2").val($("#address1").val())
        }
    });
});*/

function CopyAdd(cb) {
    if (cb.checked) {
        var a1 = document.getElementById('cad1').value;
        var a2 = document.getElementById('cad2').value;
        var a3 = document.getElementById('cstreet').value;
        var a4 = document.getElementById('carea').value;
        var a5 = document.getElementById('c_near_loc').value;
        var a6 = document.getElementById('c_ward').value;
        var a7 = document.getElementById('City').value;
        var a8 = document.getElementById('state').value;
        var a9 = document.getElementById('pin').value;
        var a10 = document.getElementById('c_Country').value;
        var a11 = document.getElementById('c_district').value;


        document.getElementById('pad1').value = a1;
        document.getElementById('pad2').value = a2;
        document.getElementById('pstreet').value = a3;
        document.getElementById('parea').value = a4;
        document.getElementById('p_near_loc').value = a5;
        document.getElementById('p_ward').value = a6;
        document.getElementById('p_city').value = a7;
        document.getElementById('p_state').value = a8;
        document.getElementById('p_pin').value = a9;
        document.getElementById('p_country').value = a10;
        document.getElementById('p_district').value = a11;
    }

    else {
        document.getElementById('pad1').value = "";
        document.getElementById('pad2').value = "";
        document.getElementById('pstreet').value = "";
        document.getElementById('parea').value = "";
        document.getElementById('p_near_loc').value = "";
        document.getElementById('p_ward').value = "";
        document.getElementById('p_city').value = "";
        document.getElementById('p_state').value = "";
        document.getElementById('p_pin').value = "";
        document.getElementById('p_country').value = "";
        document.getElementById('p_district').value = "";


    }

    
}




