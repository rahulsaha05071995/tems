﻿$(function () {
    
    $("#car_no").focusout(function () {
        var pattern = /^[A-Z]{2}\s[0-9]{2}\s[A-Z]{1}\s[0-9]{4}|[A-Z]{2}\s[0-9]{2}\s[A-Z]{2}\s[0-9]{4}$/;        
        var carno = $("#car_no").val();
        if (pattern.test(carno) && carno !== '') {
            $("#car_no_error_message").hide();
            $("#car_no").css("border-bottom", "2px solid #34F458");
        } else {
            $("#car_no_error_message").html("Car No. Invalid");
            $("#car_no_error_message").show();
            $("#car_no").css("border-bottom", "2px solid #F90A0A");
        }
    });
    

    $("#driver_name").focusout(function () {
        var pattern = /^[A-Za-z]{1,}[a-z]+[ ][A-Z]{1}[a-z]+|[A-Za-z]{1,}[a-z]+$/;
        var st = $("#driver_name").val();
        $("#driver_name").val(uppercase(st));
        var drivername = $("#driver_name").val();
        if (pattern.test(drivername) && drivername !== '') {
            $("#driver_name_error_message").hide();
            $("#driver_name").css("border-bottom", "2px solid #34F458");
        } else {
            $("#driver_name_error_message").html("Enter a Name");
            $("#driver_name_error_message").show();
            $("#driver_name").css("border-bottom", "2px solid #F90A0A");
        }
    });
    function uppercase(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        return splitStr.join(' ');
    };

    $("#car_rental_charge").focusout(function () {        
        var carrent = $("#car_rental_charge").val();
        if ( carrent !== '') {
            $("#car_rental_error_message").hide();
            $("#car_rental_charge").css("border-bottom", "2px solid #34F458");
        } else {
            $("#car_rental_error_message").html("Enter Car Rent");
            $("#car_rental_error_message").show();
            $("#car_rental_charge").css("border-bottom", "2px solid #F90A0A");
        }
    });

    

    $("#fuel_rate").focusout(function () {
        var a = 10;
        console.log(a);
        var pattern = /^[4-9]{1}[0-9]{1}[.]{1}[0-9]{2}$/;
        var fuelrate = $("#fuel_rate").val();
        if (pattern.test(fuelrate) && fuelrate !== '') {
            $("#fuel_rate_error_message").hide();
            $("#fuel_rate").css("border-bottom", "2px solid #34F458");
        } else {
            $("#fuel_rate_error_message").html("Enter Valid Fuel rate");
            $("#fuel_rate_error_message").show();
            $("#fuel_rate").css("border-bottom", "2px solid #F90A0A");
        }
    });    
});

