﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TEMS_BLL;
using TEMS_BOL;
using TEMS_DAL;
using static TEMS_BOL.PayAmountBOL;

namespace TEMS.Controllers
{
    public class PaymentController : Controller
    {
        // GET: Payment
        ExpendetureBLL expendBLL = new ExpendetureBLL();
        PayAmountBLL paymentBLL = new PayAmountBLL();

        public ActionResult SearchPayment()
        {
            if (Session["USERNAME"] != null)
            {
                if (Session["ROLLID"].ToString() != "0002")
                {
                    PayAmountBOL payment = new PayAmountBOL();
                    List<SelectListItem> PaymentToList = new List<SelectListItem>();
                    List<SelectListItem> ExpIDList = new List<SelectListItem>();
                    List<SelectListItem> AgencyNameList = new List<SelectListItem>();

                    PaymentToList = expendBLL.ListExpenditureBy();
                    ExpIDList = paymentBLL.ListExpID();
                    AgencyNameList = expendBLL.ListAgency();

                    payment.PaymentToList = PaymentToList;
                    payment.ExpIDList = ExpIDList;
                    payment.AgencyNameList = AgencyNameList;

                    return View(payment);
                }
                else
                {
                    return RedirectToAction("login", "Login");
                }

            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SearchPayment(PayAmountBOL payment)
        {
            if (Session["USERNAME"] != null)
            {
                if (Session["ROLLID"].ToString() != "0002")
                {                    
                    List<SelectListItem> PaymentToList = new List<SelectListItem>();
                    List<SelectListItem> ExpIDList = new List<SelectListItem>();
                    List<SelectListItem> AgencyNameList = new List<SelectListItem>();
                    List<SearchPayment> TableDataList = new List<SearchPayment>();

                    PaymentToList = expendBLL.ListExpenditureBy();
                    ExpIDList = paymentBLL.ListExpID();
                    AgencyNameList = expendBLL.ListAgency();

                    TableDataList = paymentBLL.SearchPaymentBLL(payment);
                    

                    payment.PaymentToList = PaymentToList;
                    payment.ExpIDList = ExpIDList;
                    payment.AgencyNameList = AgencyNameList;
                    payment.TableDataList = TableDataList;
                    return View(payment);
                }
                else
                {
                    return RedirectToAction("login", "Login");
                }

            }
            else
            {
                return RedirectToAction("login", "Login");
            }            
        }

        public ActionResult PayAmount()
        {
            if (Session["USERNAME"] != null)
            {
                if(Session["ROLLID"].ToString() != "0002")
                {
                    PayAmountBOL payment = new PayAmountBOL();
                    List<SelectListItem> PaymentToList = new List<SelectListItem>();
                    List<SelectListItem> ExpIDList = new List<SelectListItem>();
                    List<SelectListItem> AgencyNameList = new List<SelectListItem>();

                    PaymentToList = expendBLL.ListExpenditureBy();
                    ExpIDList = paymentBLL.ListExpID();
                    AgencyNameList = expendBLL.ListAgency();

                    payment.PaymentToList = PaymentToList;
                    payment.ExpIDList = ExpIDList;
                    payment.AgencyNameList = AgencyNameList;                    
                    return View(payment);
                }
                else
                {
                    return RedirectToAction("login", "Login");
                }
                
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
                
        }

        public JsonResult GetExpendituredata(string Expenditure_No,string Payment_To,string Payment_For,string Agency)
        {
            TEMSEntities db = new TEMSEntities();
            
            db.Configuration.ProxyCreationEnabled = false;
            if(Payment_For == "Employee")
            {
                var Data = db.TRN_EXPENDITURE_LOG.Where(x => x.EXPENDITURE_NO == Expenditure_No && x.IsActive == true && x.REF_USER_ID == Payment_To).
                Select(x => x.REMAINING_AMOUNT);
                return Json(Data, JsonRequestBehavior.AllowGet);

            }
            else
            {
                var Data = db.TRN_EXPENDITURE_LOG.Where(x => x.EXPENDITURE_NO == Expenditure_No && x.IsActive == true && x.REF_AGENCY_ID == Agency).
                Select(x => x.REMAINING_AMOUNT);
                return Json(Data, JsonRequestBehavior.AllowGet);
            }

            //return Json(Data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAdvancedata(string Payment_To)
        {
            TEMSEntities db = new TEMSEntities();
            db.Configuration.ProxyCreationEnabled = false;
            var Data = db.TRN_Advance.Where(x => x.IsActive == true && x.RefUserId == Payment_To).ToList();
            
            var money = Data.Sum(x => Convert.ToDecimal(x.RemainingAmount));
            return Json(money, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAdvanceexpnodata(string Payment_To)
        {
            TEMSEntities db = new TEMSEntities();
            db.Configuration.ProxyCreationEnabled = false;            
            List<TRN_EXPENDITURE_LOG> expno = db.TRN_EXPENDITURE_LOG.Where(x => x.IsActive == true && x.REF_USER_ID == Payment_To && x.PAYMENT_STATUS != "Paid").OrderBy(x => x.EXPENDITURE_NO).ToList();            
            return Json(expno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAdvanceagencydata(string Payment_Agency)
        {
            TEMSEntities db = new TEMSEntities();
            db.Configuration.ProxyCreationEnabled = false;
            var Data = db.TRN_Advance.Where(x => x.IsActive == true && x.RefAgencyId == Payment_Agency).ToList();
            var agencymoney = Data.Sum(x => Convert.ToDecimal(x.RemainingAmount));
            return Json(agencymoney, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAdvanceagencyexpnodata(string Payment_Agency)
        {
            TEMSEntities db = new TEMSEntities();
            db.Configuration.ProxyCreationEnabled = false;
            List<TRN_EXPENDITURE_LOG> expno = db.TRN_EXPENDITURE_LOG.Where(x => x.IsActive == true && x.REF_AGENCY_ID == Payment_Agency && x.PAYMENT_STATUS != "Paid").OrderBy(x => x.EXPENDITURE_NO).ToList();
            return Json(expno, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PayAmount(PayAmountBOL payment)
        {
            if (Session["USERNAME"] != null)
            {
                if (Session["ROLLID"].ToString() != "0002")
                {
                    List<SelectListItem> PaymentToList = new List<SelectListItem>();
                    List<SelectListItem> ExpIDList = new List<SelectListItem>();                    
                    string USERNAME = Session["USERID"].ToString();
                    bool PaymentData = paymentBLL.FetchPaymentData(payment, USERNAME);
                    if(PaymentData == true)
                    {
                        if(payment.PaymentType == "Advance")
                        {
                            TempData["Message"] = "Advance Added Succesfully";
                        }
                        else
                        {
                            TempData["Message"] = "Expenditure Paid";
                        }
                        
                        return RedirectToAction("PayAmount");
                    }
                    else
                    {
                        if (payment.PaymentType == "Advance")
                        {
                            TempData["Message"] = "Advance Not Given Succesfully";
                        }
                        else
                        {
                            TempData["Message"] = "Expenditure Not Paid";
                        }
                        
                    }
                    PaymentToList = expendBLL.ListExpenditureBy();
                    ExpIDList = paymentBLL.ListExpID();

                    payment.PaymentToList = PaymentToList;
                    payment.ExpIDList = ExpIDList;
                    return View(payment);
                }
                else
                {
                    return RedirectToAction("login", "Login");
                }

            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }
        
    }
}