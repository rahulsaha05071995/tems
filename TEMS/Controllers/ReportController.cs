﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TEMS_BLL;
using TEMS_BOL;

namespace TEMS.Controllers
{
    public class ReportController : Controller
    {
        LedgerBLL LedgerBl = new LedgerBLL();
        ReportBLL ReportBL = new ReportBLL();

        [HttpGet]
        public ActionResult Ledger()
        {
            if (Session["USERNAME"] != null)
            {
                LedgerBOL Ledger = new LedgerBOL();
                Ledger.AgencyNameList = ReportBL.GetAgencyNameList();
                Ledger.EmployeesNameList = ReportBL.GetEmployeesNameList();
                Ledger.LedgerFor = "Employee";

                return View(Ledger);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        [HttpPost]
        public ActionResult Ledger(LedgerBOL Ledger)
        {
            if (Session["USERNAME"] != null)
            {
                Ledger.AgencyNameList = ReportBL.GetAgencyNameList();
                Ledger.EmployeesNameList = ReportBL.GetEmployeesNameList();
                if (Ledger.LedgerFor == "Employee" && Ledger.EmployeeName != null && Ledger.EmployeeName != "")
                {
                    Ledger.LedgerSummaryReportList = LedgerBl.GetLedgerSummaryReportList(Ledger);
                }
                else if (Ledger.LedgerFor == "Agency" && Ledger.AgencyName != null && Ledger.AgencyName != "")
                {
                    if (Ledger.ReportType == "Summary")
                    {
                        Ledger.LedgerSummaryReportList = LedgerBl.GetLedgerSummaryReportList(Ledger);
                    }
                    if (Ledger.ReportType == "Details")
                    {
                        Ledger.LedgerDetailsReportList = LedgerBl.GetLedgerDetailsReportList(Ledger);
                    }
                }

                return View(Ledger);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        [HttpGet]
        public ActionResult Transaction()
        {
            if (Session["USERNAME"] != null)
            {
                TransactionReportBOL TransactionBOL = new TransactionReportBOL();
                TransactionBOL.AgencyNameList = ReportBL.GetAgencyNameList();
                TransactionBOL.EmployeesNameList = ReportBL.GetEmployeesNameList();
                TransactionBOL.ReportFor = "Employee";
                return View(TransactionBOL);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        [HttpPost]
        public ActionResult Transaction(TransactionReportBOL TransactionBOL)
        {
            if (Session["USERNAME"] != null)
            {
                TransactionBOL.AgencyNameList = ReportBL.GetAgencyNameList();
                TransactionBOL.EmployeesNameList = ReportBL.GetEmployeesNameList();
                TransactionBOL.TransactionReportList = ReportBL.GetTransactionReportList(TransactionBOL);
                return View(TransactionBOL);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }


        [HttpGet]
        public ActionResult History(string Ids, string FromDate, string ToDate, string Type)
        {
            if (Session["USERNAME"] != null)
            {

                ReportListBOL ReportBOL = new ReportListBOL();
                if (Ids != null && Ids != "")
                {
                    ReportBOL.ListReportHistory = ReportBL.GetTransactionReportHistoryData(Ids, FromDate, ToDate, Type);
                }
                else
                {
                    TempData["Message"] = "Invalid IDs!";
                    return RedirectToAction("Transaction");
                }
                return View(ReportBOL);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        [HttpGet]
        public ActionResult Register()
        {
            if (Session["USERNAME"] != null)
            {
                RegisterReportBOL RegisterBOL = new RegisterReportBOL();
                RegisterBOL.EmployeeAgencyNameList = ReportBL.GetEmployeeAgencyNameList();
                RegisterBOL.AgencyNameList = ReportBL.GetAgencyNameList();
                RegisterBOL.EmployeesNameList = ReportBL.GetEmployeesNameList();
                return View(RegisterBOL);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        [HttpPost]
        public ActionResult Register(RegisterReportBOL RegisterBOL)
        {
            if (Session["USERNAME"] != null)
            {
                RegisterBOL.EmployeeAgencyNameList = ReportBL.GetEmployeeAgencyNameList();
                RegisterBOL.AgencyNameList = ReportBL.GetAgencyNameList();
                RegisterBOL.EmployeesNameList = ReportBL.GetEmployeesNameList();
                RegisterBOL.RegisterReportList = ReportBL.GetRegisterReportList(RegisterBOL);
                return View(RegisterBOL);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        [HttpGet]
        public ActionResult Expendeture()
        {
            if (Session["USERNAME"] != null)
            {
                ExpendetureReportBOL ExpendetureBOL = new ExpendetureReportBOL();
                ExpendetureBOL.AgencyNameList = ReportBL.GetAgencyNameList();
                ExpendetureBOL.EmployeesNameList = ReportBL.GetEmployeesNameList();
                ExpendetureBOL.EmployeeAgencyNameList = ReportBL.GetEmployeeAgencyNameList();
                ExpendetureBOL.ReportType = "Summary";
                return View(ExpendetureBOL);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        [HttpPost]
        public ActionResult Expendeture(ExpendetureReportBOL ExpendetureBOL)
        {
            if (Session["USERNAME"] != null)
            {
                ExpendetureBOL.AgencyNameList = ReportBL.GetAgencyNameList();
                ExpendetureBOL.EmployeesNameList = ReportBL.GetEmployeesNameList();
                ExpendetureBOL.EmployeeAgencyNameList = ReportBL.GetEmployeeAgencyNameList();
                if (ExpendetureBOL.ReportType == "Summary")
                {
                    ExpendetureBOL.ExpendetureSummaryReportList = ReportBL.GetExpendetureSummaryReportList(ExpendetureBOL);
                }
                if (ExpendetureBOL.ReportType == "Details")
                {
                    ExpendetureBOL.ExpendetureDetailsReportList = ReportBL.GetExpendetureDetailsReportList(ExpendetureBOL);
                }
                return View(ExpendetureBOL);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }
    }
}