﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TEMS_BLL;
using TEMS_BOL;
using TEMS_DAL;
using static TEMS_BOL.SearchExpenditureBOL;

namespace TEMS.Controllers
{
    public class ExpendetureController : Controller
    {
        TEMSEntities db = new TEMSEntities();
        ExpendetureBLL expendBLL = new ExpendetureBLL();
        public ActionResult search()
        {
            if(Session["USERNAME"] != null)
            {
                string USERNAME = Session["USERNAME"].ToString();
                string ROLLID = Session["ROLLID"].ToString();
                SearchExpenditureBOL search = new SearchExpenditureBOL();
                List<SelectListItem> ExpenditureByList = new List<SelectListItem>();
                List<SelectListItem> PaymentForList = new List<SelectListItem>();
                List<SelectListItem> CostCenterList = new List<SelectListItem>();
                List<SelectListItem> CarNOList = new List<SelectListItem>();
                List<SelectListItem> StatusList = new List<SelectListItem>();

                List<SummaryTableList> SummaryTableDataList = new List<SummaryTableList>();

                
                PaymentForList = expendBLL.ListPayment();
                CostCenterList = expendBLL.ListCostCenter();
                CarNOList = expendBLL.ListCarNo();
                StatusList = expendBLL.ListStatus();
                if(ROLLID == "0002")
                {
                    ExpenditureByList = expendBLL.ListExpenditureBy(USERNAME);
                    SummaryTableDataList = expendBLL.FetchSummaryTableData(USERNAME);                    
                }
                else
                {
                    ExpenditureByList = expendBLL.ListExpenditureBy();
                    SummaryTableDataList = expendBLL.FetchSummaryTableData();                    
                }
                

                search.ExpenditureByList = ExpenditureByList;
                search.PaymentForList = PaymentForList;
                search.CostCenterList = CostCenterList;
                search.CarNOList = CarNOList;
                search.StatusList = StatusList;
                search.SummaryTableDataList = SummaryTableDataList;
                return View(search);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
            
        }

        [HttpPost]
        public ActionResult search(SearchExpenditureBOL searchdetails)
        {
            if (Session["USERNAME"] != null)
            {
                string USERNAME = Session["USERNAME"].ToString();
                string ROLLID = Session["ROLLID"].ToString();

                List<SelectListItem> ExpenditureByList = new List<SelectListItem>();
                List<SelectListItem> PaymentForList = new List<SelectListItem>();
                List<SelectListItem> CostCenterList = new List<SelectListItem>();
                List<SelectListItem> CarNOList = new List<SelectListItem>();
                List<SelectListItem> StatusList = new List<SelectListItem>();
                List<SearchTableList> TableDataList = new List<SearchTableList>();
                List<SummaryTableList> SummaryTableDataList = new List<SummaryTableList>();
                List<CarSummaryTableList> CarSummaryTableDataList = new List<CarSummaryTableList>();
                List<CarDetailsTableList> CarDetailsTableDataList = new List<CarDetailsTableList>();

                if (ROLLID == "0002")
                {
                    ExpenditureByList = expendBLL.ListExpenditureBy(USERNAME);
                }
                else
                {
                    ExpenditureByList = expendBLL.ListExpenditureBy();
                }
                    
                PaymentForList = expendBLL.ListPayment();
                CostCenterList = expendBLL.ListCostCenter();
                CarNOList = expendBLL.ListCarNo();
                StatusList = expendBLL.ListStatus();
                TableDataList = expendBLL.FetchTableData(searchdetails);
                SummaryTableDataList = expendBLL.FetchSummaryTableData(searchdetails);
                CarSummaryTableDataList = expendBLL.FetchCarSummaryData(searchdetails);
                CarDetailsTableDataList = expendBLL.FetchCarDetailsData(searchdetails);

                searchdetails.ExpenditureByList = ExpenditureByList;
                searchdetails.PaymentForList = PaymentForList;
                searchdetails.CostCenterList = CostCenterList;
                searchdetails.CarNOList = CarNOList;
                searchdetails.StatusList = StatusList;
                searchdetails.TableDataList = TableDataList;
                searchdetails.SummaryTableDataList = SummaryTableDataList;
                searchdetails.CarSummaryTableDataList = CarSummaryTableDataList;
                searchdetails.CarDetailsTableDataList = CarDetailsTableDataList;
                return View(searchdetails);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        public JsonResult GetExpendituredata(string Expenditure_No)
        {                     
            db.Configuration.ProxyCreationEnabled = false;
            List<TRN_EXPENDETURE_SUMMARY> summary = db.TRN_EXPENDETURE_SUMMARY.Where(x => x.EXPENDETURE_NO == Expenditure_No && x.CAR_HIRE == true && x.ISACTIVE == true).ToList();
            if(summary.Count>0)
            {
                var Datalist = db.TRN_EXPENDETURE_SUMMARY
                .Join(db.MSTR_APPROVE_STATUS,
                c => c.REF_APPROVE_STATUS_ID,
                o => o.APPROVE_STATUS_ID,
                (c, o) => new { c, o })
                .Join(db.TRN_CAR_DETAILS,
                m => m.c.EXPENDETURE_NO,
                n => n.REF_EXPENDETURE_NO,
                (m, n) => new { m, n }).
                Where(x => x.m.c.ISACTIVE == true && x.m.c.EXPENDETURE_NO == Expenditure_No).
                Select(x => new
                {
                    Expbyname = x.m.c.MSTR_USER.FULL_NAME,
                    StatusId = x.m.c.REF_APPROVE_STATUS_ID,
                    Statusname = x.m.c.MSTR_APPROVE_STATUS.APPROVE_STATUS_NAME,
                    Name = x.m.c.MSTR_USER.FULL_NAME,
                    CarNo = x.n.CAR_NO
                }).ToList();
                return Json(Datalist, JsonRequestBehavior.AllowGet);
            }

            else
            {
                var Datalist = db.TRN_EXPENDETURE_SUMMARY.Where(x => x.ISACTIVE == true && x.EXPENDETURE_NO == Expenditure_No).
                Select(x => new
                {
                    StatusId = x.REF_APPROVE_STATUS_ID,
                    Statusname = x.MSTR_APPROVE_STATUS.APPROVE_STATUS_NAME,
                    Expbyname = x.MSTR_USER.FULL_NAME                    
                }).ToList();

                return Json(Datalist, JsonRequestBehavior.AllowGet);
            }            
        }


        public ActionResult expend()
        {
            if (Session["USERNAME"] != null)
            {
                ExpendetureBOL emp = new ExpendetureBOL();
                string UserName = Session["USERNAME"].ToString();
                string UserID = Session["USERID"].ToString();

                List<SelectListItem> PaymentForList = new List<SelectListItem>();
                List<SelectListItem> CostCenterList = new List<SelectListItem>();
                List<SelectListItem> StateList = new List<SelectListItem>();
                List<SelectListItem> ProjectList = new List<SelectListItem>();
                List<SelectListItem> AgencyList = new List<SelectListItem>();

                var duplicateexpID = expendBLL.checkduplicate(emp, UserName, UserID);
                PaymentForList = expendBLL.ListPayment();
                CostCenterList = expendBLL.ListCostCenter();
                StateList = expendBLL.ListSate();
                ProjectList = expendBLL.ListProject();
                AgencyList = expendBLL.ListAgency();

                emp.PaymentForList = PaymentForList;
                emp.CostCenterList = CostCenterList;
                emp.StateList = StateList;
                emp.ProjectList = ProjectList;
                emp.AgencyList = AgencyList;
                return View(emp);
            }            
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        public JsonResult GetCityList(string STATE_ID)
        {
            if (Session["USERNAME"] != null)
            {
                List<MSTR_CITY> citylist = expendBLL.FetchCity(STATE_ID);
                return Json(citylist, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null,JsonRequestBehavior.AllowGet);
            }
            
        }

        public JsonResult GetAllowance(string Expenditure_ID)
        {
            string UserName = Session["USERNAME"].ToString();
            List<TRN_ALLOWANCE> allowance = expendBLL.FetchAllowance(Expenditure_ID, UserName);
            return Json(allowance, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCosthead(string PROJECT_ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var ProjectDatalist = db.MSTR_PROJECT_COSTHEAD_MAP
                .Join(db.MSTR_COST_CENTER,
                c => c.REF_COST_CENTER_ID,
                o => o.COST_CENTER_ID,
                (c, o) => new { c, o }).
                Where(x => x.c.ISACTIVE == true && x.c.REF_PROJECT_ID == PROJECT_ID).
                Select(x => new
                {
                    CostcenterName = x.o.COST_CENTER_NAME,
                    CostcenterID = x.o.COST_CENTER_ID
                });
            return Json(ProjectDatalist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetJourneyDate(string JourneyDate)
        {
            db.Configuration.ProxyCreationEnabled = false;
            string UserName = Session["USERID"].ToString();
            DateTime ConvertJourneyDate = Convert.ToDateTime(JourneyDate);

            var Date = db.TRN_EXPENDETURE_SUMMARY.Where(x => x.CREATED_BY == UserName && x.JOURNEY_DATE == ConvertJourneyDate).ToList();
            var ReturnResult = false;
            if(Date.Count > 0)
            {
                ReturnResult = true;
            }
            else
            {
                ReturnResult = false;
            }

            return Json(ReturnResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost] 
        [ValidateAntiForgeryToken]
        public ActionResult expend(ExpendetureBOL emp)
        {
            List<SelectListItem> PaymentForList = new List<SelectListItem>();
            List<SelectListItem> CostCenterList = new List<SelectListItem>();
            List<SelectListItem> StateList = new List<SelectListItem>();
            List<SelectListItem> ProjectList = new List<SelectListItem>();
            List<SelectListItem> AgencyList = new List<SelectListItem>();
            try
            {
                if (Session["USERNAME"] != null)
                {
                    string UserName = Session["USERID"].ToString();
                    

                    if (ModelState.IsValid)
                    {
                        foreach (var item in emp.ExpendetureDetailsList)
                        {
                            if (item.expendeture_attachment != null)
                            {
                                string filename = Path.GetFileNameWithoutExtension(item.expendeture_attachment.FileName);
                                string extension = Path.GetExtension(item.expendeture_attachment.FileName);
                                filename = emp.expendeture_no + "_" + item.ExpendetureFor + "_" + filename + "_" + DateTime.Now.ToString("ddmmssfff") + extension;
                                string path = Server.MapPath("~/Receipt/");
                                string fullpath = Path.Combine(path, filename);
                                item.expendeture_attachment.SaveAs(fullpath);
                                item.expendeture_attachmentpATH = filename;
                            }
                            else
                            {

                            }

                        }

                        if (emp.car_hire == true)
                        {
                            if (emp.car_rent_recipt != null)
                            {
                                string car_rent_recipt_filename = Path.GetFileNameWithoutExtension(emp.car_rent_recipt.FileName);
                                string car_rent_recipt_extension = Path.GetExtension(emp.car_rent_recipt.FileName);
                                car_rent_recipt_filename = emp.expendeture_no + "_" + "Car Rent" + "_" + car_rent_recipt_filename + "_" + DateTime.Now.ToString("ddmmssfff") + car_rent_recipt_extension;
                                string car_rent_recipt_path = Server.MapPath("~/Receipt/");
                                string car_rent_recipt_fullpath = Path.Combine(car_rent_recipt_path, car_rent_recipt_filename);
                                emp.car_rent_recipt.SaveAs(car_rent_recipt_fullpath);
                                emp.car_rent_reciptPath = car_rent_recipt_filename;
                            }

                            if (emp.toll_tax_recipt != null)
                            {
                                string toll_tax_recipt_filename = Path.GetFileNameWithoutExtension(emp.toll_tax_recipt.FileName);
                                string toll_tax_recipt_extension = Path.GetExtension(emp.toll_tax_recipt.FileName);
                                toll_tax_recipt_filename = emp.expendeture_no + "_" + "Toll Tax" + "_" + toll_tax_recipt_filename + "_" + DateTime.Now.ToString("ddmmssfff") + toll_tax_recipt_extension;
                                string toll_tax_recipt_path = Server.MapPath("~/Receipt/");
                                string toll_tax_recipt_fullpath = Path.Combine(toll_tax_recipt_path, toll_tax_recipt_filename);
                                emp.car_rent_recipt.SaveAs(toll_tax_recipt_fullpath);
                                emp.toll_tax_reciptPath = toll_tax_recipt_filename;
                            }

                            if (emp.fuel_cost_recipt != null)
                            {
                                string fuel_cost_recipt_filename = Path.GetFileNameWithoutExtension(emp.fuel_cost_recipt.FileName);
                                string fuel_cost_recipt_extension = Path.GetExtension(emp.fuel_cost_recipt.FileName);
                                fuel_cost_recipt_filename = emp.expendeture_no + "_" + "Fuel Cost" + "_" + fuel_cost_recipt_filename + "_" + DateTime.Now.ToString("ddmmssfff") + fuel_cost_recipt_extension;
                                string fuel_cost_recipt_path = Server.MapPath("~/Receipt/");
                                string fuel_cost_recipt_fullpath = Path.Combine(fuel_cost_recipt_path, fuel_cost_recipt_filename);
                                emp.car_rent_recipt.SaveAs(fuel_cost_recipt_fullpath);
                                emp.fuel_cost_reciptPath = fuel_cost_recipt_filename;
                            }

                        }

                        List<SelectListItem> savedata = new List<SelectListItem>();
                        savedata = expendBLL.getdata(emp, UserName);
                        TempData["Message"] = "Expenditure Save Successfull";
                        return RedirectToAction("expend");
                    }
                    else
                    {

                    }

                    
                }
                else
                {
                    return RedirectToAction("login", "Login");
                }
        }
            catch(Exception ex)
            {
                TempData["Message"] = "Expenditure Not Submitted";
                
            }

    PaymentForList = expendBLL.ListPayment();
            CostCenterList = expendBLL.ListCostCenter();
            StateList = expendBLL.ListSate();
            ProjectList = expendBLL.ListProject();
            AgencyList = expendBLL.ListAgency();

            emp.PaymentForList = PaymentForList;
            emp.CostCenterList = CostCenterList;
            emp.StateList = StateList;
            emp.ProjectList = ProjectList;
            emp.AgencyList = AgencyList;
            return View(emp);
        }
    }
}