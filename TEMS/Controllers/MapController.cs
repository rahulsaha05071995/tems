﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TEMS_BLL;
using TEMS_BOL;

namespace TEMS.Controllers
{
    public class MapController : Controller
    {        
        ExpendetureBLL expendBLL = new ExpendetureBLL();
        // GET: Map
        [HttpGet]
        public ActionResult CostCenterProjectList()
        {
            if (Session["USERNAME"] != null)
            {
                CostCenterProjectBOL costproject = new CostCenterProjectBOL();
                List<SelectListItem> CostCenterList = new List<SelectListItem>();
                List<SelectListItem> ProjectList = new List<SelectListItem>();

                CostCenterList = expendBLL.ListCostCenter();
                ProjectList = expendBLL.ListProject();

                costproject.CostCenterList = CostCenterList;
                costproject.ProjectList = ProjectList;

                return View(costproject);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpPost]
        public ActionResult CostCenterProjectList(CostCenterProjectBOL costproject)
        {
            if (Session["USERNAME"] != null)
            {
                CostCenterProjectBLL costprojectbll = new CostCenterProjectBLL();
                List<SelectListItem> CostCenterList = new List<SelectListItem>();
                List<SelectListItem> ProjectList = new List<SelectListItem>();

                CostCenterList = expendBLL.ListCostCenter();
                ProjectList = expendBLL.ListProject();

                costproject.CostCenterList = CostCenterList;
                costproject.ProjectList = ProjectList;
                costproject.CostCenterProjectListData = costprojectbll.CostProjectData(costproject);
                return View(costproject);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpGet]
        public ActionResult AddCostCenterProjectMap()
        {
            if (Session["USERNAME"] != null)
            {
                CostCenterProjectBOL costproject = new CostCenterProjectBOL();
                List<SelectListItem> CostCenterList = new List<SelectListItem>();
                List<SelectListItem> ProjectList = new List<SelectListItem>();

                CostCenterList = expendBLL.ListCostCenter();
                ProjectList = expendBLL.ListProject();

                costproject.CostCenterList = CostCenterList;
                costproject.ProjectList = ProjectList;

                return View(costproject);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddCostCenterProjectMap(CostCenterProjectBOL costproject)
        {
            if (Session["USERNAME"] != null)
            {
                CostCenterProjectBLL costprojectbll = new CostCenterProjectBLL();
                List<SelectListItem> CostCenterList = new List<SelectListItem>();
                List<SelectListItem> ProjectList = new List<SelectListItem>();
                CostCenterList = expendBLL.ListCostCenter();
                ProjectList = expendBLL.ListProject();

                string USERID = Session["USERID"].ToString();

                bool CheckAddCostCenterProjectMap = costprojectbll.AddCostCenterProjectMap(costproject, USERID);
                if (CheckAddCostCenterProjectMap == true)
                {
                    TempData["Message"] = "Cost Center Project Map Details Added Successfully !";
                    return RedirectToAction("AddCostCenterProjectMap");
                }
                else
                {
                    TempData["Message"] = "Fail To Add Cost Center Project Map Details !";
                }

                costproject.CostCenterList = CostCenterList;
                costproject.ProjectList = ProjectList;
                return View(costproject);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpGet]
        public ActionResult EditCostCenterProject(string CostProjectMapId)
        {
            if (Session["USERNAME"] != null)
            {
                CostCenterProjectBLL costprojectbll = new CostCenterProjectBLL();
                CostCenterProjectBOL costproject = new CostCenterProjectBOL();
                List<SelectListItem> CostCenterList = new List<SelectListItem>();
                List<SelectListItem> ProjectList = new List<SelectListItem>();
                if (CostProjectMapId != null && CostProjectMapId != "")
                {
                    CostCenterList = expendBLL.ListCostCenter();
                    ProjectList = expendBLL.ListProject();

                    costproject = costprojectbll.EditCostCenterProject(CostProjectMapId);
                }
                else
                {
                    TempData["Message"] = "Invalid Map ID!";
                    return RedirectToAction("CostCenterProjectList");
                }
                costproject.CostCenterList = CostCenterList;
                costproject.ProjectList = ProjectList;
                return View(costproject);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCostCenterProject(CostCenterProjectBOL costproject)
        {
            if (Session["USERNAME"] != null)
            {
                List<SelectListItem> CostCenterList = new List<SelectListItem>();
                List<SelectListItem> ProjectList = new List<SelectListItem>();

                if (ModelState.IsValid)
                {
                    CostCenterProjectBLL costprojectbll = new CostCenterProjectBLL();
                    

                    string USERID = Session["USERID"].ToString();
                    bool EditCostCenterProjectdata = costprojectbll.EditCostCenterProjectData(costproject, USERID);
                    if (EditCostCenterProjectdata == true)
                    {
                        TempData["Message"] = "Map Details Updated Successfully !";
                        return RedirectToAction("AgencyList");
                    }
                    else
                    {
                        TempData["Message"] = "Fail To Update Map Details !";
                        return RedirectToAction("EditCostCenterProject");
                    }
                    
                }
                else
                {
                    TempData["Message"] = "Please Enter Valid Data!";
                }
                costproject.CostCenterList = CostCenterList;
                costproject.ProjectList = ProjectList;
                return View(costproject);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }
    }
}