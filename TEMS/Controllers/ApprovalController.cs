﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TEMS_BLL;
using TEMS_BOL;
using TEMS_DAL;
using static TEMS_BOL.ApprovalBOL;

namespace TEMS.Controllers
{
    public class ApprovalController : Controller
    {

        ExpendetureBLL expendBLL = new ExpendetureBLL();
        ApprovalBLL approvebll = new ApprovalBLL();

        public ActionResult Approve(string ID)
        {
            try
            {                
                if (Session["USERNAME"] != null)
                {                    
                        ApprovalBOL approve = new ApprovalBOL();

                        List<SelectListItem> PaymentForList = new List<SelectListItem>();
                        List<SelectListItem> CostCenterList = new List<SelectListItem>();
                        List<SelectListItem> StateList = new List<SelectListItem>();
                        List<SelectListItem> CityList = new List<SelectListItem>();
                        List<SelectListItem> ProjectList = new List<SelectListItem>();
                        List<SelectListItem> AgencyList = new List<SelectListItem>();


                        PaymentForList = expendBLL.ListPayment();
                        CostCenterList = expendBLL.ListCostCenter();
                        StateList = expendBLL.ListSate();
                        CityList = approvebll.ListCity();
                        ProjectList = expendBLL.ListProject();
                        AgencyList = expendBLL.ListAgency();
                        approve = approvebll.FetchApproveList(ID);


                        approve.PaymentForList = PaymentForList;
                        approve.CostCenterList = CostCenterList;
                        approve.StateList = StateList;
                        approve.CityList = CityList;
                        approve.AgencyList = AgencyList;
                        approve.ProjectList = ProjectList;


                        return View(approve);                    
                }
                else
                {
                    return RedirectToAction("search", "Expendeture");
                }
            }            


            catch(Exception Ex)
            {
                TempData["Message"] = Ex.Message;
                return RedirectToAction("Expendeture", "Login");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Approve(ApprovalBOL approve)
        {            
            if (Session["USERNAME"] != null)
            {
                if (Session["ROLLID"].ToString() == "0001" || Session["ROLLID"].ToString() == "0003")
                {
                    string USERNAME = Session["USERID"].ToString();

                    List<SelectListItem> PaymentForList = new List<SelectListItem>();
                    List<SelectListItem> CostCenterList = new List<SelectListItem>();
                    List<SelectListItem> StateList = new List<SelectListItem>();
                    List<SelectListItem> CityList = new List<SelectListItem>();
                    List<SelectListItem> ProjectList = new List<SelectListItem>();
                    if(ModelState.IsValid)
                    {
                        List<SelectListItem> Savedata = new List<SelectListItem>();

                        Savedata = approvebll.getdata(approve, USERNAME);
                        return RedirectToAction("search", "Expendeture");
                    }
                    PaymentForList = expendBLL.ListPayment();
                    CostCenterList = expendBLL.ListCostCenter();
                    StateList = expendBLL.ListSate();
                    CityList = approvebll.ListCity();
                    ProjectList = expendBLL.ListProject();

                    approve.PaymentForList = PaymentForList;
                    approve.CostCenterList = CostCenterList;
                    approve.StateList = StateList;
                    approve.CityList = CityList;
                    approve.ProjectList = ProjectList;

                }
                else
                {
                    return RedirectToAction("search", "Expendeture");
                }
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
            return View(approve);
        }

        
        public ActionResult Decline(string ID)
        {
            if(Session["USERNAME"] != null)
            {
                if (Session["ROLLID"].ToString() == "0001" || Session["ROLLID"].ToString() == "0003")
                {                    
                    string USERNAME = Session["USERNAME"].ToString();
                    if (ModelState.IsValid)
                    {
                        List<SelectListItem> Decline = new List<SelectListItem>();
                        Decline = approvebll.DeclineData(ID, USERNAME);
                        return RedirectToAction("search", "Expendeture");
                    }

                    return RedirectToAction("search", "Expendeture");
                }
                else
                {
                    return RedirectToAction("search", "Expendeture");
                }
                    
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
            
        }

        public FileResult ViewAttachment(string ID)
        {
            string filename = Path.GetFileName(ID);
            string path = Server.MapPath("~/Receipt/");
            string fullpath = Path.Combine(path, filename);
            return File(fullpath, "pdf");

        }

        public FileResult Download(string ID)
        {
            string filename = Path.GetFileName(ID);
            string path = Server.MapPath("~/Receipt/");
            string fullpath = Path.Combine(path, filename);
            return File(fullpath, "image/png", ID);

        }

        public ActionResult Delete(string ID)
        {
            if (Session["USERNAME"] != null)
            {
                if (Session["ROLLID"].ToString() == "0002" || Session["ROLLID"].ToString() == "0003")
                {
                    string USERNAME = Session["USERNAME"].ToString();
                    bool DeleteStatus = approvebll.DeleteConfirmedExpNO(ID,USERNAME);
                    if (DeleteStatus == true)
                    {
                        TempData["Message"] = "Expditure Delete Successfully !";
                        return RedirectToAction("search", "Expendeture");
                    }
                    else
                    {
                        TempData["Message"] = "Fail To Delete Expenditure!";
                    }
                    return RedirectToAction("search", "Expendeture");
                }
                else
                {
                    return RedirectToAction("search", "Expendeture");
                }
                    
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }
    }
}