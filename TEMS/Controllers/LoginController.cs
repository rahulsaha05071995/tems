﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TEMS_BLL;
using TEMS_BOL;

namespace TEMS.Controllers
{
    public class LoginController : Controller
    {        
        public ActionResult login()
        {
            loginBOL emp = new loginBOL();
            return View(emp);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult login(loginBOL emp)
        {
            loginBLL loginbl = new loginBLL();
            var user = loginbl.checkuser(emp);
            if(user == null)
            {
                TempData["Message"] = "Wrong Username or Password";
                return View("login", user);
            }
            else
            {
                Session["USERID"] = user.USER_ID;
                Session["USERNAME"] = user.FULL_NAME;
                Session["ROLLID"] = user.REF_ROLE_ID;
                return RedirectToAction("Dashboard");
            }            
        }

        public ActionResult Logout()
        {
            //int USERID = (int)Session["USERID"];
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("login");
        }

        public ActionResult Dashboard()
        {
            return View();
        }
    }
}