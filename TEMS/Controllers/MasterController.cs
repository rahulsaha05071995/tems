﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TEMS_BLL;
using TEMS_BOL;
using TEMS_DAL;

namespace TEMS.Controllers
{
    public class MasterController : Controller
    {
        AddNewUserBLL UserBLL = new AddNewUserBLL();
        ExpendetureBLL expendBLL = new ExpendetureBLL();

        public ActionResult UserList()
        {
            if (Session["USERNAME"] != null)
            {
                SearchNewUserBOL search = new SearchNewUserBOL();
                List<SelectListItem> ListRole = new List<SelectListItem>();

                ListRole = UserBLL.RoleList();

                search.ListRole = ListRole;
                return View(search);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }


        }

        [HttpPost]
        public ActionResult UserList(SearchNewUserBOL search)
        {
            SearchNewUserBLL SearchBLL = new SearchNewUserBLL();
            if (Session["USERNAME"].ToString() != null)
            {
                List<SelectListItem> ListRole = new List<SelectListItem>();

                ListRole = UserBLL.RoleList();

                search.UserListReportData = SearchBLL.GetUserList(search);

                search.ListRole = ListRole;
                return View(search);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }



        public ActionResult AddUser()
        {
            if (Session["USERNAME"].ToString() != null)
            {
                AddNewUserBOL User = new AddNewUserBOL();
                List<SelectListItem> ListRole = new List<SelectListItem>();
                List<SelectListItem> ListGrade = new List<SelectListItem>();

                ListRole = UserBLL.RoleList();
                ListGrade = UserBLL.GradeList();

                User.ListRole = ListRole;
                User.ListGrade = ListGrade;
                return View(User);
            }

            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddUser(AddNewUserBOL User)
        {
            if (Session["USERNAME"] != null)
            {
                List<SelectListItem> ListRole = new List<SelectListItem>();
                List<SelectListItem> ListGrade = new List<SelectListItem>();
                if (ModelState.IsValid)
                {
                    string USERNAME = Session["USERNAME"].ToString();
                    int DuplicateUserEmail = UserBLL.CheckDuplicateUserEmail(User);
                    if (DuplicateUserEmail == 0)
                    {
                        bool SaveUser = UserBLL.SaveUserDetails(User, USERNAME);

                        if (SaveUser == true)
                        {
                            TempData["Message"] = "User Details Added Successfully !";

                            return RedirectToAction("AddUser");
                        }
                        else
                        {
                            TempData["Message"] = "Fail To Save User Details !";
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Email ID Already Exists!";
                    }
                }
                ListRole = UserBLL.RoleList();
                ListGrade = UserBLL.GradeList();

                User.ListRole = ListRole;
                User.ListGrade = ListGrade;
                return View(User);
            }

            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        [HttpGet]
        public ActionResult Edit(string UID)
        {
            if (Session["USERNAME"] != null)
            {
                AddNewUserBOL User = new AddNewUserBOL();
                List<SelectListItem> ListRole = new List<SelectListItem>();
                List<SelectListItem> ListGrade = new List<SelectListItem>();
                if (UID != null && UID != "")
                {
                    User = UserBLL.EditUser(UID);
                    ListRole = UserBLL.RoleList();
                    ListGrade = UserBLL.GradeList();

                    User.ListRole = ListRole;
                    User.ListGrade = ListGrade;
                }
                else
                {
                    TempData["Message"] = "Invalid User ID!";
                    return RedirectToAction("UserList");
                }
                return View(User);
            }

            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AddNewUserBOL User)
        {
            if (Session["USERNAME"] != null)
            {
                List<SelectListItem> ListRole = new List<SelectListItem>();
                List<SelectListItem> ListGrade = new List<SelectListItem>();

                if (ModelState.IsValid)
                {
                    string USERNAME = Session["USERNAME"].ToString();
                    //if (User.Password == null)
                    //{
                        int DuplicateUserEmail = UserBLL.CheckDuplicateUserEditEmail(User);
                        if (DuplicateUserEmail == 0)
                        {
                            bool EditUser = UserBLL.EditUserDetails(User, USERNAME);

                            if (EditUser == true)
                            {
                                TempData["Message"] = "User Details Updated Successfully !";

                                return RedirectToAction("UserList");
                            }
                            else
                            {
                                TempData["Message"] = "Fail To Save User Details !";
                            }
                        }
                        else
                        {
                            TempData["Message"] = "Email ID Already Exists!";
                        }

                    ListRole = UserBLL.RoleList();
                    ListGrade = UserBLL.GradeList();

                }
                else
                {
                    TempData["Message"] = "Please Enter Valid Data !";
                }
                User.ListRole = ListRole;
                User.ListGrade = ListGrade;
                return View(User);

            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }                

        [HttpGet]
        public ActionResult AgencyList()
        {
            if (Session["USERNAME"] != null)
            {
                AddAgencyBOL search = new AddAgencyBOL();
                List<SelectListItem> AgencyNameList = new List<SelectListItem>();

                AgencyNameList = expendBLL.ListAgency();
                search.AgencyNameList = AgencyNameList;
                return View(search);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }


        }

        [HttpPost]        
        public ActionResult AgencyList(AddAgencyBOL search)
        {
            AgencyBLL Agncybll = new AgencyBLL();
            if (Session["USERNAME"] != null)
            {                
                List<SelectListItem> AgencyNameList = new List<SelectListItem>();

                AgencyNameList = expendBLL.ListAgency();

                search.AgencyNameList = AgencyNameList;
                search.AgencyListReportData = Agncybll.GetagencyList(search);
                return View(search);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }


        }

        [HttpGet]
        public ActionResult AddAgency()
        {
            if (Session["USERNAME"] != null)
            {
                AddAgencyBOL agency = new AddAgencyBOL();
                return View(agency);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAgency(AddAgencyBOL agency)
        {
            AgencyBLL Agncybll = new AgencyBLL();
            if (Session["USERNAME"] != null)
            {
                if (ModelState.IsValid)
                {
                    string USERID = Session["USERID"].ToString();
                    bool checkagencydata = Agncybll.AddAgencyData(agency,USERID);

                    if (checkagencydata == true)
                    {
                        TempData["Message"] = "Agency Details Added Successfully !";
                        return RedirectToAction("AddAgency");
                    }
                    else
                    {
                        TempData["Message"] = "Fail To Add Agency Details !";
                    }
                }
                else
                {
                    TempData["Message"] = "Please Enter Valid Data !";
                }
                return View(agency);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        [HttpGet]
        public ActionResult EditAgency(string AId)
        {
            AgencyBLL Agncybll = new AgencyBLL();
            if (Session["USERNAME"] != null)
            {
                AddAgencyBOL agency = new AddAgencyBOL();
                if (AId != null && AId != "")
                {
                    agency = Agncybll.EditAgency(AId);
                }
                else
                {
                    TempData["Message"] = "Invalid Agency ID!";
                    return RedirectToAction("AgencyList");
                }
                return View(agency);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAgency(AddAgencyBOL agency)
        {
            if (Session["USERNAME"] != null)
            {
                if (ModelState.IsValid)
                {
                    AgencyBLL Agncybll = new AgencyBLL();
                    string USERID = Session["USERID"].ToString();
                    bool Editagencydata = Agncybll.EditAgencyData(agency, USERID);
                    if (Editagencydata == true)
                    {
                        TempData["Message"] = "Agency Details Updated Successfully !";
                        return RedirectToAction("AgencyList");
                    }
                    else
                    {
                        TempData["Message"] = "Fail To Update Agency Details !";
                        return RedirectToAction("EditAgency");
                    }

                }
                else
                {
                    TempData["Message"] = "Please Enter Valid Data!";
                }
                return View(agency);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpGet]
        public ActionResult ProjectList()
        {
            if (Session["USERNAME"] != null)
            {
                ProjectBOL Project = new ProjectBOL();
                List<SelectListItem> ProjectList = new List<SelectListItem>();
                ProjectList = expendBLL.ListProject();

                Project.ProjectList = ProjectList;
                return View(Project);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpPost]
        public ActionResult ProjectList(ProjectBOL Project)
        {
            if (Session["USERNAME"] != null)
            {
                ProjectBLL projectbll = new ProjectBLL();
                List<SelectListItem> ProjectList = new List<SelectListItem>();
                ProjectList = expendBLL.ListProject();

                Project.ProjectList = ProjectList;
                Project.ProjectListData = projectbll.GetProjectList(Project);
                return View(Project);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpGet]
        public ActionResult AddProject()
        {
            if (Session["USERNAME"] != null)
            {
                ProjectBOL Project = new ProjectBOL();
                return View(Project);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddProject(ProjectBOL Project)
        {
            if (Session["USERNAME"] != null)
            {
                if (ModelState.IsValid)
                {
                    ProjectBLL projectbll = new ProjectBLL();
                    string USERID = Session["USERID"].ToString();
                    bool checkprojectdata = projectbll.AddProjectData(Project, USERID);

                    if (checkprojectdata == true)
                    {
                        TempData["Message"] = "Project Details Added Successfully !";
                        return RedirectToAction("AddProject");
                    }
                    else
                    {
                        TempData["Message"] = "Fail To Add Project Details !";
                    }
                }
                else
                {
                    TempData["Message"] = "Please Enter Valid Data !";
                }
                return View(Project);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }
        
        [HttpGet]
        public ActionResult EditProject(string PId)
        {
            if (Session["USERNAME"] != null)
            {
                ProjectBOL Project = new ProjectBOL();
                ProjectBLL projectbll = new ProjectBLL();
                if (PId != null && PId != "")
                {
                    Project = projectbll.Editproject(PId);
                }
                else
                {
                    TempData["Message"] = "Invalid Project ID!";
                    return RedirectToAction("ProjectList");
                }
                return View(Project);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProject(ProjectBOL Project)
        {
            if (Session["USERNAME"] != null)
            {
                if (ModelState.IsValid)
                {
                    ProjectBLL projectbll = new ProjectBLL();
                    string USERID = Session["USERID"].ToString();
                    bool EditProjectdata = projectbll.EditProjectData(Project, USERID);
                    if (EditProjectdata == true)
                    {
                        TempData["Message"] = "Project Details Updated Successfully !";
                        return RedirectToAction("ProjectList");
                    }
                    else
                    {
                        TempData["Message"] = "Fail To Update Project Details !";
                        return RedirectToAction("EditProject");
                    }
                }
                else
                {
                    TempData["Message"] = "Please Enter Valid Data!";
                }
                return View(Project);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpGet]
        public ActionResult DistrictList()
        {
            if (Session["USERNAME"] != null)
            {
                AddDistrictBOL District = new AddDistrictBOL();
                CityBLL citybll = new CityBLL();
                List<SelectListItem> DistrictList = new List<SelectListItem>();
                List<SelectListItem> StateList = new List<SelectListItem>();

                DistrictList = citybll.ListDistrict();
                StateList = expendBLL.ListSate();


                District.DistrictList = DistrictList;
                District.StateList = StateList;

                return View(District);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpPost]
        public ActionResult DistrictList(AddDistrictBOL District)
        {
            if (Session["USERNAME"] != null)
            {
                AddDistrictBLL DistrictBLL = new AddDistrictBLL();
                CityBLL citybll = new CityBLL();
                List<SelectListItem> DistrictList = new List<SelectListItem>();
                List<SelectListItem> StateList = new List<SelectListItem>();
                DistrictList = citybll.ListDistrict();
                StateList = expendBLL.ListSate();

                District.DistrictList = DistrictList;
                District.StateList = StateList;
                District.DistrictListReportData = DistrictBLL.GetDistrictData(District);
                return View(District);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpGet]
        public ActionResult AddDistrict()
        {
            if (Session["USERNAME"] != null)
            {
                AddDistrictBLL DistrictBLL = new AddDistrictBLL();
                AddDistrictBOL District = new AddDistrictBOL();
                List<SelectListItem> StateList = new List<SelectListItem>();
                StateList = DistrictBLL.ListState();

                District.StateList = StateList;
                return View(District);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpPost]
        public ActionResult AddDistrict(AddDistrictBOL District)
        {
            if (Session["USERNAME"] != null)
            {
                if (ModelState.IsValid)
                {
                    AddDistrictBLL DistrictBLL = new AddDistrictBLL();
                    List<SelectListItem> StateList = new List<SelectListItem>();
                    StateList = DistrictBLL.ListState();

                    string USERID = Session["USERID"].ToString();
                    bool checkdistrictdata = DistrictBLL.AddDistrictData(District, USERID);

                    if (checkdistrictdata == true)
                    {
                        TempData["Message"] = "District Details Added Successfully !";
                        return RedirectToAction("AddDistrict");
                    }
                    else
                    {
                        TempData["Message"] = "Fail To Add District Details !";
                    }

                    District.StateList = StateList;
                }
                else
                {
                    TempData["Message"] = "Please Enter Valid Data !";
                }
                return View(District);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpGet]
        public ActionResult EditDistrict(string DId)
        {
            if (Session["USERNAME"] != null)
            {
                AddDistrictBOL District = new AddDistrictBOL();
                AddDistrictBLL DistrictBLL = new AddDistrictBLL();
                List<SelectListItem> StateList = new List<SelectListItem>();
                StateList = DistrictBLL.ListState();
                if (DId != null && DId != "")
                {
                    District = DistrictBLL.EditDistrict(DId);
                }
                else
                {
                    TempData["Message"] = "Invalid District ID!";
                    return RedirectToAction("DistrictList");
                }
                District.StateList = StateList;
                return View(District);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpPost]
        public ActionResult EditDistrict(AddDistrictBOL District)
        {
            if (Session["USERNAME"] != null)
            {
                List<SelectListItem> StateList = new List<SelectListItem>();
                if (ModelState.IsValid)
                {
                    AddDistrictBLL DistrictBLL = new AddDistrictBLL();
                    
                    StateList = DistrictBLL.ListState();
                    string USERID = Session["USERID"].ToString();
                    bool EditDistrictdata = DistrictBLL.EditDistrictData(District, USERID);
                    if (EditDistrictdata == true)
                    {
                        TempData["Message"] = "District Details Updated Successfully !";
                        return RedirectToAction("DistrictList");
                    }
                    else
                    {
                        TempData["Message"] = "Fail To Update District Details !";
                        return RedirectToAction("EditDistrict");
                    }                    
                }

                else
                {
                    TempData["Message"] = "Please Enter Valid Data!";
                }
                District.StateList = StateList;
                return View(District);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpGet]
        public ActionResult CityList()
        {
            if (Session["USERNAME"] != null)
            {
                CityBLL citybll = new CityBLL();
                AddCityBOL City = new AddCityBOL();
                List<SelectListItem> CityList = new List<SelectListItem>();
                List<SelectListItem> StateList = new List<SelectListItem>();
                List<SelectListItem> DistrictList = new List<SelectListItem>();

                StateList = expendBLL.ListSate();
                DistrictList = citybll.ListDistrict();
                CityList = citybll.ListCity();

                City.CityList = CityList;
                City.DistrictList = DistrictList;
                City.StateList = StateList;
                return View(City);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpPost]
        public ActionResult CityList(AddCityBOL City)
        {
            if (Session["USERNAME"] != null)
            {
                CityBLL citybll = new CityBLL();
                List<SelectListItem> CityList = new List<SelectListItem>();
                List<SelectListItem> StateList = new List<SelectListItem>();
                List<SelectListItem> DistrictList = new List<SelectListItem>();

                StateList = expendBLL.ListSate();
                DistrictList = citybll.ListDistrict();
                CityList = citybll.ListCity();

                City.CityList = CityList;
                City.DistrictList = DistrictList;
                City.StateList = StateList;
                City.CityListData = citybll.GetCityData(City);
                return View(City);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpGet]
        public ActionResult AddCity()
        {
            if (Session["USERNAME"] != null)
            {
                CityBLL citybll = new CityBLL();
                AddDistrictBLL DistrictBLL = new AddDistrictBLL();                
                AddCityBOL City = new AddCityBOL();

                List<SelectListItem> StateList = new List<SelectListItem>();
                List<SelectListItem> DistrictList = new List<SelectListItem>();


                StateList = DistrictBLL.ListState();
                DistrictList = citybll.ListDistrict();

                City.StateList = StateList;
                City.DistrictList = DistrictList;
                return View(City);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        public JsonResult GetDistrictList(string STATE_ID)
        {
            TEMSEntities db = new TEMSEntities();
            db.Configuration.ProxyCreationEnabled = false;
            List<MSTR_DISTRICT> District = db.MSTR_DISTRICT.Where(x => x.ISACTIVE == true && x.REF_STATE_ID == STATE_ID).ToList();
            return Json(District, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddCity(AddCityBOL City)
        {
            if (Session["USERNAME"] != null)
            {
                CityBLL citybll = new CityBLL();
                AddDistrictBLL DistrictBLL = new AddDistrictBLL();                

                List<SelectListItem> StateList = new List<SelectListItem>();
                List<SelectListItem> DistrictList = new List<SelectListItem>();
                string USERID = Session["USERID"].ToString();

                bool CheckAddCity = citybll.AddCityData(City, USERID);
                if(CheckAddCity == true)
                {
                    TempData["Message"] = "City Details Added Successfully !";
                    return RedirectToAction("AddCity");
                }
                else
                {
                    TempData["Message"] = "Fail To Add City Details !";
                }

                StateList = DistrictBLL.ListState();
                DistrictList = citybll.ListDistrict();

                City.StateList = StateList;
                City.DistrictList = DistrictList;
                return View(City);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpGet]
        public ActionResult EditCity(string CId)
        {
            if (Session["USERNAME"] != null)
            {
                CityBLL citybll = new CityBLL();
                AddDistrictBLL DistrictBLL = new AddDistrictBLL();
                AddCityBOL City = new AddCityBOL();

                List<SelectListItem> StateList = new List<SelectListItem>();
                List<SelectListItem> DistrictList = new List<SelectListItem>();

                StateList = DistrictBLL.ListState();
                DistrictList = citybll.ListDistrict();

                if (CId != null && CId != "")
                {
                    City = citybll.EditCity(CId);
                }
                else
                {
                    TempData["Message"] = "Invalid City ID!";
                    return RedirectToAction("CityList");
                }

                City.StateList = StateList;
                City.DistrictList = DistrictList;
                return View(City);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpPost]
        public ActionResult EditCity(AddCityBOL City)
        {
            if (Session["USERNAME"] != null)
            {
                CityBLL citybll = new CityBLL();
                AddDistrictBLL DistrictBLL = new AddDistrictBLL();               

                List<SelectListItem> StateList = new List<SelectListItem>();
                List<SelectListItem> DistrictList = new List<SelectListItem>();

                if (ModelState.IsValid)
                {
                    StateList = DistrictBLL.ListState();
                    DistrictList = citybll.ListDistrict();

                    string USERID = Session["USERID"].ToString();
                    bool EditCitydata = citybll.EditCityData(City, USERID);
                    if (EditCitydata == true)
                    {
                        TempData["Message"] = "City Details Updated Successfully !";
                        return RedirectToAction("CityList");
                    }
                    else
                    {
                        TempData["Message"] = "Fail To Update City Details !";
                        return RedirectToAction("EditCity");
                    }
                }
                else
                {
                    TempData["Message"] = "Please Enter Valid Data!";
                }

                City.StateList = StateList;
                City.DistrictList = DistrictList;
                return View(City);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpGet]
        public ActionResult CostCenterList()
        {
            if (Session["USERNAME"] != null)
            {
                AddCostCenterBOL costcenter = new AddCostCenterBOL();
                AddCostCenterBLL addcostcenter = new AddCostCenterBLL();
                List<SelectListItem> CityList = new List<SelectListItem>();
                List<SelectListItem> CostCenterList = new List<SelectListItem>();

                CityList = addcostcenter.ListCity();
                CostCenterList = addcostcenter.ListCostCenter();

                costcenter.CityList = CityList;
                costcenter.CostCenterList = CostCenterList;
                return View(costcenter);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        public JsonResult GetCostCenterList(string CITY_ID)
        {
            TEMSEntities db = new TEMSEntities();
            db.Configuration.ProxyCreationEnabled = false;
            List<MSTR_COST_CENTER> CostCenter = db.MSTR_COST_CENTER.Where(x => x.ISACTIVE == true && x.REF_CITY_ID == CITY_ID).ToList();
            return Json(CostCenter, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CostCenterList(AddCostCenterBOL costcenter)
        {
            if (Session["USERNAME"] != null)
            {
                AddCostCenterBLL addcostcenter = new AddCostCenterBLL();
                List<SelectListItem> CityList = new List<SelectListItem>();
                List<SelectListItem> CostCenterList = new List<SelectListItem>();

                CityList = addcostcenter.ListCity();
                CostCenterList = addcostcenter.ListCostCenter();

                costcenter.CityList = CityList;
                costcenter.CostCenterList = CostCenterList;
                costcenter.CostCenterDataList = addcostcenter.ListCostCenterData(costcenter);
                return View(costcenter);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        [HttpGet]
        public ActionResult AddCostCenter()
        {
            if (Session["USERNAME"] != null)
            {
                AddCostCenterBOL costcenter = new AddCostCenterBOL();
                AddCostCenterBLL addcostcenter = new AddCostCenterBLL();
                List<SelectListItem> CityList = new List<SelectListItem>();

                CityList = addcostcenter.ListCity();

                costcenter.CityList = CityList;
                return View(costcenter);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddCostCenter(AddCostCenterBOL costcenter)
        {
            if (Session["USERNAME"] != null)
            {
                AddCostCenterBLL addcostcenter = new AddCostCenterBLL();
                List<SelectListItem> CityList = new List<SelectListItem>();

                CityList = addcostcenter.ListCity();

                string USERID = Session["USERID"].ToString();

                bool CheckAddCostCenter = addcostcenter.AddCostCenterData(costcenter, USERID);
                if (CheckAddCostCenter == true)
                {
                    TempData["Message"] = "Cost Center Details Added Successfully !";
                    return RedirectToAction("AddCostCenter");
                }
                else
                {
                    TempData["Message"] = "Fail To Add Cost Center Details !";
                }

                costcenter.CityList = CityList;
                return View(costcenter);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }

        }

        [HttpGet]
        public ActionResult EditCostCenter(string CostCenterId)
        {
            if (Session["USERNAME"] != null)
            {
                AddCostCenterBOL costcenter = new AddCostCenterBOL();
                AddCostCenterBLL addcostcenter = new AddCostCenterBLL();
                List<SelectListItem> CityList = new List<SelectListItem>();

                CityList = addcostcenter.ListCity();

                if (CostCenterId != null && CostCenterId != "")
                {
                    costcenter = addcostcenter.EditCostCenter(CostCenterId);
                }
                else
                {
                    TempData["Message"] = "Invalid Cost Center ID!";
                    return RedirectToAction("CostCenterList");
                }
                costcenter.CityList = CityList;
                return View(costcenter);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCostCenter(AddCostCenterBOL costcenter)
        {
            if (Session["USERNAME"] != null)
            {
                AddCostCenterBLL addcostcenter = new AddCostCenterBLL();
                List<SelectListItem> CityList = new List<SelectListItem>();
                
                if (ModelState.IsValid)
                {
                    CityList = addcostcenter.ListCity();

                    string USERID = Session["USERID"].ToString();
                    bool EditCostCenterdata = addcostcenter.EditCostCenterdata(costcenter, USERID);
                    if (EditCostCenterdata == true)
                    {
                        TempData["Message"] = "Cost Center Details Updated Successfully !";
                        return RedirectToAction("CostCenterList");
                    }
                    else
                    {
                        TempData["Message"] = "Fail To Update Cost Center Details !";
                        return RedirectToAction("EditCostCenter");
                    }
                }
                else
                {
                    TempData["Message"] = "Please Enter Valid Data!";
                }

                costcenter.CityList = CityList;
                return View(costcenter);
            }
            else
            {
                return RedirectToAction("login", "Login");
            }
        }
    }
}