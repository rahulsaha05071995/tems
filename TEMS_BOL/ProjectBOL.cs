﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TEMS_BOL
{
    public class ProjectBOL
    {
        public List<SelectListItem> ProjectList { get; set; }

        public string ProjectId { get; set; }

        public string ProjectName { get; set; }

        public string Status { get; set; }

        public List<ProjectData> ProjectListData { get; set; }

        public class ProjectData
        {
            public string ProjectId { get; set; }

            public string ProjectName { get; set; }

            public string Status { get; set; }
        }
    }
}
