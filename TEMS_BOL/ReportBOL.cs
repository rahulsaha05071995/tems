﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TEMS_BOL
{
    public class ReportBOL
    {
    }

    public class TransactionReportBOL
    {
        [DisplayName("Report For")]
        public string ReportFor { get; set; }

        [DisplayName("Agency Name")]
        public string AgencyName { get; set; }

        [DisplayName("Employee Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Report Type")]
        public string ReportType { get; set; }

        [DisplayName("From Date ")]
        public string FromDate { get; set; }

        [DisplayName("To Date")]
        public string ToDate { get; set; }

        public List<SelectListItem> AgencyNameList { get; set; }
        public List<SelectListItem> EmployeesNameList { get; set; }
        public List<TransactionReportListBOL> TransactionReportList { get; set; }
    }

    public class TransactionReportListBOL
    {
        public string AgencyID { get; set; }
        public string EmployeeID { get; set; }
        public string AgencyName { get; set; }
        public string EmployeeName { get; set; }
        public decimal ClaimedAmount { get; set; }
        public decimal ApprovedAmount { get; set; }
        public decimal AdvanceAmount { get; set; }
        public decimal AdvanceAdjustmentAmount { get; set; }
        public decimal PaymentAmount { get; set; }
        public decimal PaidAmount { get; set; }
    }

    public class ReportListBOL
    {
        public string AgencyName { get; set; }
        public string EmployeeName { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public List<TransactionHistoryListBOL> ListReportHistory { get; set; }
    }

    public class TransactionHistoryListBOL
    {
        public string Particulars { get; set; }
        public string VoucherNo { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime EntryDate { get; set; }
        public decimal Amount { get; set; }
    }

    public class RegisterReportBOL
    {
        [DisplayName("Report For")]
        public string ReportFor { get; set; }

        [DisplayName("Name")]
        public string EmployeeOrAgencyName { get; set; }

        [DisplayName("Agency Name")]
        public string AgencyName { get; set; }

        [DisplayName("Employee Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Report Type")]
        public string ReportType { get; set; }

        [DisplayName("View By")]
        public string ViewBy { get; set; }

        [DisplayName("Year ")]
        public string Years { get; set; }

        [DisplayName("Month ")]
        public string Month { get; set; }

        [DisplayName("From Month ")]
        public string FromMonth { get; set; }

        [DisplayName("To Month ")]
        public string ToMonth { get; set; }

        [DisplayName("From Date ")]
        public string FromDate { get; set; }

        [DisplayName("To Date")]
        public string ToDate { get; set; }

        public List<SelectListItem> EmployeeAgencyNameList { get; set; }
        public List<SelectListItem> AgencyNameList { get; set; }
        public List<SelectListItem> EmployeesNameList { get; set; }
        public List<RegisterReportListBOL> RegisterReportList { get; set; }
    }

    public class RegisterReportListBOL
    {
        public string MonthName { get; set; }
        public string Names { get; set; }
        public decimal ClaimedAmount { get; set; }
        public decimal ApprovedAmount { get; set; }
        public decimal AdvanceAmount { get; set; }
        public decimal AdvanceAdjustmentAmount { get; set; }
        public decimal PaymentAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal BalanceAmount { get; set; }
        public decimal UnsettledClaimsAmount { get; set; }
    }

    public class ExpendetureReportBOL
    {
        [DisplayName("Expendeture No")]
        public string ExpendetureNo { get; set; }

        [DisplayName("Report For")]
        public string ReportFor { get; set; }

        [DisplayName("Name")]
        public string EmployeeOrAgencyName { get; set; }

        [DisplayName("Agency Name")]
        public string AgencyName { get; set; }

        [DisplayName("Employee Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Report Type")]
        public string ReportType { get; set; }

        [DisplayName("From Date ")]
        public string FromDate { get; set; }

        [DisplayName("To Date")]
        public string ToDate { get; set; }

        public List<SelectListItem> EmployeeAgencyNameList { get; set; }
        public List<SelectListItem> AgencyNameList { get; set; }
        public List<SelectListItem> EmployeesNameList { get; set; }
        public List<ExpendetureSummaryReportListBOL> ExpendetureSummaryReportList { get; set; }
        public List<ExpendetureDetailsReportListBOL> ExpendetureDetailsReportList { get; set; }
    }

    public class ExpendetureSummaryReportListBOL
    {
        public string ExpendetureNo { get; set; }
        public string ReportFor { get; set; }
        public string Name { get; set; }
        public DateTime VoucherDate { get; set; }
        public decimal EntryAmount { get; set; }
        public decimal ClaimedAmount { get; set; }
        public decimal ApprovedAmount { get; set; }
        public string Status { get; set; }
    }

    public class ExpendetureDetailsReportListBOL
    {
        public string ExpendetureNo { get; set; }
        public string ReportFor { get; set; }
        public string Name { get; set; }
        public DateTime VoucherDate { get; set; }
        public string ExpendetureHead { get; set; }
        public decimal EntryAmount { get; set; }
        public decimal ClaimedAmount { get; set; }
        public decimal ApprovedAmount { get; set; }
        public string Status { get; set; }
    }
}
