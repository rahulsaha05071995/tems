﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TEMS_BOL
{
    public class AddAgencyBOL
    {
        public List<SelectListItem> AgencyNameList { get; set; }

        public string AgencyName { get; set; }
        public string AgencyAddress { get; set; }
        public string AgencyMobileNo { get; set; }
        public string AgencyWhatsAppNo { get; set; }
        public string AgencyEmail { get; set; }
        public string AgencyID { get; set; }

        public string Status { get; set; }

        public List<AgencyListData> AgencyListReportData { get; set; }

        public class AgencyListData
        {
            public string AgencyID { get; set; }

            public string AgencyName { get; set; }

            public string AgencyEmail { get; set; }

            public string Status { get; set; }

            public string AgencyMobileNo { get; set; }

            public string AgencyWhatsAppNo { get; set; }

            public string AgencyAddress { get; set; }
        }
    }
}
