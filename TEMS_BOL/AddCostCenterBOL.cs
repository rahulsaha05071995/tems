﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TEMS_BOL
{
    public class AddCostCenterBOL
    {
        public List<SelectListItem> CityList { get; set; }
        public List<SelectListItem> CostCenterList { get; set; }

        public string CostCenterID { get; set; }
        public string RefCityID { get; set; }
        public string CostCenterName { get; set; }
        public string Status { get; set; }

        public List<CostCenterData> CostCenterDataList { get; set; }

        public class CostCenterData
        {
            public string CostCenterID { get; set; }
            public string RefCityID { get; set; }
            public string CostCenterName { get; set; }
            public string Status { get; set; }
        }
    }
}
