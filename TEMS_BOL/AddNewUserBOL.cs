﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TEMS_BOL
{
    public class AddNewUserBOL
    {
        public List<SelectListItem> ListRole { get; set; }
        public List<SelectListItem> ListGrade { get; set; }

        public string UserID { get; set; }
        public string Name { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password")]        
        public string ConfirmPassword { get; set; }

        [DataType(DataType.EmailAddress)]
        public string EmailID { get; set; }

        public string Role { get; set; }

        public string Designation { get; set; }

        public string Grade { get; set; }
        
        [DisplayName("Fooding Expense")]
        public decimal FoodingExpense { get; set; }

        [DisplayName("Lodging Expense")]
        public decimal LodgingExpense { get; set; }
    }
}
