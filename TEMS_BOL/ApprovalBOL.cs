﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace TEMS_BOL
{
    public class ApprovalBOL
    {
        public List<SelectListItem> PaymentForList { get; set; }
        public List<SelectListItem> CostCenterList { get; set; }
        public List<SelectListItem> StateList { get; set; }
        public List<SelectListItem> CityList { get; set; }
        public List<SelectListItem> ProjectList { get; set; }
        public List<SelectListItem> AgencyList { get; set; }
        public List<expendeture_details> ExpendetureDetailsList { get; set; }
        public List<CarExpendetureDetails> CarExpendetureList { get; set; }


        [Required(ErrorMessage = "This Field is Required")]
        [DisplayName("Expendeture No. ")]
        public string expendeture_no { get; set; }

        public string State { get; set; }

        [Required(ErrorMessage = "This Field is Required")]
        [DisplayName("Journey Date")]
        public string journey_date { get; set; }

        [Required(ErrorMessage = "This Field is Required")]
        [DisplayName("Journey From")]
        public string Journey_from { get; set; }

        [Required(ErrorMessage = "This Field is Required")]
        [DisplayName("Journey To")]
        public string Journey_to { get; set; }

        public string Journey_type { get; set; }

        public string cilent_visited { get; set; }

        public string Remarks { get; set; }

        public string ApproverRemarks { get; set; }

        [DisplayName("Car Hire?")]
        public bool car_hire { get; set; }

        public string Status { get; set; }

        public string Agency { get; set; }

        [DisplayName("Car No.")]
        public string car_no { get; set; }

        [DisplayName("Car Rental Charge(Rs.)")]

        public string car_rental_charge { get; set; }

        [DisplayName("Driver Name")]
        public string driver_name { get; set; }

        [DisplayName("Mileage(KM/Ltr.)")]
        public string Car_Mileage { get; set; }

        [DisplayName("Distance Covered")]
        public string distance_covered { get; set; }

        [DisplayName("Fuel Rate(Rs.)")]
        public string fuel_rate { get; set; }

        public string car_rent_recipt { get; set; }

        public string toll_tax_recipt { get; set; }

        public string fuel_cost_receipt { get; set; }

        public class expendeture_details
        {
            public string Cost_Center { get; set; }

            public string project { get; set; }

            public string ExpendetureFor { get; set; }

            public string expendeture_amount { get; set; }                    

            public string approved_amount { get; set; }

            public string expendeture_attachment { get; set; }
        }

        public class CarExpendetureDetails
        {
            public string Cost_Center { get; set; }
            public string distance_travel { get; set; }
            public string fuel_cost_for_cost_center { get; set; }
            public string car_rent_for_cost_center { get; set; }
            public string toll_tax { get; set; }
            public string total { get; set; }

        }
    }
}
