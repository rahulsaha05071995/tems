﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TEMS_BOL
{
    public class PayAmountBOL
    {
        public List<SelectListItem> PaymentToList { get; set; }
        public List<SelectListItem> ExpIDList { get; set; }
        public List<SelectListItem> AgencyNameList { get; set; }
        public int AdvanceAdjId { get; set; }
        public string VoucherNo { get; set; }

        public string PaymentDate { get; set; }

        public string PaymentTo { get; set; }

        public string PaymentFor { get; set; }

        public string AgencyName { get; set; }

        public string PaymentMode { get; set; }

        public string PaymentType { get; set; }

        public string ExpAmount { get; set; }

        public decimal Amount { get; set; }

        public string ExpID { get; set; }

        public string AdvancedAmount { get; set; }

        public string FromDate { get; set; }
        public string ToDate { get; set; }

        public List<SearchPayment> TableDataList { get; set; }

        public class SearchPayment
        {
            public string VoucherNo { get; set; }

            public string VoucherDate { get; set; }

            public string PaymentTo { get; set; }            

            public string PaymentMode { get; set; }

            public string PaymentType { get; set; }

            public decimal Amount { get; set; }

            public string CreatedDate { get; set; }
        }
    }
}
