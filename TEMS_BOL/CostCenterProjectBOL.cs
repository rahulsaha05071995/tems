﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TEMS_BOL
{
    public class CostCenterProjectBOL
    {
        public List<SelectListItem> CostCenterList { get; set; }

        public List<SelectListItem> ProjectList { get; set; }

        public string MapId { get; set; }

        public string CostCenterId { get; set; }

        public string ProjectId { get; set; }

        public string Status { get; set; }

        public List<CostCenterProjectData> CostCenterProjectListData { get; set; }

        public class CostCenterProjectData
        {
            public string MapId { get; set; }
            public string CostCenterId { get; set; }

            public string ProjectId { get; set; }

            public string Status { get; set; }
        }
    }
}
