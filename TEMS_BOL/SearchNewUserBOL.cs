﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TEMS_BOL
{
    public class SearchNewUserBOL
    {
        public List<SelectListItem> ListRole { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public string Role { get; set; }

        public string Status { get; set; }

        public List<UserListReportBO> UserListReportData { get; set; }

        public class UserListReportBO
        {
            public string UserID { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public string Designation { get; set; }
            public string Role { get; set; }
            public string Status { get; set; }
        }
    }
}
