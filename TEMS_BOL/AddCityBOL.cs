﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace TEMS_BOL
{
    public class AddCityBOL
    {
        public List<SelectListItem> DistrictList { get; set; }

        public List<SelectListItem> StateList { get; set; }

        public List<SelectListItem> CityList { get; set; }

        public string RefDistrictId { get; set; }

        public string CityName { get; set; }

        public string RefStateId { get; set; }

        public string Status { get; set; }

        public string CityId { get; set; }

        public List<CityData> CityListData { get; set; }

        public class CityData
        {
            public string RefStateId { get; set; }
            public string RefDistrictId { get; set; }
            public string Status { get; set; }
            public string CityName { get; set; }
            public string CityId { get; set; }
        }
    }
}
