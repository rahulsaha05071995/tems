﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TEMS_BOL
{
    public class SearchExpenditureBOL
    {
        public List<SelectListItem> ExpenditureByList { get; set; }
        public List<SelectListItem> PaymentForList { get; set; }
        public List<SelectListItem> CostCenterList { get; set; }
        public List<SelectListItem> CarNOList { get; set; }
        public List<SelectListItem> StatusList { get; set; }

        public string ExpenditureNo { get; set; }
        public string ExpenditureBy { get; set; }
        public string CostHead { get; set; }
        public string ExpenditureHead { get; set; }
        public string CarNO { get; set; }
        public string View { get; set; }
        public string Status { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

        public string ExpDetailsTotal { get; set; }
        public string ExpsummaryTotal { get; set; }

        public List<SearchTableList> TableDataList { get; set; }

        public class SearchTableList
        {
            public string ExpenditureHead { get; set; }
            public string CostHead { get; set; }
            public decimal Amount { get; set; }            
        }

        public List<SummaryTableList> SummaryTableDataList { get; set; }

        public class SummaryTableList
        {
            public string Date { get; set; }
            public string ExpenditureNo { get; set; }
            public string ExpenditureBy { get; set; }
            public string JourneyDate { get; set; }
            public decimal TotalAmount { get; set; }
            public decimal ApprovedAmount { get; set; }
            public string Status { get; set; }
        }

        public List<CarSummaryTableList> CarSummaryTableDataList { get; set; }

        public class CarSummaryTableList
        {
            public string Date { get; set; }
            public string ExpenditureNo { get; set; }
            public string ExpenditureBy { get; set; }
            public string SubmitDate { get; set; }
            public string CarNo { get; set; }
            public string CostHead { get; set; }
            public decimal TotalAmount { get; set; }
            public string Status { get; set; }
        }

        public List<CarDetailsTableList> CarDetailsTableDataList { get; set; }

        public class CarDetailsTableList
        {
            public string Date { get; set; }
            public string ExpenditureNo { get; set; }
            public string ExpenditureHead { get; set; }
            public string CostHead { get; set; }
            public decimal TotalAmount { get; set; }
            public string Status { get; set; }
        }
    }
}
