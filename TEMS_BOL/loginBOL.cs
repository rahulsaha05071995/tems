﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TEMS_BOL
{
    public class loginBOL
    {
        [Required]
        public string USER_NAME { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string PASSWORD { get; set; }
    }
}
