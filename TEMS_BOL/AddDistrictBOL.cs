﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TEMS_BOL
{
    public class AddDistrictBOL
    {
        public List<SelectListItem> StateList { get; set; }

        public List<SelectListItem> DistrictList { get; set; }

        public string DistrictID { get; set; }

        public string RefStateID { get; set; }

        public string DistrictName { get; set; }

        public string Status { get; set; }

        public List<DistrictData> DistrictListReportData { get; set; }

        public class DistrictData
        {
            public string DistrictID { get; set; }

            public string DistrictName { get; set; }

            public string RefStateID { get; set; }

            public string Status { get; set; }
        }
    }
}
