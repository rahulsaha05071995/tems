﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TEMS_BOL
{
    public class LedgerBOL
    {
        [DisplayName("Ledger For")]
        public string LedgerFor { get; set; }

        [DisplayName("Agency Name")]
        public string AgencyName { get; set; }

        [DisplayName("Employee Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Report Type")]
        public string ReportType { get; set; }

        [DisplayName("From Date ")]
        public string FromDate { get; set; }

        [DisplayName("To Date")]
        public string ToDate { get; set; }

        public List<SelectListItem> AgencyNameList { get; set; }
        public List<SelectListItem> EmployeesNameList { get; set; }

        public List<LedgerSummaryReportListBOL> LedgerSummaryReportList { get; set; }
        public List<LedgerDetailsReportListBOL> LedgerDetailsReportList { get; set; }
    }

    public class LedgerSummaryReportListBOL
    {
        public string CreatedDate { get; set; }
        public string TransactionDate { get; set; }
        public string Particulars { get; set; }
        public string VoucherNo { get; set; }
        public decimal DebitAmount { get; set; }
        public decimal CreditAmount { get; set; }
        public decimal BalanceAmount { get; set; }
    }

    public class LedgerDetailsReportListBOL
    {
        public DateTime CreatedDate { get; set; }
        public string TransactionDate { get; set; }
        public string Particulars { get; set; }
        public string VoucherNo { get; set; }
        public decimal DebitAmount { get; set; }
        public decimal CreditAmount { get; set; }
        public decimal BalanceAmount { get; set; }
    }
}
