﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TEMS_BOL;
using TEMS_DAL;
using static TEMS_BOL.CostCenterProjectBOL;

namespace TEMS_BLL
{
    public class CostCenterProjectBLL
    {
        CostCenterProjectDAL costprojectdal = new CostCenterProjectDAL();
        public bool AddCostCenterProjectMap(CostCenterProjectBOL costproject, string USERID)
        {
            bool CheckAddCostCenterProjectMap = costprojectdal.AddCostCenterProjectMap(costproject, USERID);
            return CheckAddCostCenterProjectMap;
        }

        public List<CostCenterProjectData> CostProjectData(CostCenterProjectBOL costproject)
        {
            List<CostCenterProjectData> CostCenterProjectListData = costprojectdal.CostProjectData(costproject);
            return CostCenterProjectListData;
        }

        public CostCenterProjectBOL EditCostCenterProject(string CostProjectMapId)
        {
            CostCenterProjectBOL costproject = costprojectdal.EditCostCenterProject(CostProjectMapId);
            return costproject;
        }

        public bool EditCostCenterProjectData(CostCenterProjectBOL costproject, string USERID)
        {
            bool EditCostCenterProjectdata = costprojectdal.EditCostCenterProjectData(costproject, USERID);
            return EditCostCenterProjectdata;
        }
    }
}
