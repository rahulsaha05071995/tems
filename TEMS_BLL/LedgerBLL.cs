﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TEMS_BOL;
using TEMS_DAL;
using static TEMS_BOL.LedgerBOL;

namespace TEMS_BLL
{
    public class LedgerBLL
    {
        LedgerDAL LedgerDal = new LedgerDAL();

        public List<LedgerSummaryReportListBOL> GetLedgerSummaryReportList(LedgerBOL Ledger)
        {
            DateTime FromDate = Ledger.FromDate != "" ? Convert.ToDateTime(Ledger.FromDate) : DateTime.Now;
            DateTime ToDate = Ledger.ToDate != "" ? Convert.ToDateTime(Ledger.ToDate + " 23:59:59") : DateTime.Now;

            List<LedgerSummaryReportListBOL> SummaryReportListData = LedgerDal.GetLedgerSummaryReportList(Ledger, FromDate, ToDate);
            return SummaryReportListData;
        }

        public List<LedgerDetailsReportListBOL> GetLedgerDetailsReportList(LedgerBOL Ledger)
        {
            DateTime FromDate = Ledger.FromDate != "" ? Convert.ToDateTime(Ledger.FromDate) : DateTime.Now;
            DateTime ToDate = Ledger.ToDate != "" ? Convert.ToDateTime(Ledger.ToDate + " 23:59:59") : DateTime.Now;

            List<LedgerDetailsReportListBOL> DetailsReportListData = LedgerDal.GetLedgerDetailsReportList(Ledger, FromDate, ToDate);
            return DetailsReportListData;
        }
    }
}
