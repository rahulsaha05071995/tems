﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TEMS_BOL;
using TEMS_DAL;
using static TEMS_BOL.SearchExpenditureBOL;

namespace TEMS_BLL
{
    public class ExpendetureBLL
    {
        ExpendetureDAL expendDAL = new ExpendetureDAL();

        public int checkduplicate(ExpendetureBOL emp, string UserName, string UserID)
        {
            int duplicateexpID = expendDAL.checkduplicate(emp, UserName,UserID);
            return duplicateexpID;
        }

        public List<SelectListItem> ListPayment()
        {            
            List<SelectListItem> PaymentForList = expendDAL.ListPayment();
            return PaymentForList;
        }

        public List<SelectListItem> ListCostCenter()
        {            
            List<SelectListItem> CostCenterList = expendDAL.ListCostCenter();
            return CostCenterList;
        }

        public List<SelectListItem> ListSate()
        {
            List<SelectListItem> StateList = expendDAL.ListSate();
            return StateList;
        }

        public List<SelectListItem> ListProject()
        {
            List<SelectListItem> ProjectList = expendDAL.ListProject();
            return ProjectList;
        }

        public List<MSTR_CITY> FetchCity(string STATE_ID)
        {
            List<MSTR_CITY> citylist = expendDAL.FetchCity(STATE_ID);
            return citylist;
        }

        public List<TRN_ALLOWANCE> FetchAllowance(string Expenditure_ID, string UserName)
        {
            List<TRN_ALLOWANCE> allowancelist = expendDAL.FetchAllowance(Expenditure_ID,UserName);
            return allowancelist;
        }
        public List<SelectListItem> getdata(ExpendetureBOL emp, string UserName)
        {
            List<SelectListItem> savedata = expendDAL.getdata(emp, UserName);
            return savedata;
        }

        public List<TRN_EXPENDETURE_SUMMARY> FetchExpendituredata(string Expenditure_No)
        {
            List<TRN_EXPENDETURE_SUMMARY> Datalist = expendDAL.FetchExpendituredata(Expenditure_No);
            return Datalist;
        }

        public List<SelectListItem> ListExpenditureBy(string USERNAME)
        {
            List<SelectListItem> ExpenditureByList = expendDAL.ListExpenditureBy(USERNAME);
            return ExpenditureByList;
        }

        public List<SelectListItem> ListExpenditureBy()
        {
            List<SelectListItem> ExpenditureByList = expendDAL.ListExpenditureBy();
            return ExpenditureByList;
        }

        public List<SelectListItem> ListCarNo()
        {
            List<SelectListItem> CarNOList = expendDAL.ListCarNo();
            return CarNOList;
        }

        public List<SelectListItem> ListStatus()
        {
            List<SelectListItem> StatusList = expendDAL.ListStatus();
            return StatusList;
        }

        public List<SearchTableList> FetchTableData(SearchExpenditureBOL searchdetails)
        {
            List<SearchTableList> TableDataList = expendDAL.FetchTableData(searchdetails);
            return TableDataList;
        }

        public List<SummaryTableList> FetchSummaryTableData(string USERNAME)
        {
            List<SummaryTableList> SummaryTableDataList = expendDAL.FetchSummaryTableData(USERNAME);
            return SummaryTableDataList;
        }

        public List<SummaryTableList> FetchSummaryTableData()
        {
            List<SummaryTableList> SummaryTableDataList = expendDAL.FetchSummaryTableData();
            return SummaryTableDataList;
        }

        public List<SummaryTableList> FetchSummaryTableData(SearchExpenditureBOL searchdetails)
        {
            List<SummaryTableList> SummaryTableDataList = expendDAL.FetchSummaryTableData(searchdetails);
            return SummaryTableDataList;
        }

        public List<CarSummaryTableList> FetchCarSummaryData(SearchExpenditureBOL searchdetails)
        {
            List<CarSummaryTableList> CarSummaryTableDataList = expendDAL.FetchCarSummaryData(searchdetails);
            return CarSummaryTableDataList;
        }

        public List<CarDetailsTableList> FetchCarDetailsData(SearchExpenditureBOL searchdetails)
        {
            List<CarDetailsTableList> CarDetailsTableDataList = expendDAL.FetchCarDetailsData(searchdetails);
            return CarDetailsTableDataList;
        }    
        
        public List<SelectListItem> ListAgency()
        {
            List<SelectListItem> AgencyList = expendDAL.ListAgency();
            return AgencyList;
        }

    }
}
