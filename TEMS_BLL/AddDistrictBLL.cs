﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TEMS_BOL;
using TEMS_DAL;
using static TEMS_BOL.AddDistrictBOL;

namespace TEMS_BLL
{
    public class AddDistrictBLL
    {
        AddDistrictDAL DistrictDAL = new AddDistrictDAL();
        public List<SelectListItem> ListState()
        {
            List<SelectListItem> StateList = DistrictDAL.ListState();
            return StateList;
        }

        public bool AddDistrictData(AddDistrictBOL District, string USERID)
        {
            bool checkdistrictdata = DistrictDAL.AddDistrictData(District, USERID);
            return checkdistrictdata;
        }

        public List<DistrictData> GetDistrictData(AddDistrictBOL District)
        {
            List<DistrictData> DistrictListReportData = DistrictDAL.GetDistrictData(District);
            return DistrictListReportData;
        }

        public AddDistrictBOL EditDistrict(string DId)
        {
            AddDistrictBOL District = DistrictDAL.EditDistrict(DId);
            return District;
        }

        public bool EditDistrictData(AddDistrictBOL District,string USERID)
        {
            bool EditDistrictdata = DistrictDAL.EditDistrictData(District, USERID);
            return EditDistrictdata;
        }
    }
}
