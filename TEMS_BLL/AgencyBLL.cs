﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TEMS_BOL;
using TEMS_DAL;
using static TEMS_BOL.AddAgencyBOL;

namespace TEMS_BLL
{
    public class AgencyBLL
    {
        AgencyDAL AgncyDAL = new AgencyDAL();

        public List<AgencyListData> GetagencyList(AddAgencyBOL search)
        {
            List<AgencyListData> AgencyListReportData = AgncyDAL.GetagencyList(search);
            return AgencyListReportData;
        }

        public bool AddAgencyData(AddAgencyBOL agency,string USERID)
        {
            bool checkagencydata = AgncyDAL.AddAgencyData(agency, USERID);
            return checkagencydata;
        }

        public AddAgencyBOL EditAgency(string AId)
        {
            AddAgencyBOL agency = AgncyDAL.EditAgency(AId);
            return agency;
        }

        public bool EditAgencyData(AddAgencyBOL agency, string USERID)
        {
            bool Editagencydata = AgncyDAL.EditAgencyData(agency, USERID);
            return Editagencydata;
        }
    }
}
