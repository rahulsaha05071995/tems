﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TEMS_BOL;
using TEMS_DAL;
using static TEMS_BOL.ProjectBOL;

namespace TEMS_BLL
{
    public class ProjectBLL
    {
        ProjectDAL projectdal = new ProjectDAL();
        public bool AddProjectData(ProjectBOL Project,string USERID)
        {
            bool checkprojectdata = projectdal.AddProjectData(Project, USERID);
            return checkprojectdata;
        }

        public List<ProjectData> GetProjectList(ProjectBOL Project)
        {
            List<ProjectData> ProjectListData = projectdal.GetProjectList(Project);
            return ProjectListData;
        }

        public ProjectBOL Editproject(string PId)
        {
            ProjectBOL Project = projectdal.Editproject(PId);
            return Project;
        }

        public bool EditProjectData(ProjectBOL Project, string USERID)
        {
            bool EditProjectdata = projectdal.EditProjectData(Project, USERID);
            return EditProjectdata;
        }
    }
}
