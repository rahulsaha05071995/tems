﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TEMS_BOL;
using TEMS_DAL;

namespace TEMS_BLL
{
    public class ReportBLL
    {
        ReportDAL ReportDal = new ReportDAL();
        public List<SelectListItem> GetAgencyNameList()
        {
            List<SelectListItem> AgencyList = ReportDal.GetAgencyNameList();
            return AgencyList;
        }

        public List<SelectListItem> GetEmployeesNameList()
        {
            List<SelectListItem> EmployeesList = ReportDal.GetEmployeesNameList();
            return EmployeesList;
        }

        public List<SelectListItem> GetEmployeeAgencyNameList()
        {
            List<SelectListItem> EmployeeAgencyList = ReportDal.GetEmployeeAgencyNameList();
            return EmployeeAgencyList;
        }

        public List<TransactionReportListBOL> GetTransactionReportList(TransactionReportBOL TransactionBOL)
        {
            DateTime FromDate = TransactionBOL.FromDate != "" ? Convert.ToDateTime(TransactionBOL.FromDate) : DateTime.Now;
            DateTime ToDate = TransactionBOL.ToDate != "" ? Convert.ToDateTime(TransactionBOL.ToDate + " 23:59:59") : DateTime.Now;

            List<TransactionReportListBOL> ReportListData = ReportDal.GetTransactionReportList(TransactionBOL, FromDate, ToDate);
            return ReportListData;
        }

        public List<TransactionHistoryListBOL> GetTransactionReportHistoryData(string Ids, string FromDate, string ToDate, string Type)
        {
            DateTime FromDateNow = FromDate != "" ? Convert.ToDateTime(FromDate) : DateTime.Now;
            DateTime ToDateNow = ToDate != "" ? Convert.ToDateTime(ToDate + " 23:59:59") : DateTime.Now;

            List<TransactionHistoryListBOL> ListReportHistory = ReportDal.GetTransactionReportHistoryData(Ids, FromDate, ToDate, Type, FromDateNow, ToDateNow);
            return ListReportHistory;
        }

        public List<RegisterReportListBOL> GetRegisterReportList(RegisterReportBOL RegisterBOL)
        {
            DateTime Years = (RegisterBOL.Years != "" && RegisterBOL.Years != null) ? Convert.ToDateTime("01-Jan" + RegisterBOL.Years) : DateTime.Now;
            DateTime Month = (RegisterBOL.Month != "" && RegisterBOL.Month != null) ? Convert.ToDateTime("01-" + RegisterBOL.Month) : DateTime.Now;
            DateTime FromMonth = (RegisterBOL.FromMonth != "" && RegisterBOL.FromMonth != null) ? Convert.ToDateTime("01-" + RegisterBOL.FromMonth) : DateTime.Now;
            DateTime ToMonth = (RegisterBOL.ToMonth != "" && RegisterBOL.ToMonth != null) ? Convert.ToDateTime("01-" + RegisterBOL.ToMonth).AddMonths(1).AddMinutes(-1) : DateTime.Now;

            DateTime FromDate = RegisterBOL.FromDate != "" ? Convert.ToDateTime(RegisterBOL.FromDate) : DateTime.Now;
            DateTime ToDate = RegisterBOL.ToDate != "" ? Convert.ToDateTime(RegisterBOL.ToDate + " 23:59:59") : DateTime.Now;

            List<RegisterReportListBOL> RegisterReportListData = ReportDal.GetRegisterReportList(RegisterBOL, Years, Month, FromMonth, ToMonth, FromDate, ToDate);
            return RegisterReportListData;
        }

        public List<ExpendetureSummaryReportListBOL> GetExpendetureSummaryReportList(ExpendetureReportBOL ExpendetureBOL)
        {
            DateTime FromDate = ExpendetureBOL.FromDate != "" ? Convert.ToDateTime(ExpendetureBOL.FromDate) : DateTime.Now;
            DateTime ToDate = ExpendetureBOL.ToDate != "" ? Convert.ToDateTime(ExpendetureBOL.ToDate + " 23:59:59") : DateTime.Now;

            List<ExpendetureSummaryReportListBOL> ExpendetureSummaryReportList = ReportDal.GetExpendetureSummaryReportList(ExpendetureBOL, FromDate, ToDate);
            return ExpendetureSummaryReportList;
        }

        public List<ExpendetureDetailsReportListBOL> GetExpendetureDetailsReportList(ExpendetureReportBOL ExpendetureBOL)
        {
            DateTime FromDate = ExpendetureBOL.FromDate != "" ? Convert.ToDateTime(ExpendetureBOL.FromDate) : DateTime.Now;
            DateTime ToDate = ExpendetureBOL.ToDate != "" ? Convert.ToDateTime(ExpendetureBOL.ToDate + " 23:59:59") : DateTime.Now;

            List<ExpendetureDetailsReportListBOL> ExpendetureDetailsReportList = ReportDal.GetExpendetureDetailsReportList(ExpendetureBOL, FromDate, ToDate);
            return ExpendetureDetailsReportList;
        }
    }
}
