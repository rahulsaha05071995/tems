﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TEMS_BOL;
using TEMS_DAL;

namespace TEMS_BLL
{
    public class ApprovalBLL
    {
        ApprovalDAL approvedal = new ApprovalDAL();
        public List<SelectListItem> ListCity()
        {
            List<SelectListItem> CityList = approvedal.ListCity();
            return CityList;
        }

        public ApprovalBOL FetchApproveList(string ID)
        {
            ApprovalBOL approve = approvedal.FetchApproveList(ID);
            return approve;
        }

        public List<SelectListItem> getdata(ApprovalBOL approve,string USERNAME)
        {
            List<SelectListItem> Savedata = approvedal.getdata(approve, USERNAME);
            return Savedata;
        }

        public List<SelectListItem> DeclineData(string ID, string USERNAME)
        {
            List<SelectListItem> Decline = approvedal.DeclineData(ID, USERNAME);
            return Decline;
        }

        public bool DeleteConfirmedExpNO(string ID, string USERNAME)
        {
            bool DeleteStatus = approvedal.DeleteConfirmedExpNO(ID, USERNAME);
            return DeleteStatus;
        }

    }
}
