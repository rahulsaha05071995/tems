﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TEMS_BOL;
using TEMS_DAL;

namespace TEMS_BLL
{
    public class AddNewUserBLL
    {
        AddNewUserDAL UserDAL = new AddNewUserDAL();
        public List<SelectListItem> RoleList()
        {
            List<SelectListItem> ListRole = UserDAL.RoleList();
            return ListRole;
        }

        public List<SelectListItem> GradeList()
        {
            List<SelectListItem> ListGrade = UserDAL.GradeList();
            return ListGrade;
        }

        public int CheckDuplicateUserEmail(AddNewUserBOL User)
        {
            int DuplicateEmail = UserDAL.CheckDuplicateUserEmail(User);
            return DuplicateEmail;
        }

        public bool SaveUserDetails(AddNewUserBOL User, string USERNAME)
        {
            bool SaveUser = UserDAL.SaveUserDetails(User,USERNAME);
            return SaveUser;
        }

        public AddNewUserBOL EditUser(string UID)
        {
            AddNewUserBOL User = UserDAL.EditUser(UID);
            return User;
        }

        public bool EditUserDetails(AddNewUserBOL User, string USERNAME)
        {
            bool EditUser = UserDAL.EditUserDetails(User, USERNAME);
            return EditUser;
        }

        public int CheckDuplicateUserEditEmail(AddNewUserBOL User)
        {
            int DuplicateEmail = UserDAL.CheckDuplicateUserEditEmail(User);
            return DuplicateEmail;
        }
    }
}
