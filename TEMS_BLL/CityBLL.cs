﻿using System.Collections.Generic;
using System.Web.Mvc;
using TEMS_BOL;
using TEMS_DAL;
using static TEMS_BOL.AddCityBOL;

namespace TEMS_BLL
{
    public class CityBLL
    {
        CityDAL citydal = new CityDAL();
        public List<SelectListItem> ListDistrict()
        {
            List<SelectListItem> DistrictList = citydal.ListDistrict();
            return DistrictList;
        }

        public bool AddCityData(AddCityBOL City, string USERID)
        {
            bool CheckAddCity = citydal.AddCityData(City, USERID);
            return CheckAddCity;
        }

        public List<SelectListItem> ListCity()
        {
            List<SelectListItem> CityList = citydal.ListCity();
            return CityList;
        }

        public List<CityData> GetCityData(AddCityBOL City)
        {
            List<CityData> CityListData = citydal.GetCityData(City);
            return CityListData;
        }

        public AddCityBOL EditCity(string CId)
        {
            AddCityBOL City = citydal.EditCity(CId);
            return City;
        }

        public bool EditCityData(AddCityBOL City, string USERID)
        {
            bool EditCitydata = citydal.EditCityData(City, USERID);
            return EditCitydata;
        }
    }
}
