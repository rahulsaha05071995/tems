﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TEMS_BOL;
using TEMS_DAL;
using static TEMS_BOL.PayAmountBOL;

namespace TEMS_BLL
{
    public class PayAmountBLL
    {
        PayAmountDAL paymentDAL = new PayAmountDAL();

        public List<SelectListItem> ListExpID()
        {
            List<SelectListItem> ExpIDList = paymentDAL.ListExpID();
            return ExpIDList;
        }

        public List<SearchPayment> SearchPaymentBLL(PayAmountBOL payment)
        {
            List<SearchPayment> TableDataList = paymentDAL.SearchPaymentDAL(payment);
            return TableDataList;
        }

        public bool FetchPaymentData(PayAmountBOL payment, string USERNAME)
        {
            int AdvanceAdjId = paymentDAL.GetAdvanceAdjId();
            payment.AdvanceAdjId = AdvanceAdjId;
            bool PaymentData = paymentDAL.FetchPaymentData(payment, USERNAME);
            return PaymentData;
        }
    }
}
