﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TEMS_BOL;
using TEMS_DAL;
using static TEMS_BOL.SearchNewUserBOL;

namespace TEMS_BLL
{
    public class SearchNewUserBLL
    {
        SearchNewUserDAL SearchDAL = new SearchNewUserDAL();
        public List<UserListReportBO> GetUserList(SearchNewUserBOL search)
        {
            List<UserListReportBO> UserData = SearchDAL.GetUserList(search);
            return UserData;
        }
    }
}
