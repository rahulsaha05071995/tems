﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TEMS_BOL;
using TEMS_DAL;

namespace TEMS_BLL
{
    public class loginBLL
    {
        loginDAL loginDL = new loginDAL();
        public MSTR_USER checkuser(loginBOL emp)
        {
            var user = loginDL.checkuser(emp);
            return user;
        }
    }
}
