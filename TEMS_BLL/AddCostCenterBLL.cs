﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TEMS_BOL;
using TEMS_DAL;
using static TEMS_BOL.AddCostCenterBOL;

namespace TEMS_BLL
{
    public class AddCostCenterBLL
    {
        AddCostCenterDAL costcenterDAL = new AddCostCenterDAL();
        public List<SelectListItem> ListCity()
        {
            List<SelectListItem> CityList = costcenterDAL.ListCity();
            return CityList;
        }

        public bool AddCostCenterData(AddCostCenterBOL costcenter, string USERID)
        {
            bool CheckAddCostCenter = costcenterDAL.AddCostCenterData(costcenter, USERID);
            return CheckAddCostCenter;
        }

        public List<SelectListItem> ListCostCenter()
        {
            List<SelectListItem> CostCenterList = costcenterDAL.ListCostCenter();
            return CostCenterList;
        }

        public List<CostCenterData> ListCostCenterData(AddCostCenterBOL costcenter)
        {
            List<CostCenterData> CostCenterDataList = costcenterDAL.ListCostCenterData(costcenter);
            return CostCenterDataList;
        }

        public AddCostCenterBOL EditCostCenter(string CostCenterId)
        {
            AddCostCenterBOL costcenter = costcenterDAL.EditCostCenter(CostCenterId);
            return costcenter;
        }

        public bool EditCostCenterdata(AddCostCenterBOL costcenter, string USERID)
        {
            bool EditCostCenterdata = costcenterDAL.EditCostCenterdata(costcenter, USERID);
            return EditCostCenterdata;
        }
    }
}
